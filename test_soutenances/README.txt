Les bogues à corriger sont donnés sous forme de tests dans ce
répertoire.

I. Organisations des tests

- Des tests classés suivant la hiérarchie utilisée pendant le
  projet. Il y a éventuellement des liens symboliques sur certains de
  vos tests pour lesquels vous avez fait un contre-sens.

- Des tests "test*.deca": votre compilateur se comporte incorrectement
  sur ces fichiers. A vous de trouver le comportement qu'il devrait
  avoir et comment le corriger.

II. Remarques 

Vous générez une erreur à l'exécution sur les débordements
arithmétiques entiers. Ceci n'est pas le comportement attendu, les
erreurs de débordements étaient uniquement à générer pour les
*flottants* cf. [ Semantique].  Ce contre-sens est génant notamment
pour évaluer votre classe Math.
