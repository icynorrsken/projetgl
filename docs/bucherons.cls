\ProvidesClass{bucherons}

\LoadClass[a4paper,11pt]{report}

\RequirePackage{fullpage}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage[frenchb]{babel}
\RequirePackage{listings}
\RequirePackage{tabularx}
\RequirePackage{longtable}
\RequirePackage{hyperref}

\RequirePackage{graphicx}

\lstset{
	basicstyle=\ttfamily\footnotesize,
	columns=fullflexible,
	tabsize=4,
	lineskip={-1.5pt}
}

\usepackage{geometry}
\geometry{
 a4paper,
 total={210mm,297mm},
 left=20mm,
 right=20mm,
 top=20mm,
 bottom=20mm,
}

\author{Les B\^ucherons d'AST}


