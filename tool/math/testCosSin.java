import java.text.DecimalFormat;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.System.out;

class testCosSin {

    public static void main(String[] args) {
        DecimalFormat formatter = new DecimalFormat("0.00000E00");
        for (float f = 0; f <= Math.PI / 2; f += Math.PI / 2.0 / 8.0) {
            out.println("cos(" + formatter.format(f) + ") = " +
                        formatter.format((float) cos(f)));

            out.println("sin(" + formatter.format(f) + ") = " +
                        formatter.format((float) sin(f)));
        }
    }
}

