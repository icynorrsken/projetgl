import java.text.DecimalFormat;

class testUlp {

    public static void main(String[] args) {
        DecimalFormat formatter = new DecimalFormat("0.00000E00");

        float f = 0.0f;

        while (f < 10.0f) {
            System.out.println("ulp(" + formatter.format(f) + ") = " +
                               formatter.format(Math.ulp(f)));
            f = f + 1.0f;
        }

        while (f < 1.0e16f) {
            System.out.println("ulp(" + formatter.format(f) + ") = " +
                               formatter.format(Math.ulp(f)));
            f = f * 10.0f;
        }
    }
}
