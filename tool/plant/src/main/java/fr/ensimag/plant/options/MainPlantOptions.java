package fr.ensimag.plant.options;
import fr.ensimag.plant.Plant;
import fr.ensimag.plant.data.definition.ANSIColor;
import fr.ensimag.plant.data.definition.RunnerScope;

import java.io.File;
import java.util.*;
import java.util.Map.Entry;
/**
 * Created by Tom on 24/6/15.
 */
public class MainPlantOptions {
    private static final Set<String> knownOptions = new HashSet<>(Arrays.asList(
            new String[]{"web", "context", "syntax", "codegen", "decompile",
                         "-s", "options", "--all", "--mono", "--no-merge",
                         "--verbose", "--version"}));
    private static final Set<String> decacOptions = new HashSet<>(Arrays.asList(
            new String[]{"-n", "-r", "-p", "-P", "-d", "-b", "-v"}));
    private boolean                                web;
    private boolean                                all;
    private boolean                                mono;
    private boolean                                mergeable;
    private boolean                                verbose;
    private Map<RunnerScope, MultiplePlantOptions> runners;

    public MainPlantOptions() {
        web = false;
        all = false;
        mono = false;
        mergeable = true;
        verbose = false;
        runners = new HashMap<>();
    }

    public void computeOptions(String[] args) throws IllegalArgumentException {
        if (args.length >= 1) {
            for (int i = 0, argsLength = args.length; i < argsLength; i++) {
                String arg = new String(args[i]);
                if (knownOptions.contains(arg)) {
                    RunnerScope runnerScope;
                    switch (arg) {
                        case "--version":
                            printVersion();
                            System.exit(0);
                            break;
                        case "--verbose":
                            verbose = true;
                            break;
                        case "--no-merge":
                            mergeable = false;
                            break;
                        case "--mono":
                            mono = true;
                            break;
                        case "web":
                            web = true;
                            break;
                        case "--all":
                            all = true;
                            break;
                        case "options":
                            runnerScope = RunnerScope.OPTIONS;
                            runners.put(runnerScope,
                                        new MultiplePlantOptions<File>(
                                                runnerScope));
                            if (argsLength > i + 1)
                                if (decacOptions.contains(args[i + 1]))
                                    i += computeDecacOptions(
                                            Arrays.copyOfRange(args, ++i,
                                                               argsLength));
                            break;
                        case "syntax":
                            runnerScope = RunnerScope.SYNTAX;
                            runners.put(runnerScope,
                                        new MultiplePlantOptions<File>(
                                                runnerScope));
                            i += doCompute(
                                    Arrays.copyOfRange(args, i, argsLength),
                                    runnerScope);
                            break;
                        case "context":
                            runnerScope = RunnerScope.CONTEXT;
                            runners.put(runnerScope,
                                        new MultiplePlantOptions<File>(
                                                runnerScope));
                            i += doCompute(
                                    Arrays.copyOfRange(args, i, argsLength),
                                    runnerScope);
                            break;
                        case "codegen":
                            runnerScope = RunnerScope.DECAC;
                            runners.put(runnerScope,
                                        new MultiplePlantOptions<File>(
                                                runnerScope));
                            i += doCompute(
                                    Arrays.copyOfRange(args, i, argsLength),
                                    runnerScope);
                            break;
                        case "decompile":
                            runnerScope = RunnerScope.DECOMPILE;
                            runners.put(runnerScope,
                                        new MultiplePlantOptions<File>(
                                                runnerScope));
                            i += doCompute(
                                    Arrays.copyOfRange(args, i, argsLength),
                                    runnerScope);
                            break;
                        default:
                            throw new UnknownFormatFlagsException(
                                    "Unknown given argument: " + arg);
                    }
                } else {
                    throw new IllegalArgumentException(
                            "Unknown option: " + arg);
                }
            }
        }
        if (all) {
            initGenericsRunner();
            runners.put(RunnerScope.OPTIONS,
                        new MultiplePlantOptions<File>(RunnerScope.OPTIONS));
        } else if (isComplete()) {
            initGenericsRunner();
        }
    }

    public void initGenericsRunner() {
        runners.put(RunnerScope.SYNTAX,
                    new MultiplePlantOptions<File>(RunnerScope.SYNTAX));
        runners.put(RunnerScope.CONTEXT,
                    new MultiplePlantOptions<File>(RunnerScope.CONTEXT));
        runners.put(RunnerScope.DECAC,
                    new MultiplePlantOptions<File>(RunnerScope.DECAC));
        runners.put(RunnerScope.DECOMPILE,
                    new MultiplePlantOptions<File>(RunnerScope.DECOMPILE));
    }

    public Set<Entry<RunnerScope, MultiplePlantOptions>> entrySet() {
        return runners.entrySet();
    }

    public void printVersion() {
        StringBuilder sb = new StringBuilder();
        sb.append(ANSIColor.ANSI_BLUE.getCode());
        sb.append("\n" +
                  "--------------------------------------------------------------------\n\n");
        sb.append(ANSIColor.ANSI_CYAN.getCode());
        sb.append("                         Plant version ");
        sb.append(Plant.VERSION);
        sb.append("\n");
        sb.append(ANSIColor.ANSI_PURPLE.getCode());
        sb.append("                           Developed by\n");
        sb.append("                        Les bûcherons d'AST");
        sb.append(ANSIColor.ANSI_BLUE.getCode());
        sb.append(
                "\n\n--------------------------------------------------------------------\n");
        sb.append(ANSIColor.ANSI_RESET.getCode());
        System.out.println(sb.toString());
    }

    private int doCompute(String[] args, RunnerScope runnerScope) {
        int argsLength = args.length;
        int i = 0;
        String arg = args[0];

        if (argsLength > i + 1)
            if (args[i + 1].equals("-s"))
                if (argsLength > ++i + 1) {
                    if (!knownOptions.contains(args[i + 1]) &&
                        !decacOptions.contains(args[i + 1]))
                        i += computeSpecificFiles(
                                Arrays.copyOfRange(args, ++i, argsLength),
                                runnerScope);
                    else
                        throw new IllegalArgumentException(
                                "The given file may not exist cause of it's " +
                                "strange name: '" +
                                args[i + 1] + "'");
                } else
                    throw new IllegalArgumentException(
                            "Not enough arguments given");
        return i;
    }

    private int computeSpecificFiles(String[] args, RunnerScope runnerScope) {
        int i = 0;
        for (int argsLength = args.length; i < argsLength; i++) {
            String arg = args[i];
            if (decacOptions.contains(arg) || knownOptions.contains(arg)) {
                break;
            } else {
                File f = new File(arg);
                if (f.exists()) {
                    runners.get(runnerScope).add(f);
                } else
                    throw new IllegalArgumentException(
                            "The given file or directory doesn't exist");
            }
        }
        return i;
    }

    private int computeDecacOptions(String[] args) {
        MultiplePlantOptions options = runners.get(RunnerScope.OPTIONS);
        int i = 0;
        for (int argsLength = args.length; i < argsLength; i++) {
            String arg = args[i];
            if (decacOptions.contains(arg))
                options.add(arg);
            else
                break;
        }
        return i;
    }

    /**
     * Gets verbose
     *
     * @return the verbose's value
     */
    public boolean isVerbose() {
        return verbose;
    }

    /**
     * Gets merge
     *
     * @return the mergeable's value
     */
    public boolean isMergeable() {
        return mergeable;
    }

    /**
     * Gets mono
     *
     * @return the mono's value
     */
    public boolean isMono() {
        return mono;
    }

    /**
     * Gets web
     *
     * @return the web's value
     */
    public boolean isWeb() {
        return web;
    }

    /**
     * Gets all
     *
     * @return the all's value
     */
    public boolean isAll() {
        return all;
    }

    public boolean isWithContext() {
        return runners.containsKey(RunnerScope.CONTEXT);
    }

    public boolean isWithSyntax() {
        return runners.containsKey(RunnerScope.SYNTAX);
    }

    public boolean isWithCodegen() {
        return runners.containsKey(RunnerScope.DECAC);
    }

    public boolean isWithOptions() {
        return runners.containsKey(RunnerScope.OPTIONS);
    }

    public boolean isWithDecompile() {
        return runners.containsKey(RunnerScope.DECOMPILE);
    }

    public boolean isComplete() {
        return !isWithCodegen() && !isWithSyntax() && !isWithContext() &&
               !isWithOptions() && !isWithDecompile() && !isAll();
    }
}
