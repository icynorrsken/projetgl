package fr.ensimag.plant.report;
import fr.ensimag.plant.data.FileWrapper;
import fr.ensimag.plant.statistics.Statistics;

import java.util.List;
/**
 * Created by Tom on 22/6/15.
 */
public interface Reportable {
    /**
     * Create report
     */
    void createReport(String name, Statistics statistics,
                      List<FileWrapper> sourceFiles);
}
