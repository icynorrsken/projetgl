package fr.ensimag.plant.data;
import fr.ensimag.plant.data.definition.State;

import java.io.File;
/**
 * Created by Tom on 23/6/15.
 */
public class FileWrapper {
    private File   file;
    private State  state;
    private String errorMessage;
    private String output;

    public FileWrapper(File file) {
        this.file = file;
        state = State.WAIT;
    }

    /**
     * Gets output
     *
     * @return the output's value
     */
    public String getOutput() {
        return output;
    }

    /**
     * Sets output
     *
     * @param output the value to set
     */
    public void setOutput(String output) {
        this.output = output;
    }

    /**
     * Gets errorMessage
     *
     * @return the errorMessage's value
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets errorMessage
     *
     * @param errorMessage the value to set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Gets state
     *
     * @return the state's value
     */
    public State getState() {
        return state;
    }

    /**
     * Sets state
     *
     * @param state the value to set
     */
    public void setState(State state) {
        this.state = state;
    }

    /**
     * Gets file
     *
     * @return the file's value
     */
    public File getFile() {
        return file;
    }
}
