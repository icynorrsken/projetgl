package fr.ensimag.plant.data.definition;
/**
 * Created by Tom on 22/6/15.
 */
public enum State {
    WAIT,
    PASSED,
    FAILED,
    NOT_RUN;
}
