package fr.ensimag.plant.data.exception;
import fr.ensimag.plant.data.definition.RunnerScope;
import fr.ensimag.plant.runner.Runner;
/**
 * Created by Tom on 26/6/15.
 */
public class UnknownRunnerException extends RuntimeException {
    public UnknownRunnerException(Runner runner) {
        this(runner.getRunnerScope());
    }

    public UnknownRunnerException(Class clazz) {
        super("Unkown runner class: '" + clazz.getSimpleName() +
              "' please contact Plant's developer");
    }

    public UnknownRunnerException(RunnerScope runner) {
        super("Unkown runner name: '" + runner.getName() +
              "' please contact Plant's developer");
    }
}
