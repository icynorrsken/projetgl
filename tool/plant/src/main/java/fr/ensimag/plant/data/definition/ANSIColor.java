package fr.ensimag.plant.data.definition;
/**
 * Created by Tom on 25/6/15.
 */
public enum ANSIColor {
    ANSI_RESET("\u001B[0m"),
    ANSI_BLACK("\u001B[30m"),
    ANSI_RED("\u001B[31m"),
    ANSI_GREEN("\u001B[32m"),
    ANSI_YELLOW("\u001B[33m"),
    ANSI_BLUE("\u001B[34m"),
    ANSI_PURPLE("\u001B[35m"),
    ANSI_CYAN("\u001B[36m"),
    ANSI_WHITE("\u001B[37m");

    private String code;

    ANSIColor(String code) {
        this.code = code;
    }

    /**
     * Gets code
     *
     * @return the code's value
     */
    public java.lang.String getCode() {
        return code;
    }
}
