package fr.ensimag.plant.executor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/**
 * Created by Tom on 27/6/15.
 */
public abstract class AbstractExecutor implements Executor {
    protected String[] arguments;

    public AbstractExecutor(String[] arguments) {
        this.arguments = arguments;
    }

    public AbstractExecutor() {
        super();
    }

    @Override
    public ExecutorResult execute() throws InterruptedException, IOException {
        Process process = exec();
        process.waitFor();
        String output = readOutputStream(process);
        return new ExecutorResult(process.exitValue(), output);
    }

    protected final String readOutputStream(Process process)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(process.getInputStream()));
        String line;
        StringBuilder output = new StringBuilder();
        while ((line = bufferedReader.readLine()) != null) {
            output.append(line);
            output.append("\n");
        }
        return output.toString();
    }

    protected abstract Process exec() throws IOException;

    /**
     * Sets argument
     *
     * @param arguments the value to set
     */
    public void setArguments(String... arguments) {
        this.arguments = arguments;
    }
}
