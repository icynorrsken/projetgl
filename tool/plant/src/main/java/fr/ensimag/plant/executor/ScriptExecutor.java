package fr.ensimag.plant.executor;
import fr.ensimag.plant.data.exception.ElementNotFoundException;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
/**
 * Created by Tom on 27/6/15.
 */
public class ScriptExecutor extends AbstractExecutor {
    private String script;

    public ScriptExecutor(String script, String[] argument) {
        super(argument);
        this.script = script;
    }

    public ScriptExecutor(String script) {
        this(script, null);
    }

    public ScriptExecutor() {
        super();
    }

    @Override
    protected Process exec() throws IOException {
        File file = new File(script);
        if (!file.exists())
            throw new ElementNotFoundException(script);
        return new ProcessBuilder(script,
                                  Arrays.toString(arguments).replace("[", "")
                                          .replace("]", ""))
                .redirectErrorStream(true).start();
    }

    /**
     * Sets script
     *
     * @param script the value to set
     */
    public void setScript(String script) {
        this.script = script;
    }
}
