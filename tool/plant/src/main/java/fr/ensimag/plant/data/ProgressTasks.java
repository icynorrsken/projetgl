package fr.ensimag.plant.data;
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/**
 * Created by Tom on 25/6/15.
 */
public class ProgressTasks extends JFrame {
    private static ProgressTasks   instance        = new ProgressTasks();
    private        ExecutorService executorService =
            Executors.newSingleThreadExecutor();
    private Map<String, PairElement> progressBars;

    private ProgressTasks() {
        super("Tasks Progression");
        progressBars = new HashMap<>();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static ProgressTasks getInstance() {
        return instance;
    }

    public PairElement put(String key) {
        JProgressBar progressBar = new JProgressBar();
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        return progressBars.put(key, new PairElement(key, progressBar));
    }

    public void init(String name, int tasksLength) {
        PairElement pairElement = progressBars.get(name);
        if (pairElement.getProgressBar() != null) {
            pairElement.getProgressBar().setMinimum(0);
            pairElement.getProgressBar().setMaximum(tasksLength);
        } else {
            throw new NullPointerException("The asked given is not set");
        }
        start();
    }

    public void notifyUpdate(final String name, final int progress) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                updateProgressBars(name, progress);
            }
        });
    }

    public void closeWindow(final Window window) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                dispatchEvent(
                        new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
            }
        });
    }

    public void closeWindow() {
        dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }

    private void start() {
        int maximumValue = 0;
        for (Entry<String, PairElement> entry : progressBars.entrySet()) {
            if (!entry.getKey().equals("Plant"))
                maximumValue += entry.getValue().getMaximumProgress();
        }
        if (!progressBars.containsKey("Plant")) {
            JProgressBar progressBar = new JProgressBar(0, maximumValue);
            progressBar.setValue(0);
            progressBar.setStringPainted(true);
            progressBars.put("Plant", new PairElement("Plant", progressBar));
        } else
            progressBars.get("Plant").setMaximumProgress(maximumValue);

        int tasksNumber = progressBars.size();
        setSize(200, tasksNumber * 50);
        setMinimumSize(new Dimension(400, tasksNumber * 50));

        GridLayout gridLayout = new GridLayout(tasksNumber, 2);
        setLayout(gridLayout);

        for (Entry<String, PairElement> entry : progressBars.entrySet()) {
            PairElement pairElement = entry.getValue();
            add(pairElement.getLabel());
            add(pairElement.getProgressBar());
            if (!pairElement.isFinished())
                pairElement.setVisible(true);
        }

        setVisible(true);
    }

    private synchronized void updateProgressBars(String name, int progress) {
        PairElement pairElement = progressBars.get(name);
        if (pairElement.getProgressBar() != null) {
            pairElement.getProgressBar().setValue(progress);
        } else {
            throw new NullPointerException("The asked given is not set");
        }

        int totalProgress = 0;
        for (Entry<String, PairElement> entry : progressBars.entrySet()) {
            PairElement pe = entry.getValue();
            if (!entry.getKey().equals("Plant"))
                totalProgress += pe.getProgress();
        }

        PairElement plant = progressBars.get("Plant");
        plant.setProgress(totalProgress);
        if (plant.isFinished()) {
            executorService.shutdown();
        }
    }
}
