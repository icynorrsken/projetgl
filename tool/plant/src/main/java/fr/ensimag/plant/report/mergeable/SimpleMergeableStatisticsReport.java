package fr.ensimag.plant.report.mergeable;
import fr.ensimag.plant.data.definition.ANSIColor;
import fr.ensimag.plant.report.AbstractReport;
/**
 * Created by Tom on 25/6/15.
 */
public class SimpleMergeableStatisticsReport extends MergeableStatisticsReport {
    public SimpleMergeableStatisticsReport(boolean concurrent) {
        super(concurrent);
    }

    @Override
    public void createReport() {
        createTitle("Global Statistics");
        sb.append("Test passed: ");
        sb.append(totalPassedTestNumber()).append("/")
                .append(totalTestRunNumber());
        sb.append(" (").append(totalPercentOfPassedTest()).append("%)\n");
        sb.append("Test run: ");
        sb.append(totalTestRunNumber()).append("/").append(totalTestNumber());
        sb.append(" (").append(totalPercentOfRunTest()).append("%)\n");
        sb.append("Task executing in: ");
        sb.append(totalDuration()).append("\n");
        if (totalFailedTestNumber() > 0) {
            sb.append(ANSIColor.ANSI_RED.getCode());
            sb.append(AbstractReport.failedScriptMessage());
            sb.append(ANSIColor.ANSI_RESET.getCode());
        }
        write(null);
    }

    @Override
    protected void write(String name) {
        System.out.println(sb.toString());
    }

    @Override
    protected void createTitle(String name) {
        sb.append("----------------------------------------\n");
        for (int i = 0; i < 20 - (name.length() / 2) - 1; i++) {
            sb.append(" ");
        }
        sb.append(name);
        for (int i = 0; i < 20 - (name.length() / 2); i++) {
            sb.append(" ");
        }
        sb.append("\n----------------------------------------\n\n");
    }
}
