package fr.ensimag.plant.report.mergeable;
import fr.ensimag.plant.data.FileWrapper;
import fr.ensimag.plant.report.WebReport;
import fr.ensimag.plant.statistics.Statistics;

import java.io.*;
import java.util.List;
/**
 * Created by Tom on 25/6/15.
 */
public class WebMergeableReport extends WebReport implements MergeableReport {
    public static final String  NAME  = "Plant";
    private static      boolean FIRST = true;

    @Override
    public synchronized void createReport(String name, Statistics statistics,
                                          List<FileWrapper> sourceFiles) {
        createTitle(name);
        createBody(initBody(sourceFiles));
        webWrite(NAME);
    }

    @Override
    public void webWrite(String name) {
        File index = new File(
                System.getProperty("user.dir") + "/index-" + name + ".html");

        try {
            if (!index.exists())
                index.createNewFile();

            PrintWriter writer;
            if (FIRST) {
                writer = new PrintWriter(
                        new BufferedWriter(new FileWriter(index, false)));
                StringBuilder html = new StringBuilder();
                html.append(htmlStart());
                html.append(sb.toString());
                writer.println(html.toString());
                FIRST = false;
            } else {
                writer = new PrintWriter(
                        new BufferedWriter(new FileWriter(index, true)));
                writer.println(sb.toString());
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void merge(String name, Statistics statistics) {
        sb.setLength(0);
        createStatistics(name, statistics);

        File index = new File(
                System.getProperty("user.dir") + "/index-" + NAME + ".html");

        try (PrintWriter writer = new PrintWriter(
                new BufferedWriter(new FileWriter(index, true)))) {
            writer.println(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
