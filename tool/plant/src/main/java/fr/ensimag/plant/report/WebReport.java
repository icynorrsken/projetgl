package fr.ensimag.plant.report;
import fr.ensimag.plant.Plant;
import fr.ensimag.plant.data.FileWrapper;
import fr.ensimag.plant.data.definition.State;
import fr.ensimag.plant.statistics.Statistics;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.UUID;
/**
 * Created by Tom on 22/6/15.
 */
public class WebReport extends AbstractReport {
    public static String htmlStart() {
        StringBuilder web = new StringBuilder();
        web.append("<!DOCTYPE html>");
        web.append("<html>");
        web.append("<head>");
        web.append("<title>Plant Tester Tool web report - Version ");
        web.append(Plant.VERSION);
        web.append("</title>");
        web.append("<style> .hide { display: none } </style>");
        web.append(
                "<script> function revertVisibility(uuid){ v=document" +
                ".getElementById(uuid).style.display; if(v!=\"none\"){ " +
                "document.getElementById(uuid).style" +
                ".display=\"none\"; } else { document.getElementById(uuid)" +
                ".style.display=\"block\"; } } </script>");
        web.append(
                "<link href=\"lib/bs/css/bootstrap.min.css\" " +
                "rel=\"stylesheet\">");
        web.append("</head>");
        web.append("<body>");
        web.append("<h1 align='center'>");
        web.append("Plant Tester Tool web report - Version ");
        web.append(Plant.VERSION);
        web.append("</h1>");
        return web.toString();
    }

    public static String htmlEnd() {
        StringBuilder web = new StringBuilder();
        web.append(
                "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1" +
                ".11.3/jquery.min.js\"></script>");
        web.append("<script src=\"lib/bs/js/bootstrap.min.js\"></script>");
        web.append("</body>");
        web.append("</html>");
        return web.toString();
    }

    @Override
    protected void createTitle(String name) {
        sb.append("<div width='100%' align='center'><h1>").append(name)
                .append("</h1></div>");
    }

    @Override
    protected void write(String name) {
        File index = new File(
                System.getProperty("user.dir") + "/index-" + name + ".html");

        try {
            if (!index.exists())
                index.createNewFile();

            PrintWriter writer =
                    new PrintWriter(index.getAbsolutePath(), "UTF-8");
            writer.println(htmlSurrounding());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Site generated at: " + index.getAbsolutePath());
    }

    @Override
    protected void createBody(State state, List<FileWrapper> files) {
        if (!files.isEmpty()) {
            sb.append("<h2 ");
            sb.append("class=\"bg-");
            switch (state) {
                case FAILED:
                    sb.append("danger");
                    break;
                case NOT_RUN:
                    sb.append("info");
                    break;
                case PASSED:
                    sb.append("success");
                    break;
                default:
            }
            sb.append("\">");
            sb.append(state.toString());
            sb.append("</h2>");

            if (State.FAILED == state) {
                createErrorBody(files);
            } else {
                sb.append("<table class=\"table table-striped\">");
                sb.append("<tr>");
                sb.append("<th>");
                sb.append("Test file");
                sb.append("</th>");
                sb.append("<th>");
                sb.append("State");
                sb.append("</th>");
                for (FileWrapper file : files) {
                    sb.append("<tr>");
                    sb.append("<td>");
                    sb.append(file.getFile().getAbsolutePath());
                    sb.append("</td>");
                    sb.append("<td>");
                    sb.append("<span class=\"label label-");
                    if (state == State.PASSED)
                        sb.append("success");
                    else
                        sb.append("primary");
                    sb.append("\">");
                    sb.append("<b>").append(state).append("</b>");
                    sb.append("</span>");
                    sb.append("</td>");
                    sb.append("</tr>");
                }
                sb.append("</table>");
            }
        }
    }

    @Override
    protected void createBody(Map<State, List<FileWrapper>> map) {
        sb.append("<div id='result'>");
        createBody(State.PASSED, map.get(State.PASSED));
        createBody(State.NOT_RUN, map.get(State.NOT_RUN));
        createBody(State.FAILED, map.get(State.FAILED));
        sb.append("</div>");
    }

    @Override
    protected void createStatistics(String name, Statistics statistics) {
        sb.append("<div id='statistics' width='100%' align='center'>");
        createTitle(name + " Statistics");
        sb.append("<p>");
        sb.append("<b>Test passed:</b> ");
        sb.append(statistics.getPassedTestNumber()).append("/")
                .append(statistics.getTestRunNumber());
        sb.append(" (").append(statistics.percentOfPassedTest()).append("%)");
        sb.append("</p>");
        sb.append("<p>");
        sb.append("<b>Test run:</b> ");
        sb.append(statistics.getTestRunNumber()).append("/")
                .append(statistics.getTestNumber());
        sb.append(" (").append(statistics.percentOfRunTest()).append("%)");
        sb.append("</p>");
        sb.append("<p>");
        sb.append("<b>Task executing in:</b> ");
        sb.append(statistics.getDuration());
        sb.append("</p>");
        sb.append("<div class=\"alert alert-");
        if (statistics.getFailedTestNumber() > 0)
            sb.append("danger\" role=\"alert\">").append(failedScriptMessage());
        else
            sb.append("success\" role=\"alert\">")
                    .append(passedScriptMessage());
        sb.append("</div>");
        sb.append("</div>");
    }

    private void createErrorBody(List<FileWrapper> files) {
        sb.append(
                "<div class=\"panel-group\" id=\"accordion\" role=\"tablist\"" +
                " aria-multiselectable=\"true\">");

        for (FileWrapper file : files) {
            String uuid = UUID.randomUUID().toString();
            sb.append("<div class=\"panel panel-default\">");
            sb.append("<div class=\"panel-heading\" role=\"tab\" id=\"heading");
            sb.append(uuid);
            sb.append("\">");
            sb.append("<h4 class=\"panel-title\">");
            sb.append(
                    "<a role=\"button\" data-toggle=\"collapse\" " +
                    "data-parent=\"#accordion\" href=\"#collapse");
            sb.append(uuid);
            sb.append("\" aria-expanded=\"false\" aria-controls=\"collapse");
            sb.append(uuid);
            sb.append("\">");
            sb.append(file.getFile().getName().toString());
            sb.append("</a>");
            sb.append("</h4>");
            sb.append("</div>");
            sb.append("<div id=\"collapse");
            sb.append(uuid);
            sb.append(
                    "\" class=\"panel-collapse collapse in\" " +
                    "role=\"tabpanel\" aria-labelledby=\"heading");
            sb.append(uuid);
            sb.append("\">");
            sb.append("<div class=\"panel-body\">");
            sb.append(
                    "<span class=\"label label-danger\">Script Error " +
                    "Message</span><p>");
            sb.append(file.getErrorMessage());
            sb.append("</p>");
            sb.append(
                    "<span class=\"label label-danger\">Standard and Errror " +
                    "Output Stream</span><p>");
            sb.append("<p>");
            sb.append(file.getOutput());
            sb.append("</p>");
            sb.append("</div>");
            sb.append("</div>");
            sb.append("</div>");
        }
    }

    private void createErrorMessage(String uuid, String scriptErrorMessage,
                                    String output) {
        sb.append("<tr class='hide' id='");
        sb.append(uuid);
        sb.append("'>");
        sb.append("<td colspan='2'>");
        sb.append("<table align='center' border='1' width='center'>");
        sb.append("<tr>");
        sb.append("<th>");
        sb.append("<span style='color:orange'>");
        sb.append("Script Error Message");
        sb.append("</span>");
        sb.append("</th>");
        sb.append("<th>");
        sb.append("<span style='color:orange'>");
        sb.append("Standard and error output stream");
        sb.append("</span>");
        sb.append("</th>");
        sb.append("</tr>");
        sb.append("<tr>");
        sb.append("<td>");
        sb.append(scriptErrorMessage);
        sb.append("</td>");
        sb.append("<td>");
        sb.append(output.replace("\n", "<br>"));
        sb.append("</td>");
        sb.append("</tr>");
        sb.append("</table>");
        sb.append("</td>");
        sb.append("</tr>");
    }

    private String htmlSurrounding() {
        StringBuilder web = new StringBuilder();
        web.append(htmlStart());
        web.append(sb.toString());
        web.append(htmlEnd());
        return web.toString();
    }
}
