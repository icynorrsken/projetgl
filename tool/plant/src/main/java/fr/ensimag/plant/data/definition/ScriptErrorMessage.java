package fr.ensimag.plant.data.definition;
/**
 * Created by Tom on 26/6/15.
 */
public enum ScriptErrorMessage {
    SYNTAX_INVALID_STATE_2("Unexpected success"),
    SYNTAX_VALID_STATE_2("Unexpected failure"),
    CONTEXT_VALID_STATE_2("Unexpected failure"),
    CONTEXT_INVALID_STATE_2("Unexpected success"),
    DECAC_STATE_2("File compilation failed"),
    DECAC_STATE_3("Unexpected ima output"),
    DECOMPILE_STATE_2("File decompilation failed"),
    DECOMPILE_STATE_3("File compilation failed"),
    DECOMPILE_STATE_4("Decompiled file compilation failed"),
    DECOMPILE_STATE_5("Mismatch between assembly files"),
    DEFAULT("The script itself raise an error."),
    UNKNOWN_STATUS(
            "Unknown error, please have a look on the script or contact " +
            "developer for an upgrade of Plant"),
    UNKNOWN_RUNNER("Unknown runner, please contact Plant's developer");

    private static final ScriptErrorMessage[] SYNTAX    =
            new ScriptErrorMessage[]{SYNTAX_INVALID_STATE_2,
                                     SYNTAX_VALID_STATE_2};
    private static final ScriptErrorMessage[] CONTEXT   =
            new ScriptErrorMessage[]{CONTEXT_INVALID_STATE_2,
                                     CONTEXT_VALID_STATE_2};
    private static final ScriptErrorMessage[] DECAC     =
            new ScriptErrorMessage[]{DECAC_STATE_2, DECAC_STATE_3};
    private static final ScriptErrorMessage[] DECOMPILE =
            new ScriptErrorMessage[]{DECOMPILE_STATE_2, DECOMPILE_STATE_3,
                                     DECOMPILE_STATE_4, DECOMPILE_STATE_5};

    private String message;

    ScriptErrorMessage(String message) {
        this.message = message;
    }

    /**
     * Method call if a script is not generic (valid and invalid script are
     * differents)
     *
     * @param scope
     * @param exitStatus
     * @param valid
     *
     * @return
     */
    public static String getErrorMessage(final RunnerScope scope,
                                         final int exitStatus,
                                         final boolean valid) {
        if (exitStatus == 1)
            return DEFAULT.getMessage();
        switch (scope) {
            case SYNTAX:
                if (valid) {
                    switch (exitStatus) {
                        case 2:
                            return SYNTAX_VALID_STATE_2.getMessage();
                        default:
                            return UNKNOWN_STATUS.getMessage();
                    }
                } else {
                    switch (exitStatus) {
                        case 2:
                            return SYNTAX_INVALID_STATE_2.getMessage();
                        default:
                            return UNKNOWN_STATUS.getMessage();
                    }
                }
            case CONTEXT:
                if (valid) {
                    switch (exitStatus) {
                        case 2:
                            return CONTEXT_VALID_STATE_2.getMessage();
                        default:
                            return UNKNOWN_STATUS.getMessage();
                    }
                } else {
                    switch (exitStatus) {
                        case 2:
                            return CONTEXT_INVALID_STATE_2.getMessage();
                        default:
                            return UNKNOWN_STATUS.getMessage();
                    }
                }
            default:
                return UNKNOWN_RUNNER.getMessage();
        }
    }

    /**
     * Method call if a script is generic (valid and invalid script are both the
     * same)
     *
     * @param scope
     * @param exitStatus
     *
     * @return
     */
    public static String getErrorMessage(final RunnerScope scope,
                                         final int exitStatus) {
        if (exitStatus == 1)
            return DEFAULT.getMessage();
        switch (scope) {
            case DECAC:
                switch (exitStatus) {
                    case 2:
                        return DECAC_STATE_2.getMessage();
                    case 3:
                        return DECAC_STATE_3.getMessage();
                    default:
                        return UNKNOWN_STATUS.getMessage();
                }
            case DECOMPILE:
                switch (exitStatus) {
                    case 2:
                        return DECOMPILE_STATE_2.getMessage();
                    case 3:
                        return DECOMPILE_STATE_3.getMessage();
                    case 4:
                        return DECOMPILE_STATE_4.getMessage();
                    case 5:
                        return DECOMPILE_STATE_5.getMessage();
                    default:
                        return UNKNOWN_STATUS.getMessage();
                }
            case OPTIONS:
                switch (exitStatus) {
                    default:
                        return UNKNOWN_STATUS.getMessage();
                }
            default:
                return UNKNOWN_RUNNER.getMessage();
        }
    }

    /**
     * Gets message
     *
     * @return the message's value
     */
    public String getMessage() {
        return message;
    }
}
