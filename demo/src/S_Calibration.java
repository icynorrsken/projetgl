import static shim.JavaShim.*;

class S_Calibration extends State {
    void enter() {
        clear();
        reset();
        setBrightColor(1);
        setCursor(1, 3);
        println("       POUR NE PAS SPOILER NOTRE SOUTENANCE...");
        println("       MERCI DE FAIRE CONTROL-C SI CE N'EST   ");
        println("       PAS ENCORE LE 1ER JUILLET 2015 !!!!    ");
        reset();
        setCursor(1, 9);
        println("   ************** CALIBRAGE *************");
        println();
        println("   La démo nécessite un terminal de");
        println("   ", SW, " colonnes par ", SH, " lignes.");
        println("   Recommandation uxterm: fg=white bg=black");
        println();
        println("   Le cadre doit être entièrement visible");
        println("   pour que la démo se déroule sans artefact");
        println("   visuel.");
        println();
        println("   Une fois que vous êtes prêt,");
        println("   tapez un nombre puis ENTRÉE (pour débloquer RINT),");
        print("   ou CTRL-C pour abandonner.");
        saveCursor();

        drawFrame();

        unsaveCursor();

        duration = 0;
    }

    void tick() {
        pause();
    }
}
