import static shim.JavaShim.print;
import static shim.JavaShim.println;

class HelixSine {
    int center;
    int prevX;
}

class S_Helix extends State {
    HelixSine sine1 = new HelixSine();
    HelixSine sine2 = new HelixSine();
    boolean underneath;

    void enter() {
        sine1.center = SW/2;
        sine2.center = SW/2;
        duration = 325;

        cycles = -2*SH;
        while (cycles < 0) {
            tick();
            cycles = cycles + 1;
        }
    }

    void plot(HelixSine sine, int thickness, int cy, float maxAmp) {
        float amp = maxAmp * (1 + 0.5f * math.sin(cy * 0.1f)) - thickness;
        int x = (int)(sine.center + amp*math.sin(cy * 0.2f));
        int min = math._min(x, sine.prevX);
        int max = math._max(x, sine.prevX);

        saveCursor();

        if (min > 0) {
            forward(min);
        }
        while (min <= max+thickness) {
            print("█");
            min = min + 1;
        }

        sine.prevX = x;

        unsaveCursor();
    }

    void drawLink() {
        int min = math._min(sine1.prevX, sine2.prevX) + 5;
        int max = math._max(sine1.prevX, sine2.prevX);

        saveCursor();
        setBrightColor(7);
        forward(min);
        while (min < max) {
            print("▀");
            min = min + 1;
        }
        unsaveCursor();
    }

    void tick() {
        float maxAmpMul = 1;

        println();
        if (cycles % 16 < 8) {
            saveCursor();
            home();
            println("100% REALTIME!");
			unsaveCursor();
		}

		if (cycles < 0) {
			maxAmpMul = 0;
		} else if (cycles < 75) {
			maxAmpMul = cycles / 75.0f;
		} else if ((cycles+120) % 16 == 0) {
			underneath = !underneath;
		}

		if (cycles % 3 == 0) {
			drawLink();
		}
		if (underneath) {
			setBrightColor(4);
			plot(sine2, 8, 5 + cycles / 2, maxAmpMul * SW / 4);
			setBrightColor(3);
			plot(sine1, 8, cycles, maxAmpMul * SW / 3);
		} else {
			setBrightColor(3);
			plot(sine1, 8, cycles, maxAmpMul * SW / 3);
			setBrightColor(4);
			plot(sine2, 8, 5 + cycles / 2, maxAmpMul * SW / 4);
		}

	}
}
