class S_Ensimag extends State {
    protected EnsimagStripes stripes = new EnsimagStripes();

    void enter() {
        EnsimagText text = new EnsimagText();

        clear();
        reset();
        setBrightColor(0); text.printChannel0();
        setBrightColor(2); text.printChannel2();
        setBrightColor(1); stripes.printChannel1();
        setBrightColor(2); stripes.printChannel2();
        setBrightColor(3); stripes.printChannel3();
        setBrightColor(4); stripes.printChannel4();
        setBrightColor(5); stripes.printChannel5();
        setBrightColor(6); stripes.printChannel6();

        duration = 100;
    }

    float cycleRateAccel = 0;
    float cycleRate = 0;

    void tick() {
        int offset = (int)(cycleRate);

        if (cycles < 25) {
            // no op
        } else {
            setBrightColor(1 + ((1 + offset) % 6)); stripes.printChannel1();
            setBrightColor(1 + ((2 + offset) % 6)); stripes.printChannel2();
            setBrightColor(1 + ((3 + offset) % 6)); stripes.printChannel3();
            setBrightColor(1 + ((4 + offset) % 6)); stripes.printChannel4();
            setBrightColor(1 + ((5 + offset) % 6)); stripes.printChannel5();
            setBrightColor(1 + ((6 + offset) % 6)); stripes.printChannel6();

            cycleRateAccel = cycleRateAccel + 0.008f;
            cycleRate = cycleRate + cycleRateAccel;
        }
    }
}
