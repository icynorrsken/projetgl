import static shim.JavaShim.*;


class Framebuffer {
    IntArrayInterface ai = new IntArrayInterface();
    Object buf;
    int w;
    int h;
    int words;
    Math math = new Math();

    void init(int w, int h) {
        this.w = w;
        this.h = h;
        words = w*h;
        buf = ai.alloc(words);
        fill(0);
    }

    void fill(int word) {
        ai.fill(buf, word, words);
    }

    int get(int x, int y) {
        return ai.get(buf, y*w + x);
    }

    void rawplot(int x, int y, int word) {
        ai.put(buf, word, y*w + x);
    }

    void display() {
        int y = 0;
        int i = 0;
        int x;
        int raw;
        print("[1;1H");
        while (y < h) {
            x = 0;
            while (x < w) {
                raw = ai.get(buf, i);
                if      (raw ==  0) { print(" "); }
                else if (raw ==  1) { print("▀"); }
                else if (raw ==  2) { print("▄"); }
                else if (raw ==  3) { print("█"); }
                else if (raw ==  8) { print("░"); }
                else if (raw ==  9) { print("▒"); }
                else if (raw == 10) { print("▓"); }
                else if (raw == 11) { print("█"); }
                else                { print("?"); }
                x = x + 1;
                i = i + 1;
            }
            if (y < h-1) {
                println();
            }
            y = y + 1;
        }
    }

    void doubleDisplay() {
        int y = 0;
        int i = 0;
        int x;
        int raw;
        print("[1;1H");
        while (y < h) {
            x = 0;
            while (x < w) {
                raw = ai.get(buf, i);
                if      (raw ==  0) { print("  "); }
                else if (raw ==  1) { print("▀▀"); }
                else if (raw ==  2) { print("▄▄"); }
                else if (raw ==  3) { print("██"); }
                else if (raw ==  8) { print("░░"); }
                else if (raw ==  9) { print("▒▒"); }
                else if (raw == 10) { print("▓▓"); }
                else if (raw == 11) { print("██"); }
                else                { print("??"); }
                x = x + 1;
                i = i + 1;
            }
            if (y < h-1) {
                println();
            }
            y = y + 1;
        }
    }

    void hiResPlot(int x, int y) {
        // TODO: ca pourrait etre accelere en shufflant les offsets des glyphs
        boolean hi = y%2==0;
        int idx = w*(y/2) + x;
        int c = ai.get(buf, idx);
        if (c == 3 || (c == 2 && hi) || (c == 1 && !hi)) {
            ai.put(buf, 3, idx);
        } else {
            ai.put(buf, 1 + (y%2), idx);
        }
    }

    void line(int x1, int y1, int x2, int y2) {
        boolean keepGoing = true;

        // delta of exact value and rounded value of the dependant variable
        int d = 0;

        int dy = math._abs(y2 - y1);
        int dx = math._abs(x2 - x1);

        int dy2 = dy * 2; // slope scaling factors to avoid floating
        int dx2 = dx * 2; // point

        int ix; // = x1 < x2 ? 1 : -1; // increment direction
        int iy; //= y1 < y2 ? 1 : -1;

        if (x1 < x2) {ix = 1;} else {ix = -1;}
        if (y1 < y2) {iy = 1;} else {iy = -1;}

        if (dy <= dx) {
            while (keepGoing) {
                hiResPlot(x1, y1);
                if (x1 == x2) {
                    keepGoing = false;
                }
                x1 = x1 + ix;
                d = d + dy2;
                if (d > dx) {
                    y1 = y1 + iy;
                    d = d - dx2;
                }
            }
        } else {
            while (keepGoing) {
                hiResPlot(x1, y1);
                if (y1 == y2) {
                    keepGoing = false;
                }
                y1 = y1 + iy;
                d = d + dx2;
                if (d > dy) {
                    x1 = x1 + ix;
                    d = d - dy2;
                }
            }
        }
    }
}
