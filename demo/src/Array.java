import java.util.Arrays;

abstract //JAVA
class ArrayInterface {
    abstract //JAVA
    Object alloc(int size)
    ;/*//JAVA
    asm("
            LOAD -3(LB), R0 ; size
            NEW R0, R0
            BOV USER.Array.alloc_asm.outofmem
            RTS
        USER.Array.alloc_asm.outofmem:
            WSTR \"ARRAY ALLOC: OUT OF MEMORY!!!\"
            WNL
            ERROR
    ");
    *///JAVA
}

class IntArrayInterface extends ArrayInterface {
    /*//JAVA
    void fill(Object addr, int val, int size)
    asm("
        USER.Array.fillint_asm:
            PUSH R2
            LOAD -3(LB), R0 ; addr
            LOAD -4(LB), R1 ; val
            LOAD -5(LB), R2 ; size
        USER.Array.fillint_asm.loop:
            STORE R1, -1(R0, R2)
            SUB #1, R2
            BNE USER.Array.fillint_asm.loop
            POP R2
            RTS
    ");

    void put(Object addr, int val, int index)
    asm("
        USER.Array.putint_asm:
            PUSH R2          ; save nonscratch
            LOAD -3(LB), R0  ; addr
            LOAD -4(LB), R1  ; val
            LOAD -5(LB), R2  ; index
            STORE R1, 0(R0, R2) ; store in array
            POP R2           ; restore nonscratch
            RTS
    ");

    int get(Object addr, int index)
    asm("
        USER.Array.getint_asm:
            LOAD -3(LB), R0    ; addr
            LOAD -4(LB), R1    ; index
            LOAD 0(R0, R1), R0 ; store item in R0 (return value)
            RTS
    ");
    *///JAVA
    /*JAVA*/ Object alloc(int size) { return new int[size]; }
    /*JAVA*/ void fill(Object o, int v, int size) { Arrays.fill((int[])o, 0, size, v); }
    /*JAVA*/ void put(Object o, int v, int i) { ((int[])o)[i] = v; }
    /*JAVA*/ int  get(Object o, int i) { return ((int[])o)[i]; }
}

class FloatArrayInterface extends ArrayInterface {
    /*//JAVA
    void fill(Object a, float v, int sz) asm("BRA USER.Array.fillint_asm");
    void put(Object a, float v, int i)   asm("BRA USER.Array.putint_asm");
    float get(Object a, int i)           asm("BRA USER.Array.getint_asm");
    *///JAVA
    /*JAVA*/ Object alloc(int size) { return new float[size]; }
    /*JAVA*/ void fill(Object o, float v, int size) { Arrays.fill((float[])o, 0, size, v); }
    /*JAVA*/ void put(Object o, float v, int i) { ((float[])o)[i] = v; }
    /*JAVA*/ float get(Object o, int i) { return ((float[])o)[i]; }
}
