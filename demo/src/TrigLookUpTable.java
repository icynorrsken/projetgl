class TrigLookUpTable {
    Math math = new Math();
    FloatArrayInterface ai = new FloatArrayInterface();
    int degrees;
    Object sinLUT;
    Object cosLUT;
    float d2rFact;
    float r2dFact;

    void init(int deg) {
        int i = 0;

        degrees = deg;
        d2rFact = math._TWOPI / degrees;
        r2dFact = degrees / math._TWOPI;

        sinLUT = ai.alloc(deg);
        cosLUT = ai.alloc(deg);

        ai.fill(sinLUT, 0xDEADC.0DEp1f, degrees);
        ai.fill(cosLUT, 0xDEADC.0DEp1f, degrees);

        while (i < deg) {
            ai.put(sinLUT, math.sin(d2rFact * i), i);
            ai.put(cosLUT, math.cos(d2rFact * i), i);
            i = i + 1;
        }
    }

    float sin(float rad) {
        rad = math._fmod(rad, math._TWOPI);
        return ai.get(sinLUT, (int) (r2dFact * rad));
    }

    float cos(float rad) {
        rad = math._fmod(rad, math._TWOPI);
        return ai.get(cosLUT, (int) (r2dFact * rad));
    }

    float fastsin(float rad) {
        return ai.get(sinLUT, (int) (r2dFact * rad));
    }

    float fastcos(float rad) {
        return ai.get(cosLUT, (int) (r2dFact * rad));
    }

}
