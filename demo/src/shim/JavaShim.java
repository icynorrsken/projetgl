package shim;

import java.io.PrintStream;

public class JavaShim {
	private final static PrintStream out;

	static {
		try {
			out = new PrintStream(System.out, true, "UTF-8");
		} catch (Exception e) {
			throw new Error(e);
		}
	}

    public static void print(Object ... args) {
        for (Object o : args) {
            out.print(o);
        }
    }

    public static void println(Object ... args) {
        print(args);
        out.println();
    }
}

