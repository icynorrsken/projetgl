import static shim.JavaShim.print;

class S_Intro extends State {
    void enter() {
        //duration = 50;
        clear();
		new LumberjackLogo().printChannel3();
    }

	// event timestamps
	int evt1 = 30;
	int evt2 = evt1 + 20;
	int evt3 = evt2 + 20;
	int evt4 = evt3 + 20;
	int evt5 = evt4 + 20;
	int evt6 = evt5 + 3;
	int evt7 = evt6 + 20;
	int evt8 = evt7 + 20;

    void tick() {
        if (cycles == evt1) {
            setCursor(40, 5);
            bold();
            print("  LES BÛCHERONS D'AST ❤");
            reset();
        } else if (cycles == evt2) {
            setCursor(40, 6);
            print("      présentent");
        } else if (cycles == evt3) {
            setCursor(40, 7);
            print("     la démo deca");
		} else if (cycles == evt4) {
            setCursor(40, 8);
			print(" la plus ");
		} else if (cycles == evt5) {
			setBrightColor(1);
			print("SE");
		} else if (cycles >= evt6 && cycles < evt7 && cycles % 2 == 0) {
			print("X");
		} else if (cycles == evt7) {
			print("Y");
			reset();
			setCursor(40, 9);
			print("   de tous les temps");
		}
	}
}
