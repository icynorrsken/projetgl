class S_Spiral extends State {

    Framebuffer fb = new Framebuffer();

    void enter() {
        fb.init(SW, SH);
        setBrightColor(1);
        msec = 5;
    }


    float angle = 0.0f;
    float r = SH;

    int px = 0;
    int py = 0;

    void tick() {
        int cx = SW/2;
        int cy = SH;

        int x = (int) (cx + math.cos(angle) * r);
        int y = (int) (cy + math.sin(angle) * r);

        if (cycles > 0) {
            fb.line(px, py, x, y);
        }

        px = x;
        py = y;
        //fb.line(cx+1, cy, x+1, y);
        //fb.hiResPlot(x, y);

        fb.display();

        angle = angle + 0.5f;

        r = r - 0.5f;

        if (cycles == 50) {
            fb.fill(0);
            setBrightColor(6);
        }

        //pause();
    }

    boolean done() {
        return r < -SH;
    }
}
