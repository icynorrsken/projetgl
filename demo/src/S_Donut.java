class S_Donut extends State {
    // This is a monster function to minimize method calls
    // and variable initialization overhead.
    void play() {

        float theta_spacing = 0.30f;
        float phi_spacing   = 0.08f;

        int VPW = 56/2;
        int VPH = 24;
        float HALF_VPW_F = VPW/2.0f;
        float HALF_VPH_F = VPH/2.0f;

        float R1 = 0.4f;
        float R2 = 1.0f;
        float K2 = 5.0f;
        // Calculate K1 based on screen size: the maximum x-distance occurs roughly at
        // the edge of the torus, which is at x=R1+R2, z=0.  we want that to be
        // displaced 3/8ths of the width of the screen, which is 3/4th of the way from
        // the center to the side of the screen.
        // screen_width*3/8 = K1*(R1+R2)/(K2+0)
        // screen_width*K2*3/(8*(R1+R2)) = K1
        float K1 = VPW*K2*3/(8*(R1+R2)) * 0.90f;

        float lumiFact = 8.0f*4.0f/12.0f; // this initially brings L into the range 0..11 (8*sqrt(2) = 11.3), then into 0..3

        TrigLookUpTable trigLUT = new TrigLookUpTable();

        Framebuffer fb = new Framebuffer();
        IntArrayInterface ai;
        Object buf;

        // precompute sines and cosines of A and B
        float cosA = 0, sinA = 0;
        float cosB = 0, sinB = 0;

        float theta;
        float cosTh, sinTh;
        float phi, cosphi, sinphi;
        float circlex, circley;
        float x, y, z;
        int lumi;

        // let's precompute EVERYTHING
        float cosTh_cosA;
        float cosTh_sinB;
        float cosB_sinA_cosTh;
        float cosB_cosA_sinTh_minus_sinTh_sinA;
        float sinA_sinB;
        float sinA_cosB;
        float cx_cosA;
        float cy_cosA_sinB;
        float cy_cosA_cosB;
        float K2_plus_cy_sinA;

        float twopi = math._TWOPI;

        int pixel_idx;
        int cycles = 0;

        float A;
        float B;


        //=================================================================
        // ENTER
        //=================================================================

        fb.init(VPW, VPH);
        ai = fb.ai;
        buf = fb.buf;

        trigLUT.init(1000);

        reset();
        home();
        new DunkinSideLogo().printComposite();

        reset();
        setBrightColor(3);

        //==========================

        while (cycles < 150) {
            A = cycles / 5.0f;
            B = cycles / 10.0f;
            cosA = trigLUT.cos(A);
            sinA = trigLUT.sin(A);
            cosB = trigLUT.cos(B);
            sinB = trigLUT.sin(B);

            fb.fill(0);

            theta = 0.0f;

            // theta goes around the cross-sectional circle of a torus
            while (theta < twopi) {
                // precompute sines and cosines of theta
                cosTh = trigLUT.fastcos(theta);
                sinTh = trigLUT.fastsin(theta);

                // the x,y coordinate of the circle, before revolving (factored out of the above equations)
                circlex = R2 + R1*cosTh;
                circley = R1*sinTh;

                cosTh_cosA = cosTh * cosA;
                cosTh_sinB = cosTh * sinB;
                cosB_sinA_cosTh = cosB * sinA * cosTh;
                cosB_cosA_sinTh_minus_sinTh_sinA = cosB*cosA*sinTh - sinTh*sinA;
                sinA_sinB = sinA * sinB;
                sinA_cosB = sinA * cosB;
                cx_cosA = circlex * cosA;
                cy_cosA_sinB = circley * cosA * sinB;
                cy_cosA_cosB = circley * cosA * cosB;
                K2_plus_cy_sinA = K2 + circley * sinA;

                // phi goes around the center of revolution of a torus
                phi = 0;
                while (phi < twopi) {
                    // precompute sines and cosines of phi
                    cosphi = trigLUT.fastcos(phi);
                    sinphi = trigLUT.fastsin(phi);

                    // final 3D (x,y,z) coordinate after rotations, directly from our math above
                    x = circlex*(cosB*cosphi + sinA_sinB*sinphi) - cy_cosA_sinB;
                    y = circlex*(sinB*cosphi - sinA_cosB*sinphi) + cy_cosA_cosB;
                    z = K2_plus_cy_sinA + cx_cosA*sinphi;

                    // x and y projection.  note that y is negated here, because y goes up in
                    // 3D space but down on 2D displays.
                    pixel_idx =
                            (int) (HALF_VPH_F - K1/z*y) * VPW +
                                    (int) (HALF_VPW_F + K1/z*x);

                    // calculate luminance.  ugly, but correct.
                    lumi = 8 + (int)(lumiFact * (cosphi*cosTh_sinB - sinphi*cosTh_cosA
                            + cosB_cosA_sinTh_minus_sinTh_sinA
                            - cosB_sinA_cosTh*sinphi));
                    // L ranges from -sqrt(2) to +sqrt(2).  If it's < 0, the surface is
                    // pointing away from us, so we won't bother trying to plot it.
//                    // test against the z-buffer.  larger 1/z means the pixel is closer to
//                    // the viewer than what's already plotted.
                    if (lumi >= 8 && lumi > ai.get(buf, pixel_idx)) {
                        ai.put(buf, lumi, pixel_idx);
                    }
                    phi = phi + phi_spacing;
                }

                theta = theta + theta_spacing;
            }

            fb.doubleDisplay();
            cycles = cycles +  1;
        }

    }
}
