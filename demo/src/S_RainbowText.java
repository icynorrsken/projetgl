import static shim.JavaShim.print;

class S_RainbowText extends State {
    int offset = 0;

    void enter() {
        clear();

        setCursor(24, 23);
        print("© 2015 BÛCHERONS D'AST - 1AA ENSIMAG");
    }

    void sbr(int x) {
        setCursor(x, 10+offset);
        setBrightColor(1 + ((cycles/5 + offset) % 6));
        offset = offset + 1;
    }

    void printUltimate(int x) {
        sbr(x); print("   ██  ██ ▀██  ██     ▀▀                ██            ");
        sbr(x); print("   ██  ██  ██  ██▀▀  ▀██  ▄█▄██▄ ▄█▀▀█▄ ██▀▀ ▄█▀▀█▄   ");
        sbr(x); print("   ██  ██  ██  ██     ██  █ █ ██ ██  ██ ██   ██▀▀▀▀   ");
        sbr(x); print("    ▀▀▀▀   ▀▀▀▀ ▀▀▀▀ ▀▀▀▀ ▀ ▀ ▀▀  ▀▀▀ ▀▀ ▀▀▀▀ ▀▀▀▀▀   ");
    }

    void printSwagger(int x) {
        sbr(x); print("   ▄█▀▀█▄                                             ");
        sbr(x); print("   ▀█▄▄▄  █ █ ██ ▄█▀▀█▄ ▄█▀▀█▄ ▄█▀▀█▄ ▄█▀▀█▄ ██▀▀█▄   ");
        sbr(x); print("   ▄▄  ██ █▄█▄██ ██  ██ ▀█▄▄██ ▀█▄▄██ ██▀▀▀▀ ██       ");
        sbr(x); print("    ▀▀▀▀   ▀ ▀▀   ▀▀▀ ▀▀▄▄▄▄█▀ ▄▄▄▄█▀  ▀▀▀▀▀ ▀▀       ");
    }

    void tick() {
        float slideFact = 0.0f;

        offset = 0;
        drawFrame();

        if (cycles >= 50 && cycles < 100) {
            slideFact = (cycles - 50) / 50.0f;
        } else if (cycles >= 100) {
            slideFact = 1.0f;
        }

        printUltimate((int)(15.0 + slideFact * 12.0 * math.sin(cycles * 0.1f)));
        printSwagger((int) (15.0 - slideFact * 12.0 * math.sin(cycles * 0.1f)));
        home();
    }
}
