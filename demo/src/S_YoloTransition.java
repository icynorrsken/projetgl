import static shim.JavaShim.*;

class S_YoloTransition extends State {
    int state = 0;
    int rows = 0;
    int cols = 0;

    void enter() {
        home();
        reset();
        msec = 3;
    }

    void tick() {
        if (state == 0) {
            if ((rows/3) % 2 == 0) {
                reverse();
                print("#YOLO");
                reset();
                print("     ");
            } else {
                print("     ");
                reverse();
                print("#YOLO");
                reset();
            }
        } else {
            print("          ");
        }

        cols = cols + 10;
        if (cols >= SW) {
            if (rows < SH - 1) {
                println();
            }
            cols = 0;
            rows = rows + 1;
        }
        if (rows >= SH) {
            rows = 0;
            cols = 0;
            home();
            state = state + 1;
        }
    }

    boolean done() {
        return state == 2;
    }
}
