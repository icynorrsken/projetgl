import java.util.Scanner;

import static shim.JavaShim.*;

class State {
    static //JAVA
    int DEFAULT_MSEC = 40;

    Math math = new Math();
    int cycles = 0;
    int duration = 200;
    int msecMultiplier = 300;
    int msec = DEFAULT_MSEC;

    int SW = 80;
    int SH = 24;

    void play() {
        cycles = 0;
        enter();
        while (!done()) {
            tick();
            sleep();
            cycles = cycles + 1;
        }
    }

    void sleep() {
        try { Thread.sleep(msec); } catch (Exception ex) { } //JAVA

        /*//JAVA
        int friedEgg = msec * msecMultiplier;
        while (friedEgg >= 0) {
            friedEgg = friedEgg - 1;
        }
        *///JAVA
    }

    boolean done() {
        return cycles > duration;
    }

    void enter() {
        // nothing, to override
    }

    void tick() {
        // nothing, to override
    }

    void clear() {
        print("[2J[1;1H");
    }

    void highlight() {
        print("[1;4;5m");
    }

    void bold() {
        print("[1m");
    }

    void reverse() {
        print("[7m");
    }

    void reset() {
        print("[0m");
    }

    void setCursor(int x, int y) {
        print("[",y,";",x,"H");
    }

    void home() {
        print("[H");
    }

    void forward(int c) {
        print("[",c,"C");
    }

	void saveCursor() {
        print("[s");
	}

	void unsaveCursor() {
        print("[u");
	}

    void setBrightColor(int c) {
        print("[1;3", c, "m");
    }

    void setDimColor(int c) {
        print("[2;3", c, "m");
    }

    // don't use readInt() to bypass auto-generated BOV
    void pause()
    /*//JAVA
    asm("
        RINT
        RTS
    ");
    *///JAVA
    { new Scanner(System.in).nextLine(); } //JAVA

    void drawFrame() {
        int i = 2;
        home();
        println("█▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀█");
		while (i < SH) {
            println("█[", i, ";80H█");
			i = i + 1;
        }
        print("█▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄█");
    }
}
