class EventSequenceState extends State {
    Event e;

    void tick() {
        if (e.progress == 0) {
            e.reset();
            e.init(this);
        }
        e.tick();
        e.progress = e.progress + 1;
        if (e.progress == e.duration()) {
            e = e.next();
        }
    }

    boolean done() {
        return e == null;
    }
}
