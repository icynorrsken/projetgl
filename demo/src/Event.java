class Event {
    State state;
    int progress;

    int duration() {
        return 10;
    }

    float progressRatio() {
        return progress/(float)(duration());
    }

    Event next() {
        return null;
    }

    void init(State s) {
        state = s;
    }

    void tick() {
        // override
    }

    void reset() {
        progress = 0;
    }
}
