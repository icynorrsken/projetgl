class Math {

    float _PI = 3.14159265358979323846264338327950288419716939937510582f;
    float _TWOPI = 2*_PI;
    float _THREEPI = 3*_PI;
    float _THREE_HALFPI = 3*_PI/2;
    float _HALFPI = _PI/2;
    int _RAND_MAX = 65535;
    float _RAND_MAX_F = _RAND_MAX;
    // use TrigLookupTable if you can't afford hardcore precision
    float _FPREC = 0.000001f;

    protected int _lfsr = 1;


    int _min(int a, int b) {
        if (a < b) {
            return a;
        } else {
            return b;
        }
    }

    int _max(int a, int b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }

    int _abs(int x) {
        if (x < 0) {
            return -x;
        } else {
            return x;
        }
    }

    float _fabs(float x) {
        if (x < 0) {
            return -x;
        } else {
            return x;
        }
    }

    void seed(int seed) {
        _lfsr = seed;
    }

    // when dividing by two doesn't cut it
    // (dividing a negative integer by two will nuke your bit operations)
    // TODO: tester l'asm!!!
    int _shr(int n, int a)
    { return a >> n; } /*//JAVA
    asm("
        load -4(lb), r0  ; a
        load -3(lb), r1  ; n
    user.math._shr.loop:
        beq user.math._shr.end
        shr r0
        sub #1, r1
    user.math._shr.end:
        rts
    ");
    *///JAVA

    // 16-bit Galois LFSR
    // we don't use 32-bit because the sign bit can be a pain
    int _rand() {
        int magic = 0;
        if (0 != _lfsr % 2) {
            magic = 46080; //0xB400;
        }
        _lfsr = _xor(16, _shr(1, _lfsr), magic);
        return _lfsr;
    }

    float _frand() {
        return _rand() / _RAND_MAX_F;
    }

    float _fmod(float a, float b) {
        return a - b * (int)(a/b);
    }

    float cos(float x) {
        float mul = 1;
        int i = 1;
        float sum = 1.0f;
        float factor = 1.0f;

        x = _fmod(x, _TWOPI); //x %= 2*_PI;
        if (x > _THREE_HALFPI) {
            x = _THREEPI - x;
            mul = -1;
        } else if (x > _PI) {
            x = x - _PI;
            mul = -1;
        } else if (x > _HALFPI) {
            x = _PI - x;
            mul = -1;
        }

        while (_fabs(factor / sum) > _FPREC) {
            factor = -factor * x * x / (2*i * (2*i-1));
            sum = sum + factor;
            i = i + 1;
        }
        return sum * mul;
    }

    // TODO perf: cache 2*i
    float sin(float x) {
        float mul = 1;
        int i = 1;
        float sum;
        float factor;

        x = _fmod(x, _TWOPI); //x %= 2*_PI;

        if (x > _THREE_HALFPI) {
            x = _TWOPI - x;
            mul = -1;
        } else if (x > _PI) {
            x = x - _PI;
            mul = -1;
        } else if (x > _HALFPI) {
            x = _PI - x;
        }

        if (x == 0) {
            // prevent division by zero
            return 0;
        }

        factor = x;
        sum = x;

        while (_fabs(factor / sum) > _FPREC) {
            factor = -factor * x * x / (2*i * (2*i+1));
            sum = sum + factor;
            i = i + 1;
        }
        return sum * mul;
    }

    // TODO perf: cache 2i+1
    float asin(float x) {
        int i = 1;
        float sum = x;
        float factor = x;
        //while (_fabs(factor/sum) > _FPREC) {
        while (i < 100000000) {
            factor = factor * x * x * i / ((i+1)*(2*i+1));
            sum = sum + factor;
            i = i + 1;
        }
        return sum;
    }

    // TODO: wrong results with very large floats
    float ulp(float x) {
        float u = 1.0f;
        float pu = u;
        while (x != x + u) {
            pu = u;
            u = u / 2.0f;
        }
        return u;
    }

    // XOR without using bitwise operations
    int _xor(int counter, int a, int b) {
        int result = 0;
        int bit = 1;

        boolean qa, qb, x;

        while (counter > 0) {
            qa = 0 != (a % 2);
            qb = 0 != (b % 2);
            x = (qa && !qb) || (!qa && qb);
            if (x) {
                result = result + bit;
            }
            bit = bit * 2;       // TODO SHL #1
            a = _shr(1, a);
            b = _shr(1, b);
            counter = counter - 1;
        }

        return result;
    }
}
