class S_Star extends State {
    Framebuffer fb = new Framebuffer();
    TrigLookUpTable trigLUT = new TrigLookUpTable();

    void enter() {
        fb.init(SW, SH);
        trigLUT.init(1000);
        msec = 10;
        duration = 300;
    }

    float initialAngle = math._PI*2/5/2;
    float radiusPhase = 0;
    float baseRadius = 6;

    void tick() {
        int cx = SW/2;
        int cy = SH;

        int branches = 5;

        float increment = math._TWOPI/branches;

        float outerTheta = initialAngle;
        float outerRadius = baseRadius - 6 * trigLUT.sin(radiusPhase);
        float innerTheta = initialAngle + increment/2;
        float innerRadius = baseRadius + 6 * trigLUT.sin(radiusPhase);

        int i = 0;
        float p;

        fb.fill(0);

        while (i < branches) {
            fb.line(
                    (int) (cx + outerRadius * trigLUT.cos(outerTheta)),
                    (int) (cy + outerRadius * trigLUT.sin(outerTheta)),
                    (int) (cx + innerRadius * trigLUT.cos(innerTheta)),
                    (int) (cy + innerRadius * trigLUT.sin(innerTheta))
            );
            outerTheta = outerTheta + increment;
            fb.line(
                    (int) (cx + innerRadius * trigLUT.cos(innerTheta)),
                    (int) (cy + innerRadius * trigLUT.sin(innerTheta)),
                    (int) (cx + outerRadius * trigLUT.cos(outerTheta)),
                    (int) (cy + outerRadius * trigLUT.sin(outerTheta))
            );
            innerTheta = innerTheta + increment;
            i = i + 1;
        }

        fb.display();

        if (cycles >= 20 && cycles < 70) {
            p = (cycles-20)/50.0f;
            initialAngle = initialAngle + 0.35f * (1-p);
            baseRadius = 6.0f + 9.0f*p;
        } else if (cycles >= 70 && cycles < 135) {
            initialAngle = initialAngle + 0.02f;
        } else if (cycles >= 200) {
            baseRadius = baseRadius - 0.13f;
            initialAngle = initialAngle + 0.05f;
        }

        if (cycles > 20) {
            radiusPhase = radiusPhase + 0.1f;
        }

        setBrightColor((cycles/15) % 7);
    }
}

