import static shim.JavaShim.*;

class E_BragBase extends Event {
    void init(State s) {
        state = s;
        this.enter();
    }

    void enter() {
        // override
    }

    int duration() {
        return 30;
    }

    void printPas() {
        state.setBrightColor(1);
        print("PAS");
        state.reset();
    }
}

class E_Brag0 extends E_BragBase {
    Event next() { return new E_Brag1(); }
    void enter() {
        state.drawFrame();
    }
}

class E_Brag1 extends E_BragBase {
    Event next() { return new E_Brag2(); }
    void enter() {
        state.setCursor(20, 8);
        printPas();
        println(" de type tableau ?");
    }
}
class E_Brag2 extends E_BragBase {
    Event next() { return new E_Brag3(); }
    void enter() {
        state.setCursor(20, 10);
        printPas();
        println(" d'opérations bit à bit ?");
    }
}
class E_Brag3 extends E_BragBase {
    Event next() { return new E_Brag4(); }
    void enter() {
        state.setCursor(20, 12);
        printPas();
        println(" de graphisme ?");
    }
}

class E_Brag4 extends E_BragBase {
    int duration() { return 50; }
    void enter() {
        state.setCursor(20, 14);
        printPas();
        state.setBrightColor(1);
        println(" DE PROBLÈME");
    }
}

class S_Brag extends EventSequenceState {
    void enter() {
        clear();
        e = new E_Brag0();
        msec = 25;
    }
}
