import static shim.JavaShim.*;

class Demo {
    void play() {
        State trans = new S_Transition();
        new S_Calibration().play();
        new S_Intro().play();
        trans.play();
        new S_RainbowText().play();
        trans.play();
        new S_Helix().play();
        trans.play();
        new S_Spiral().play();
        trans.play();
        new S_Brag().play();
        new S_YoloTransition().play();
        new S_Star().play();
        trans.play();
        new S_Ensimag().play();
        trans.play();
        new S_Wave().play();
        trans.play();
        new S_Dunkin().play();
        new S_Donut().play();
        trans.play();
        println("Bonne journée :)");
    }

/*JAVA*/    public static void main(String[] args) {
/*JAVA*/        if (args.length > 0 && args[0].equals("fast")) {
/*JAVA*/            State.DEFAULT_MSEC = 1;
/*JAVA*/        }
/*JAVA*/        new Demo().play();
/*JAVA*/    }
}

