# coding: utf-8

import PIL.Image
import codecs
import sys
import time

stdout = codecs.getwriter('utf8')(sys.stdout)

FORMAT = sys.argv[1]
MODE = sys.argv[2]
PATH = sys.argv[3]

composite = ""
channels = 8 * [""]

image = PIL.Image.open(PATH)
pix = image.load()


if MODE.startswith("channels"):
	numchars = MODE[len("channels"):]

	for ch in numchars:
		c = int(ch)

		for y in range(0, image.size[1], 2):
			jump = True

			for x in range(0, image.size[0], 1):
				top = c == pix[x, y]
				bot = c == pix[x, y+1]

				if not top and not bot:
					jump = True
					continue

				if jump:
					channels[c] += '\x1b[' + str(1+y/2) + ';' + str(1+x) + 'H'
					jump = False

				if top and bot:
					channels[c] += u'█'
				elif top:
					channels[c] += u'▀'
				elif bot:
					channels[c] += u'▄'
				else: print("SHOULDN'T HAPPEN!!!")

		if FORMAT == "preview":
			stdout.write(channels[c])

elif MODE == 'composite':
	color = (0, 7) # foreground, background

	for y in range(0, image.size[1], 2):
		composite += '\x1b[0;3' + str(color[0]) + ';4' + str(color[1]) + 'm'
		lastLine = y == image.size[1] - 2

		for x in range(0, image.size[0], 1):
			top = pix[x, y]
			bot = pix[x, y+1]

			if top not in color or bot not in color:
				if top == bot:
					color = (color[0], bot)
				else:
					color = (top, bot)
				composite += '\x1b[0;3' + str(color[0]) + ';4' + str(color[1]) + 'm'

			if top == bot and top == color[0]:
				s = u'█'
			elif top == bot and top == color[1]:
				s = " "
			elif top == color[0]:
				s = u'▀'
			elif top == color[1]:
				s = u'▄'
			else: print("SHOULDN'T HAPPEN!!!")

			composite += s

		composite += "\x1b[0m\n"

	if FORMAT == "preview":
		stdout.write(composite)
		# read line...

else:
	print("UNKNOWN MODE!")
	sys.exit(1)

if FORMAT == 'preview':
	sys.exit(1)

stdout.write("// ANSI halfblock art - " + MODE + "\n")
stdout.write("// generated from: " + PATH + ", on: " + time.strftime("%x %X") + "\n")
stdout.write("import static shim.JavaShim.*;\n")
stdout.write("class " + PATH[PATH.rfind('/')+1 : PATH.rfind('.')] + " {\n")

if MODE[:8] == 'channels':
	for i in range(0, 8):
		if len(channels[i]) == 0:
			continue
		stdout.write("\tvoid printChannel" + str(i) + "() {\n")
		n = 80 - len("        print('');") - 2
		for chunk in [channels[i][j:j+n] for j in range(0, len(channels[i]), n)]:
			stdout.write("\t\tprint(\"")
			stdout.write(chunk)
			stdout.write("\");\n")
		stdout.write("\t}\n\n")
elif MODE == 'composite':
	stdout.write("\tvoid printComposite() {\n")
	chunks = composite.split("\n")
	for i, chunk in enumerate(chunks):
		stdout.write("\t\tprint")
		if i < len(chunks)-2:
			stdout.write("ln")
		stdout.write("(\"")
		stdout.write(chunk)
		stdout.write("\");\n")
	stdout.write("\t}\n\n")

stdout.write("}\n\n")

