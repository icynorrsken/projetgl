// ANSI halfblock art - channels0123456
// generated from: src/EnsimagStripes.png, on: 06/28/15 00:10:24
import static shim.JavaShim.*;
class EnsimagStripes {
	void printChannel1() {
		print("[14;56H██[15;56H██[16;56H██[17;56H██[18;56H██[19;55H▄█");
		print("█[20;55H██[21;55H██[22;54H██▀[23;53H███[24;52H▄██");
	}

	void printChannel2() {
		print("[7;71H▄[8;70H██▄[9;70H▀██[10;71H██[11;71H███[12;71H███");
		print("[13;71H▀██[14;72H██[15;72H██[16;71H███[17;71H██▀");
	}

	void printChannel3() {
		print("[1;46H██▄[2;47H▀█▄[3;48H▀██[4;49H▀██[5;50H▀██[6;51H▀█▀");
	}

	void printChannel4() {
		print("[15;78H▄▄[16;78H██[17;78H██[18;77H▄██[19;77H███[20;77H");
		print("██▀[21;76H▄██[22;76H███");
	}

	void printChannel5() {
		print("[12;49H▄▄[13;49H██[14;50H██[15;50H██[16;50H██[17;50H██");
		print("[18;49H▄██[19;49H██[20;49H██[21;48H██▀[22;48H██");
	}

	void printChannel6() {
		print("[3;61H▄▄[4;60H▀██▄[5;61H███[6;62H██[12;65H██[13;65H██");
		print("[14;65H██[15;65H██[16;64H███[17;64H██[18;63H▄██[19;63H█");
		print("█▀[20;63H▀▀");
	}

}

