#! /bin/sh
# Return codes :
# 0 : Contextual check suceeded as expected
# 1 : Script error if this happens
# 2 : Unexpected failure

file=$1
if test_context "$file" 2>&1 | grep -q -e 'Exception'
then
    echo "$file : Unexpected failure"
    exit 2
else
    echo "$file : OK"
    exit 0
fi

# should never be here
exit 1
