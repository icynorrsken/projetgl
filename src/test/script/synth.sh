#!/usr/bin/env bash

cd "$(dirname "$0")"/../../.. || exit 1

PATH=./src/test/script/launchers:"$PATH"

cd "src/test/deca/syntax"

test_synt_invalide () {
    # $1 = premier argument.
    if test_synt "$1" 2>&1 | grep -q -e "$1:[0-9][0-9]*:"
    then
        echo "$1 : OK"
    else
        echo "Succes inattendu de test_synt sur $1."
    fi
}

test_synt_valide () {
    if test_synt "$1" 2>&1 | \
        grep -q -e ':[0-9][0-9]*:'
    then
        echo "Echec inattendu pour test_synt sur $1"
    else
        echo "$1 : OK"
    fi
}

cd "invalid"

for dossier in *; do
    cd "$dossier"
    #si le dossier n'est pas vide
    if [[ "$(ls -A .)" ]]; then
        for file in *.deca; do
            test_synt_invalide "$file"
        done
        cd ".."
    fi
done

cd "../valid"
for dossier in *; do
    cd "$dossier"

    if [[ "$(ls -A .)" ]]; then
        for file in *.deca; do
            test_synt_valide "$file"
        done
        cd ".."
    fi
done