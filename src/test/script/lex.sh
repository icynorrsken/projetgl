#!/usr/bin/env bash

#/!\ ce script ne test que la lexicogrpahie !!!!
#la syntax comprends le lexer et le parser !!

test_lex_file () {
file=$1
#si le fichier a un expected -> on peut le tester
if [[ -f "${file%.deca}.expected" ]]; then
    #on cherche ce qu'il y a sur dans le fichier expected
    if test_lex "$file" 2> "${file%.deca}.res"; then
        echo "fichier res impossible a creer"
    elif grep -q -e "$(cat ${file%.deca}.expected)" "${file%.deca}.res"; then
        echo "$file : OK"
    else
        echo "Erreur non detectee par test_lex pour $file"
    fi
else
    #cest un fichier valid lexicalement meme si il est invalid
    #syntaxiquement
    if test_lex $file 2>&1 | head -n 1 | grep -q "$file.deca:[0-9]"; then
        echo "Echec inattendu de test_lex sur $file"
    else
        echo "$file : OK"
    fi
fi
}

cd "$(dirname "$0")"/../../.. || exit 1

PATH=./src/test/script/launchers:"$PATH"

cd "src/test/deca/syntax"

cd "invalid"

for dossier in *; do
    cd "$dossier"
    rm -f *.res || exit 1
    #si le dossier n'est pas vide
    if [[ "$(ls -A .)" ]]; then
        for file in *.deca; do
            test_lex_file "$file"
        done
        #on ressort du dossier apres avoir traité tous les fichiers
        cd ".."
    fi
done

cd "../valid"
for dossier in *; do
    cd "$dossier"

    if [[ "$(ls -A .)" ]]; then
        for file in *.deca; do
            test_lex_file "$file"
        done
        cd ".."
    fi
done
