#! /bin/sh

#prends un .deca en parametre qui doit avoir un .expected
#avec le meme nom dans le repertoire courant
file=$1
if test_lex "$file" 2> "${file%.deca}.res"; then
    echo "fichier res impossible a creer"
elif grep -q -e "$(cat ${file%.deca}.expected)" "${file%.deca}.res"; then
    echo "$file : OK"
    exit 0
else
    echo "Erreur non detectee par test_lex pour $file"
    exit 1
fi
