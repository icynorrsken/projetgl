#! /bin/sh
# Return codes :
# 0 : Decompilation OK
# 1 : Script error if this happens
# 2 : File decompilation failed
# 3 : File compilation failed
# 4 : Decompiled file compilation failed
# 5 : Mismatch between assembly files

cleanup() {
    rm -f "$original_ass"
    rm -f "$decompiled_ass"
    rm -f "$decompiled_file"
    exit $1
}

file=$1
decompiled_file=${file%.deca}.decompiled.deca
original_ass=${file%.deca}.ass
decompiled_ass=${file%.deca}.decompiled.ass


decac -p "$file" > $decompiled_file || cleanup 2

decac "$file" || cleanup 3
if [ ! -f "$original_ass" ]; then
    echo "$file compilation failed"
    cleanup 3
fi

decac "$decompiled_file" || cleanup 4
if [ ! -f "$decompiled_ass" ]; then
    echo "$decompiled_file compilation file"
    cleanup 4
fi

if diff "$decompiled_ass" "$original_ass"; then
    echo "$file : OK"
    cleanup 0
else
    echo "Mismatch between $file assembly files"
    cleanup 5
fi

# should never be here
cleanup 1

