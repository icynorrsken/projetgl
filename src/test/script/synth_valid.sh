#! /bin/sh
# Return codes :
# 0 : Syntax check suceeded as expected
# 1 : Script error if this happens
# 2 : Unexpected failure

#prend un fichier .deca en parametre et test la syntax

file=$1
if test_synt "$file" 2>&1 | grep -q -e ':[0-9][0-9]*:'
then
    echo "$file : Unexpected failure"
    exit 2
else
    echo "$file : OK"
    exit 0
fi

# should never be here
exit 1
