#!/usr/bin/env bash

# Le script lancera par defaut tous les tests du jeu de test
# ie tous les tests concernant le context et la génération de
# code dans le repertoire src/test/deca/
# chaque sous repertoire de context et de codegen devra con-
# tenir un dossier valid/ et invalid/


if [[ $# -gt 1 ]]; then
    echo "Usage : test_code [nomDossierATester]"
fi

echo "

                ######################
                #                    #
                #   SCRIPT DE TEST   #
                #                    #
                ######################


                "


# se place dans le dossier avec tous les tests deca
cd "$(dirname "$0")/../deca"

globalTest=0
globalTested=0
globalPassed=0
globalFailed=0

cd "context"

echo "##############################
           CONTEXT
##############################
"


nbContextualTest=0
nbContextualTestTested=0
nbContextualTestPassed=0
nbContextualTestFailed=0

# boucle de tous les dossiers dans contex
for dossier in *; do

    echo "
    ##############################
               $dossier
    ##############################"

    cd "$dossier"

    if [ -d "invalid" ]; then
        # compteurs de tests pour les stats invalid
        curNbInvalidTest=0
        curNbInvalidTestTested=0
        curNbInvalidTestFailed=0
        curNbInvalidTestPassed=0

        cd "invalid"
        if [ "$(ls -A .)" ]; then
            echo "********* invalid *********"

            for file in $(find .) ; do
                if [[ -f $file ]]; then
                    if [ ${file: -5} == ".deca" ]; then
                        ((curNbInvalidTest++))
                    fi
                    if [ ${file: -9} == ".expected" ]; then
                        ((curNbInvalidTestTested++))
                        decaFile="${file%.expected}"
                        if test_context "${decaFile}.deca" 2> "${decaFile}.res" ; then
                            echo "--- ${decaFile}.deca: KO ---"
                            ((curNbInvalidTestFailed++))
                        #on cherche dans le res ce quily avait dans le expected
                        elif grep $(cat "${file}") "${decaFile}.res" > /dev/null ; then
                            echo "--- ${decaFile}.deca: PASSED ---"
                            ((curNbInvalidTestPassed++))
                        else
                            echo "--- ${decaFile}.deca: FAILED ---"
                            echo "DID NOT FOUND STRING \"$(cat ${file})\""
                            echo "IN \"$(cat ${decaFile}.res)\""
                            ((curNbInvalidTestFailed++))
                        fi
                    fi
                fi
            done
        fi
        #fin du dossier invalid
        (( nbContextualTest+=$curNbInvalidTest ))
        (( nbContextualTestTested+=$curNbInvalidTestTested ))
        (( nbContextualTestFailed+=$curNbInvalidTestFailed ))
        (( nbContextualTestPassed+=$curNbInvalidTestPassed ))

        cd ".."
        echo
        echo "# STATS $dossier invalid #"
        percent=0;
        if [[ ${curNbInvalidTestTested} -ne 0 ]]; then
            percent=$(echo "scale=2; $curNbInvalidTestPassed/$curNbInvalidTestTested*100" | bc)
        fi
        echo "Test passed : $percent%"
        percent=0;
        if [[ ${curNbInvalidTest} -ne 0 ]]; then
            percent=$(echo "scale=2; $curNbInvalidTestTested/$curNbInvalidTest*100" | bc)
        fi
        echo "Test done : $curNbInvalidTestTested/$curNbInvalidTest ($percent%)"
        echo
    fi

    if [ -d "valid" ]; then

        cd "valid"
        if [ "$(ls -A .)" ]; then
            echo "********* valid *********"

            # compteurs de tests pour les stats valid
            curNbValidTest=0
            curNbValidTestTested=0
            curNbValidTestPassed=0
            curNbValidTestFailed=0

            for file in $(find .) ; do
                if [[ -f ${file} ]]; then
                    # fichier a tester
                    if [ ${file: -5} == ".deca" ]; then
                        ((curNbValidTest++))
                    fi
                    #fichier testable
                    if [ ${file: -9} == ".expected" ]; then
                        ((curNbValidTestTested++))
                        decaFile="${file%.expected}"
                        test_context "${decaFile}.deca" | sed '1,4d'  > "${decaFile}.res"
                        if [ -f "${decaFile}.res" ]; then
                            if diff -q "${decaFile}.res" "${file}" > /dev/null ; then
                                echo "--- ${decaFile}.deca: PASSED ---"
                                ((curNbValidTestPassed++))
                            else
                                echo "--- ${decaFile}.deca: FAILED ---"
                                diff "${file}" "${decaFile}.res"
                                ((curNbValidTestFailed++))
                            fi
                        else
                            echo "--- ${decaFile}.deca: KO ---"
                            ((curNbValidTestFailed++))
                        fi
                    fi
                fi
            done
        fi

        (( nbContextualTest+=$curNbValidTest ))
        (( nbContextualTestTested+=$curNbValidTestTested ))
        (( nbContextualTestFailed+=$curNbValidTestFailed ))
        (( nbContextualTestPassed+=$curNbValidTestPassed ))

        echo
        echo "# STATS $dossier valid #"
        percent=0
        if [[ ${curNbValidTestTested} -ne 0 ]]; then
            percent=$(echo "scale=2; $curNbValidTestPassed/$curNbValidTestTested*100" | bc)
        fi
        echo "Test passed : $percent%"
        percent=0
        if [[ ${curNbInvalidTest} -ne 0 ]]; then
            percent=$(echo "scale=2; $curNbValidTestTested/$curNbValidTest*100" | bc)
        fi
        echo "Test done : $curNbValidTestTested/$curNbValidTest ($percent%)"
        echo
        cd ".."
    fi

    #fin de traitement d un dossier
    cd ".."
    echo "############ FIN $dossier ############"
done

cd ".."

echo
echo "##############################
        STATS CONTEXT
##############################"
percent=0
if [[ ${nbContextualTestTested} -ne 0 ]]; then
    percent=$(echo "scale=2; $nbContextualTestPassed/$nbContextualTestTested*100" | bc)
fi
echo "Test passed : $percent%"
percent=0
if [[ ${nbContextualTest} -ne 0 ]]; then
    percent=$(echo "scale=2; $nbContextualTestTested/$nbContextualTest*100" | bc)
fi
echo "Test done : $nbContextualTestTested/$nbContextualTest ($percent%)"
echo

(( globalTest += $nbContextualTest))
(( globalPassed += $nbContextualTestPassed ))
(( globalTested += $nbContextualTestTested ))

echo "##############################
           CODEGEN
##############################"

nbCodeTest=0
nbCodeTestTested=0
nbCodeTestPassed=0
nbCodeTestFailed=0


cd "codegen"

# boucle de tous les dossiers dans codegen
for dossier in *; do

    echo "
    ##############################
               $dossier
    ##############################"

    cd "$dossier"

    if [ -d "invalid" ]; then

        cd "invalid"
        nbCodeInvalidTest=0
        nbCodeInvalidTestTested=0
        nbCodeInvalidTestPassed=0
        nbCodeInvalidTestFailed=0
        if [ "$(ls -A .)" ]; then

            echo "********* invalid *********"

            rm -f *.res *.ass || exit 1
            for f in * ; do
                #si fichier .deca -> cest un fichier de test
                if [ ${f: -5} == ".deca" ]; then
                    ((nbCodeInvalidTest++))
                fi
                #si fichier .expected -> le .deca assosice est a tester
                if [ ${f: -9} == ".expected" ]; then
                    ((nbCodeInvalidTestTested++))
                    decaFile="${f%.expected}"
                    # retrieve first script line
                    line=$(head -n 1 "${decaFile}.deca")
                    if [[ "$line" == //arg* ]]; then
                        # execute with arguments
                        args=${line:6}
                        decac "${args}" "${decaFile}.deca" > "${decaFile}.res"
                    else
                        #compile le fichier deca et remplit le fichier .res avec l'execution de ima
                        decac "${decaFile}.deca" && (ima "${decaFile}.ass" > "${decaFile}.res")
                    fi
                    #si le fichier a ete créé, pas d'erreur d'execution
                    if [ -f "${decaFile}.res" ]; then
                        #si pas de difference entre le expected et le res OK !!!
	                    if diff -q "${decaFile}.res" "${decaFile}.expected" > /dev/null ; then
	                        echo "--- ${f}: PASSED ---"
	                        ((nbCodeInvalidTestPassed++))
	                    # dommage ...
	                    else
                            echo "--- ${f}: FAILED ---"
                            diff "${decaFile}.expected" "${decaFile}.res"
                            ((nbCodeInvalidTestFailed++))
                        fi
                    else
                        echo "--- ${f}: KO ---"
                        ((nbCodeInvalidTestFailed++))
                    fi
                    echo
                fi
            done
            cd ".."

            (( nbCodeTest+=$nbCodeInvalidTest ))
            (( nbCodeTestTested+=$nbCodeInvalidTestTested ))
            (( nbCodeTestPassed+=$nbCodeInvalidTestPassed ))


            echo
            echo "########### Stat $dossier Invalid ###########"
            percent=0
            if [[ ${nbCodeInvalidTestTested} -ne 0 ]]; then
                percent=$(echo "scale=2; $nbCodeInvalidTestPassed/$nbCodeInvalidTestTested*100" | bc)
            fi
            echo "Test passed : $percent%"
            percent=0
            if [[ ${nbCodeInvalidTest} -ne 0 ]]; then
                percent=$(echo "scale=2; $nbCodeInvalidTestTested/$nbCodeInvalidTest*100" | bc)
            fi
            echo "Test done : $nbCodeInvalidTestTested/$nbCodeInvalidTest ($percent%)"
            echo
        fi
    fi

    if [ -d "valid" ]; then
        cd "valid"
        nbCodeValidTest=0
        nbCodeValidTestTested=0
        nbCodeValidTestPassed=0
        nbCodeValidTestFailed=0
        if [ "$(ls -A .)" ]; then
            echo "********* valid *********"

            rm -f *.res *.ass || exit 1
            for f in * ; do
                #si fichier .deca -> cest un fichier de test
                if [ ${f: -5} == ".deca" ]; then
                    ((nbCodeValidTest++))
                fi
                #si fichier .expected -> le .deca assosice est a tester
                if [ ${f: -9} == ".expected" ]; then
                    ((nbCodeValidTestTested++))
                    decaFile="${f%.expected}"
                    # retrieve first script line
                    line=$(head -n 1 "${decaFile}.deca")
                    if [[ "$line" == //arg* ]]; then
                        # execute with arguments
                        args=${line:6}
                        decac "${args}" "${decaFile}.deca" > "${decaFile}.res"
                    else
                        #compile le fichier deca et remplit le fichier .res avec l'execution de ima
                        decac "${decaFile}.deca" && (ima "${decaFile}.ass" > "${decaFile}.res")
                    fi
                    if [ -f "${decaFile}.res" ]; then
                    if diff -q "${decaFile}.res" "${decaFile}.expected" > /dev/null ; then
                        echo "--- ${f}: PASSED ---"
                        ((nbCodeValidTestPassed++))
                    else
                        echo "--- ${f}: FAILED ---"
                        diff "${decaFile}.expected" "${decaFile}.res"
                        ((nbCodeValidTestFailed++))
                    fi
                    else
                    echo "--- ${f}: KO ---"
                        ((nbCodeValidTestFailed++))
                    fi
                    echo
                fi
            done
        fi
        cd ".."
    fi
    cd ".."
    (( nbCodeTest+=$nbCodeValidTest ))
    (( nbCodeTestTested+=$nbCodeValidTestTested ))
    (( nbCodeTestPassed+=$nbCodeValidTestPassed ))
    echo
    echo "########### Stat $dossier Valid ###########"
    percent=0
    if [[ ${nbCodeValidTestTested} -ne 0 ]]; then
        percent=$(echo "scale=2; $nbCodeValidTestPassed/$nbCodeValidTestTested*100" | bc)
    fi
    echo "Test passed : $percent%"
    percent=0
    if [[ ${nbCodeValidTest} -ne 0 ]]; then
        percent=$(echo "scale=2; $nbCodeValidTestTested/$nbCodeValidTest*100" | bc)
    fi
    echo "Test done : $nbCodeValidTestTested/$nbCodeValidTest ($percent%)"
    echo
    echo "############ FIN $dossier ############"
done
echo
echo "##############################
        STATS CODEGEN
##############################"
percent=0
if [[ ${nbCodeTestTested} -ne 0 ]]; then
    percent=$(echo "scale=2; $nbCodeTestPassed/$nbCodeTestTested*100" | bc)
fi
echo "Test passed : $percent%"
percent=0
if [[ ${nbCodeTest} -ne 0 ]]; then
    percent=$(echo "scale=2; $nbCodeTestTested/$nbCodeTest*100" | bc)
fi
echo "Test done : $nbCodeTestTested/$nbCodeTest ($percent%)"
echo

(( globalTest += $nbCodeTest ))
(( globalPassed += $nbCodeTestPassed ))
(( globalTested += $nbCodeTestTested ))

echo
echo "##############################
        GLOBAL STATS
##############################"
percent=0
if [[ ${globalTested} -ne 0 ]]; then
    percent=$(echo "scale=2; $globalPassed/$globalTested*100" | bc)
fi
echo "Test passed : $percent%"
percent=0
if [[ ${globalTest} -ne 0 ]]; then
    percent=$(echo "scale=2; $globalTested/$globalTest*100" | bc)
fi
echo "Test done : $globalTested/$globalTest ($percent%)"
echo
