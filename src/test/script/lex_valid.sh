#! /bin/sh

#prend un fichier .deca en parametre
#/!\ ce script ne test que la lexicogrpahie !!!!
#la syntax comprends le lexer et le parser !!
file=$1
#cest un fichier valid lexicalement meme si il est invalid
#syntaxiquement
if test_lex $file 2>&1 | head -n 1 | grep -q "$file.deca:[0-9]"; then
    echo "Echec inattendu de test_lex sur $file"
    exit 1
else
    echo "$file : OK"
    exit 0
fi
