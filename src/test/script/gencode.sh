#! /bin/sh
# Return codes :
# 0 : Compilation and execution OK
# 1 : Script error if this happens
# 2 : File compilation failed
# 3 : Unexpected ima output

cleanup() {
    rm -f "$ass_file"
    rm -f "$ima_output_file"
    exit $1
}

file=$1
expected_file=${file%.deca}.expected

ass_file=${file%.deca}.ass
ima_output_file=${file%.deca}.ima_out
ima_input_file=${file%.deca}.input

decac "$file" || exit 2
if [ ! -f "$ass_file" ]; then
    echo "$file : compilation failed"
    exit 2
fi
if [[ -f "$ima_input_file" ]]; then
    ima $ass_file < $ima_input_file > $ima_output_file
else
    ima $ass_file > $ima_output_file
fi

if diff --ignore-all-space "$expected_file" "$ima_output_file"; then
    echo "$file : OK"
    cleanup 0
else
    echo "$file : Mismatch between ima output and expected"
    echo "$resultat"
    cleanup 3
fi

# should never be here
cleanup 1
