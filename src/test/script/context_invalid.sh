#! /bin/sh
# Return codes :
# 0 : Contextual check failed as expected
# 1 : Script error if this happens
# 2 : Unexpected success

# Auteur : Tom Lasne - Thomas Bianchini
# Version initiale : 23/06/2015

# Test minimaliste de la vérification contextuelle.
# Le principe et les limitations sont les mêmes que pour basic-synt.sh

file=$1
if test_context "$file" 2>&1 | grep -q -e "$(cat ${file%.deca}.expected)"
then
    echo "$file : OK"
    exit 0
else
    echo "$file : Unexpected success"
    exit 2
fi

# should never be here
exit 1
