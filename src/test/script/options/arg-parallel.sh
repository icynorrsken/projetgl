#!/bin/bash

cd "$(dirname "$0")"/../../deca/options
NUMBEROFFILES=500

echo "Creation des fichiers sources.."
START=$(date +%s.%N)
SOURCE_FILE="./hello.deca"
FILE="hello"
SUFFIX=".deca"
FILE_LIST=""
TOP_FOLDER=`pwd`
for (( i=1; i<=$NUMBEROFFILES; i++ ))
do
	cp ./hello.deca ${FILE}_${i}${SUFFIX}
	FILE_LIST="${FILE_LIST} ${TOP_FOLDER}/${FILE}_${i}${SUFFIX}"
done

TEMPSNOPAR=0
TEMPSPAR=0

echo "Evaluation du temps d'exécution de manière classique"
START=$SECONDS
decac $FILE_LIST
END=$SECONDS
DIFFCLASSIC=$(echo "$END - $START" | bc)
echo "Temps d'exécution en mode classique: $DIFFCLASSIC"
#TEMPSNOPAR=`echo $TEMPSNOPAR + $DIFF | bc`
echo "Evaluation du temps d'exécution de manière parallèle"
START=$SECONDS
decac -P $FILE_LIST
END=$SECONDS
DIFFPAR=$(echo "$END - $START" | bc)
#TEMPSPAR=`echo $TEMPSPAR + $DIFFPAR | bc`
echo "Temps d'exécution en mode parallèle: $DIFFPAR"

#echo "TEMPS FINAL NORMAL $TEMPSNOPAR"
#echo "TEMPS FINAL PARALLELE $TEMPSPAR"

echo "Suppression des fichiers sources.."
rm ./hello_*

if [ $(bc <<< "$DIFFPAR<$DIFFCLASSIC") -eq 1 ];
then
	echo "Test passed"
	exit 0
else
	echo "Test failled"
	exit 1
fi
