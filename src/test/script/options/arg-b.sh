#!/bin/bash

cd "$(dirname "$0")"/../../deca/options/

decac -b > banner.res

if diff -q "banner.res" "banner.expected" > /dev/null ; then
    echo "Test passed"
    exit 0
else
    echo "Test failled"
    exit 1
fi
