#!/bin/bash

cd "$(dirname "$0")"/../../deca/options
root="$(pwd)"

decac="$(which decac)"
if [ "${decac}" == "" ] ; then
    echo "Error, executable \"decac\" not found in"
    echo "${PATH}"
    exit 1
fi

nbtests=0
nbpassed=0

echo "### TEST: $(pwd) ###"
#un decac -p on all deca files
rm -f *.res *.ass || exit 1
for f in *.argpCompatible.deca ; do
    file="${f%.argpCompatible.deca}"
    ((nbtests++))
    echo "Running decac -p ${f} then write the result in ${file}.res"
    if decac "${f}" "-p" > "${file}.res"; then
    if diff -q "${file}.res" "${f}" > /dev/null ; then
        echo "--- ${f}: PASSED ---"
        ((nbpassed++))
    else
        echo "--- ${f}: FAILED ---"
        diff "${f}" "${file}.res"
    fi
    else
    echo "--- ${f}: KO ---"
    fi
    echo
done


echo "### SCORE: ${nbpassed} PASSED / ${nbtests} TESTS ###"

if [ $nbpassed -eq $nbtests ]; then
    exit 0
else
    exit 1
fi
