#!/bin/bash

#Check the good working of v argument when running decac -v

nbtests=0
nbpassed=0

cd "$(dirname "$0")"/../../deca/options
root="$(pwd)"

decac="$(which decac)"
if [ "${decac}" == "" ] ; then
    echo "Error, executable \"decac\" not found in"
    echo "${PATH}"
    exit 1
fi

echo "### decac -v : no .ass  has to be generated ###"
rm -f *.res *.ass || exit 1
for f in *.deca ; do
    ((nbtests++))
    file="${f%.deca}"

    echo "Running : ${decac} ${f}"
    decac "${f}" "-v"

if [ -f "${file}.ass" ]; then
    echo "A .ass has been generated for ${file} : FAILED"
else
    echo "No .ass has been generated for ${file} : PASSED"
    ((nbpassed++))
fi

    break
done

echo "### decac : a .ass  has to be generated ###"
rm -f *.res *.ass || exit 1
for f in *.deca ; do
    ((nbtests++))
    file="${f%.deca}"

    echo "Running : ${decac} ${f}"
    decac "${f}"

if [ -f "${file}.ass" ]; then
    echo "A .ass has been generated for ${file} : PASSED"
    ((nbpassed++))
else
    echo "No .ass has been generated for ${file} : FAILED"
fi

    break
done



echo "### SCORE: ${nbpassed} PASSED / ${nbtests} TESTS ###"

if [ $nbpassed -eq $nbtests ]; then
    exit 0
else
    exit 1
fi
