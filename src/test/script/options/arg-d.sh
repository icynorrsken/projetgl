#!/bin/bash

cd "$(dirname "$0")"/../../deca/options

TEST_GENERAL=0

###### TEST DEBUG niveau 0 ##########

decac hello2.deca > banner.res

NB_L_MODE_0=`cat banner.res`
NB_L_MODE_1=`cat banner.res | grep -e "Decac compiler started"`
NB_L_MODE_2=`cat banner.res | grep -e "Debug mode activated"`
NB_L_MODE_3=`cat banner.res | grep -e "Trace mode activated"`

RES=0
if [ -n "$NB_L_MODE_0" ]; then
    RES=1;
fi
if [ -n "$NB_L_MODE_1" ]; then
    RES=1;
fi
if [ -n "$NB_L_MODE_2" ]; then
    RES=1;
fi
if [ -n "$NB_L_MODE_3" ]; then
    RES=1;
fi

if [ $RES -eq 0 ]
then
    echo "Test debug niveau ALL validé"
else
    echo "Test debug niveau ALL invalidé"
    TEST_GENERAL=1
fi

rm banner.res

###### TEST DEBUG niveau 1 ##########

decac -d hello2.deca >> banner.res

NB_L_MODE_0=`cat banner.res`
NB_L_MODE_1=`cat banner.res | grep -e "Decac compiler started"`
NB_L_MODE_2=`cat banner.res | grep -e "Debug mode activated"`
NB_L_MODE_3=`cat banner.res | grep -e "Trace mode activated"`

RES=0
if [ -z "$NB_L_MODE_0" ]; then
    RES=1;
fi
if [ -z "$NB_L_MODE_1" ]; then
    RES=1;
fi
if [ ! -z "$NB_L_MODE_2" ]; then
    RES=1;
fi
if [ ! -z "$NB_L_MODE_3" ]; then
    RES=1;
fi

if [ $RES -eq 0 ]
then
    echo "Test debug niveau INFO validé"
else
    echo "Test debug niveau INFO invalidé"
    TEST_GENERAL=1
fi

rm banner.res

###### TEST DEBUG niveau 2 ##########

decac -d -d hello2.deca >> banner.res

NB_L_MODE_0=`cat banner.res`
NB_L_MODE_1=`cat banner.res | grep -e "Decac compiler started"`
NB_L_MODE_2=`cat banner.res | grep -e "Debug mode activated"`
NB_L_MODE_3=`cat banner.res | grep -e "Trace mode activated"`

RES=0
if [ -z "$NB_L_MODE_0" ]; then
    RES=1;
fi
if [ -z "$NB_L_MODE_1" ]; then
    RES=1;
fi
if [ -z "$NB_L_MODE_2" ]; then
    RES=1;
fi
if [ ! -z "$NB_L_MODE_3" ]; then
    RES=1;
fi

if [ $RES -eq 0 ]
then
    echo "Test debug niveau DEBUG validé"
else
    echo "Test debug niveau DEBUG invalidé"
    TEST_GENERAL=1
fi

rm banner.res

###### TEST DEBUG niveau 3 ##########

decac -d -d -d hello2.deca >> banner.res

NB_L_MODE_0=`cat banner.res`
NB_L_MODE_1=`cat banner.res | grep -e "Decac compiler started"`
NB_L_MODE_2=`cat banner.res | grep -e "Debug mode activated"`
NB_L_MODE_3=`cat banner.res | grep -e "Trace mode activated"`

RES=0
if [ -z "$NB_L_MODE_0" ]; then
    RES=1;
fi
if [ -z "$NB_L_MODE_1" ]; then
    RES=1;
fi
if [ -z "$NB_L_MODE_2" ]; then
    RES=1;
fi
if [ -z "$NB_L_MODE_3" ]; then
    RES=1;
fi

if [ $RES -eq 0 ]
then
    echo "Test debug niveau TRACE validé"
else
    echo "Test debug niveau TRACE invalidé"
    TEST_GENERAL=1
fi

rm banner.res

###### TEST DEBUG niveau 4 ##########
###### EQUIVALENT niveau 3 ##########

decac -d -d -d -d hello2.deca >> banner.res

NB_L_MODE_0=`cat banner.res`
NB_L_MODE_1=`cat banner.res | grep -e "Decac compiler started"`
NB_L_MODE_2=`cat banner.res | grep -e "Debug mode activated"`
NB_L_MODE_3=`cat banner.res | grep -e "Trace mode activated"`

RES=0
if [ -z "$NB_L_MODE_0" ]; then
    RES=1;
fi
if [ -z "$NB_L_MODE_1" ]; then
    RES=1;
fi
if [ -z "$NB_L_MODE_2" ]; then
    RES=1;
fi
if [ -z "$NB_L_MODE_3" ]; then
    RES=1;
fi

if [ $RES -eq 0 ]
then
    echo "Test debug niveau TRACE validé"
else
    echo "Test debug niveau TRACE invalidé"
    TEST_GENERAL=1
fi

rm banner.res



if [ $TEST_GENERAL -eq 0 ]
then
    echo "Test général validé"
    exit 0
else
    echo "Test général invalidé"
    exit 1
fi
