#!/bin/bash

cd "$(dirname "$0")"/../../deca/options
root="$(pwd)"

decac="$(which decac)"
if [ "${decac}" == "" ] ; then
    echo "Error, executable \"decac\" not found in"
    echo "${PATH}"
    exit 1
fi

nbtests=0
nbpassed=0

echo "### TEST: $(pwd) ###"

#run decac on all files which have an associated .expectedWithNoArgn
echo " === Tests running decac FILE === "
rm -f *.res *.ass || exit 1

    file="compute"
    ((nbtests++))
    decac "${file}.deca"

    if [ -f "${file}.ass" ]; then

    r=`cat ${file}.ass | grep "TSTO"`
    s=`cat ${file}.ass | grep "BOV"`
        if [ ! -z "$r" -a ! -z "$s" ]; then
            echo "--- ${file}: PASSED ---"
            ((nbpassed++))
        else
            echo "--- ${file}: FAILED ---"
        fi
        else
        echo "--- ${file}: KO ---"
        fi
        echo

#run decac -n on all files which have an associated .expectedWithArgn
echo " === Tests running decac -n FILE === "
rm -f *.res *.ass || exit 1

    ((nbtests++))
    decac "${file}.deca" "-n"

	r=`cat ${file}.ass | grep "TSTO"`
    	s=`cat ${file}.ass | grep "BOV"`

        if [ -f "${file}.ass" ]; then
        if [ -z "$r" -a -z "$s" ]; then
            echo "--- ${file}: PASSED ---"
            ((nbpassed++))
    else
        echo "--- ${file}: FAILED ---"
    fi
    else
    echo "--- ${file}: KO ---"
    fi
    echo

echo "### SCORE: ${nbpassed} PASSED / ${nbtests} TESTS ###"

if [ $nbpassed -eq $nbtests ]; then
    exit 0
else
    exit 1
fi
