#!/bin/bash

nbtests=2
nbpassed=0

cd "$(dirname "$0")"/../../deca/options
root="$(pwd)"

decac="$(which decac)"
if [ "${decac}" == "" ] ; then
    echo "Error, executable \"decac\" not found in"
    echo "${PATH}"
    exit 1
fi

i=4
let j=$i+1

rm -f *.res *.ass || exit 1

file="compute"

#tests for to less and to many registers
f="argrException"

echo "Running : ${decac} ${file}.deca -r 3"
if decac "${file}.deca" "-r" "3" > "${file}.res" 2> "${file}.res" ; then
    echo "--- ${file}.deca: KO ---"
elif diff -q "${file}.res" "${f}.expected" > /dev/null ; then
    echo "--- ${file}.deca: Exception well raised ---"
    ((nbpassed++))
fi

echo "Running : ${decac} ${file}.deca -r 17"
if decac "${file}.deca" "-r" "17" > "${file}.res" 2> "${file}.res" ; then
    echo "--- ${file}.deca: KO ---"
elif diff -q "${file}.res" "${f}.expected" > /dev/null ; then
    echo "--- ${file}.deca: Exception well raised ---"
    ((nbpassed++))
fi



#tests for a good number of registers
while [ 16 -ge $i ]
do

echo "### decac -r ${i} ###"

    ((nbtests++))

    echo "Running : ${decac} ${file}.deca -r ${i}"
    decac "${file}.deca" "-r" "${i}"

r=`cat ${file}.ass | grep R${j}`

echo "${r}"

if [ -n "$r" ]; then
    echo "The register number ${j} is used in ${file} : FAILED"
else
    ((nbpassed++))
fi

((i++))
let j=$i+1
done


echo "### SCORE: ${nbpassed} PASSED / ${nbtests} TESTS ###"

if [ $nbpassed -eq $nbtests ]; then
    exit 0
else
    exit 1
fi
