#! /bin/sh
# Return codes :
# 0 : Syntax check failed as expected
# 1 : Script error if this happens
# 2 : Unexpected success

file=$1
if test_synt "$file" 2>&1 | grep -q -f "${file%.deca}.expected"
then
    echo "$file : OK"
    exit 0
else
    echo "$file : Unexpected success"
    exit 2
fi

# should never be here
exit 1

