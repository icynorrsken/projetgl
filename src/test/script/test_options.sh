#! /bin/bash

cd "$(dirname "$0")"/options

#nbTest=$(ls -A | wc -l)
#nbTest=${nbTest:7:1}
nbTest=7
nbTestPassed=0
nbTestFailled=0

echo "Execute arg -b script:"
source arg-b.sh
arg_b
if [[ $? -eq 1 ]]; then
    ((nbTestPassed++))
else
	((nbTestFailled++))
fi
echo

echo "ici"`pwd`
cd "$(dirname "$0")"/../../script/options
echo "Execute arg -d script:"
source arg-d.sh
arg_d
if [[ $? -eq 1 ]]; then
    ((nbTestPassed++))
else
	((nbTestFailled++))
fi
echo

cd "$(dirname "$0")"/../../script/options
echo "Execute arg -n script:"
source arg-n.sh
arg_n
if [[ $? -eq 1 ]]; then
    ((nbTestPassed++))
else
	((nbTestFailled++))
fi
echo

cd "$(dirname "$0")"/../../script/options
echo "Execute arg -p script:"
source arg-p.sh
arg_p
if [[ $? -eq 1 ]]; then
    ((nbTestPassed++))
else
	((nbTestFailled++))
fi
echo

cd "$(dirname "$0")"/../../script/options
echo "Execute arg -P script:"
source arg-parallel.sh
arg_P
if [[ $? -eq 1 ]]; then
    ((nbTestPassed++))
else
	((nbTestFailled++))
fi
echo

cd "$(dirname "$0")"/../../script/options
echo "Execute arg -r script:"
source arg-r.sh
arg_r
if [[ $? -eq 1 ]]; then
    ((nbTestPassed++))
else
	((nbTestFailled++))
fi
echo

cd "$(dirname "$0")"/../../script/options
echo "Execute arg -v script:"
source arg-v.sh
arg_v
if [[ $? -eq 1 ]]; then
    ((nbTestPassed++))
else
	((nbTestFailled++))
fi
echo

echo "##############################
        GLOBAL STATS
##############################"
echo
percentTestPassed=$(echo "scale=2; $nbTestPassed/$nbTest*100" | bc)
echo "Test passed: $percentTestPassed%"
nbTestDone=$((nbTestPassed + $nbTestFailled))
echo "Test done: $nbTestDone/$nbTest"
