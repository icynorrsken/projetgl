package fr.ensimag.deca.tools;
import fr.ensimag.ima.pseudocode.Label;

import java.util.ArrayList;

/**
 * Created by Thomas on 22/06/15.
 */
public class ManualTestLabelManager {
    public static void main(String[] args) {
        LabelManager lm = new LabelManager();

        lm.setAdhocPrefix(null);

        ArrayList<Label> list = new ArrayList<>();
        list.add(lm.adhoc("lol"));       // LOL
        list.add(lm.adhoc("LOL"));       // LOL.2
        list.add(lm.adhoc("loL"));       // LOL.3
        list.add(lm.adhoc("lol.2"));     // LOL.2.2
        list.add(lm.adhoc("lol.3"));     // LOL.3.2
        list.add(lm.adhoc("lol.2.2.2")); // LOL.2.2.2 ok
        list.add(lm.adhoc(
                "lol.2.2"));   // LOL.2.2 taken, LOL.2.2.2 taken, LOL.2.2.3 ok
        list.add(lm.adhoc("lol.2.2.4")); // LOL.2.2.4 ok
        list.add(lm.adhoc("lol.2.2"));   // ...LOL.2.2.5

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(i).toString().equals(list.get(j).toString())) {
                    throw new Error("name clash");
                }
            }
        }

        if (!lm.createUnique("user.lapin").toString().startsWith("OOPS.")) {
            throw new Error("should've started with OOPS.");
        }

        if (!lm.createUnique("oops.lapin").toString().startsWith("OOPS.")) {
            throw new Error("should've started with OOPS.");
        }

        if (!lm.createUnique("_lol").toString().startsWith("OOPS.")) {
            throw new Error("should've started with OOPS.");
        }

        if (!lm.createUnique("1337.lapin").toString().startsWith("OOPS.")) {
            throw new Error("should've started with OOPS.");
        }

        if (!lm.createUnique("R0").toString().startsWith("OOPS.")) {
            throw new Error("should have started with OOPS.");
        }

        if (!lm.createUnique("ADDSP").toString().startsWith("OOPS.")) {
            throw new Error("should have started with OOPS.");
        }

        if (lm.createUnique("ADDSPLOL").toString().startsWith("OOPS.")) {
            throw new Error("should NOT have started with OOPS.");
        }

        if (lm.createUnique("saumon").toString().startsWith("OOPS.")) {
            throw new Error("should NOT have started with OOPS.");
        }
    }
}
