lexer grammar DecaLexer;

options {
   language=Java;
   // Tell ANTLR to make the generated lexer class extend the
   // the named class, which is where any supporting code and
   // variables will be placed.
   superClass = AbstractDecaLexer;
}

@members {
}

// Deca lexer rules.

EOL: '\n' -> skip;

PRINTLNX: 'printlnx';
PRINTLN: 'println';
PRINTX: 'printx';
PRINT: 'print';
WHILE: 'while';	
RETURN: 'return';
IF: 'if';
ELSE: 'else';
INSTANCEOF: 'instanceof';
READINT: 'readInt';
READFLOAT: 'readFloat';
NEW: 'new';
TRUE: 'true';
FALSE: 'false';
THIS: 'this';
NULL: 'null';
CLASS: 'class';
EXTENDS: 'extends';
PROTECTED: 'protected';
ASM: 'asm';

OR: '||';
AND: '&&';
EQEQ: '==';
NEQ: '!=';
LEQ: '<=';
GEQ: '>=';
GT: '>';
LT: '<';
PLUS: '+';
MINUS: '-';
TIMES: '*';
SLASH: '/';
PERCENT: '%';
EXCLAM: '!';
DOT: '.';
OPARENT: '(';
CPARENT: ')';
OBRACE: '{';
CBRACE: '}';
SEMI: ';';
COMMA: ',';
EQUALS: '=';

fragment DIGIT: '0'..'9';
fragment POSITIVE_DIGIT: '1'..'9';
fragment LETTER: ('a'..'z'|'A'..'Z');


fragment NUM: DIGIT+;
fragment SIGN: '+' | '-' | ;
fragment EXP: ('E'|'e') SIGN NUM;
fragment DEC: NUM '.' NUM;
fragment FLOATDEC: (DEC | (DEC EXP)) ('F' | 'f' | );
fragment DIGITHEX: ('0'..'9' | 'A'..'F' | 'a'..'f');
fragment NUMHEX: DIGITHEX+;
fragment FLOATHEX: ('0x'|'0X') NUMHEX '.' NUMHEX ('P'|'p') SIGN NUM ('F'|'f'|);

fragment FILENAME: (LETTER | DIGIT | '.' | '-' | '_')+;
INCLUDE: '#include' (' ')* '"' FILENAME '"'{
    doInclude(getText());
};


FLOAT: (FLOATDEC | FLOATHEX);
INT: '0' | (POSITIVE_DIGIT DIGIT*);
IDENT: (LETTER | '$' | '_') (LETTER | DIGIT | '$' | '_')*;

fragment STRING_CAR: ~('"' | '\\' | '\n');
STRING: '"' (STRING_CAR | '\\"' | '\\\\')* '"';
MULTI_LINE_STRING: '"' (STRING_CAR | '\n' | '\\"' | '\\\\')* '"';

COMMENT: ('//' .*? '\n' | '/*' .*? '*/' ) -> skip;

WHITESPACE: (' ' | '\t' | '\r' | '\n') -> skip;
