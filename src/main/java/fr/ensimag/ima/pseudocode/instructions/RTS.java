package fr.ensimag.ima.pseudocode.instructions;

import fr.ensimag.ima.pseudocode.NullaryInstruction;

/**
 * @author Ensimag
 */
public class RTS extends NullaryInstruction {
    @Override
    public int getStackUsage() {
        return -2;
    }
}
