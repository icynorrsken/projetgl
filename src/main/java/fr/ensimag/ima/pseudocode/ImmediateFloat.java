package fr.ensimag.ima.pseudocode;

/**
 * Immediate operand containing a float value.
 *
 * @author Ensimag
 */
public class ImmediateFloat extends DVal {
    private double value;

    public ImmediateFloat(double value) {
        super();
        this.value = value;
    }

    @Override
    public String toString() {
        return "#" + Double.toHexString(value);
    }
}
