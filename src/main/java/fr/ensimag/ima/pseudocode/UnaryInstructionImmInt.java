package fr.ensimag.ima.pseudocode;

/**
 * @author Ensimag
 */
public abstract class UnaryInstructionImmInt extends UnaryInstruction {

    protected final int value;

    protected UnaryInstructionImmInt(ImmediateInteger operand) {
        super(operand);
        this.value = operand.getValue();
    }

    protected UnaryInstructionImmInt(int i) {
        super(new ImmediateInteger(i));
        this.value = i;
    }
}
