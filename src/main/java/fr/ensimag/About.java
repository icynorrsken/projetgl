package fr.ensimag;
/**
 * General informations about the application
 *
 * @author batalt
 */
public class About {

    public static final String TEAM_BANNER =
            getTeamLogo() + "\nTeam 02 - Les bûcherons d'AST\n" +
            "\tBARBIER Jérôme\n" + "\tBATAL Thibaut\n" +
            "\tBIANCHINI Thomas\n" + "\tDE LA FAYOLLE Jean\n" +
            "\tJORIO Iliyas\n" + "\tLASNE Tom\n" + "\tSAUREL Rémi\n" +
            "\tROUSSEAU Jean-Benjamin\n";

    public static final String DECAC_SYNTAXE =
            "Syntaxe : decac [[-p | -v] [-n] [-r X] <fichier deca>...] | " +
			"[-b]\n" +
			"\t-b (banner) : affiche une bannière indiquant le nom de " +
			"l’équipe\n" +
			"\t-p (parse) : arrête decac après l’étape de construction de " +
			"l’arbre, et affiche la décompilation de ce dernier\n" +
			"\t-v (verification) : arrête decac après l’étape de " +
			"vérifications (ne produit aucune sortie en l'absence d’erreur)" +
			"\n" +
			"\t-n (no check) : n'ajoute pas les tests de débordement au " +
			"pogramme compilé (débordement arithmétique, débordement mémoire" +
			" " +
			"et déréférencement de null)\n" +
			"\t-r X (registers) : limite les registres banalisés disponibles" +
			" " +
			"à R0 ... R{X-1}, avec 4 <= X <= 16\n" +
			"\t-d (debug) : active les traces de debug. Répéter l’option " +
			"jusqu'à 3 fois pour avoir plus de traces\n" +
			"\t-P (parallel) : s’il y a plusieurs fichiers sources, lance la" +
			" " +
			"compilation des fichiers en parallèle pour l'accélérer\n" +
			"\t-w (warnings) : affiche les warnings à la compilation\n";

	public static String getTeamLogo() {
		return "                                                       `..``" +
			   " " +
			   " ``..`                                                      " +
			   " " +
			   "\n" +
			   "                                                `,          " +
			   " " +
			   "           .`                                               " +
			   " " +
			   "\n" +
			   "                                            `,              " +
			   " " +
			   "               ``                                           " +
			   " " +
			   "\n" +
			   "                                         ,`                 " +
			   " " +
			   "                   .                                        " +
			   " " +
			   "\n" +
               "                                      `,                     " +
			   "                      `                                      " +
			   "\n" +
               "                                    :                        " +
			   "                        .                                    " +
			   "\n" +
               "                                  ;                          " +
			   "                          .                                  " +
			   "\n" +
               "                                ;                            " +
			   "                            .                                " +
			   "\n" +
               "                              :                              " +
			   "                              .                              " +
			   "\n" +
               "                            `:                               " +
			   "                                `                            " +
			   "\n" +
               "                           ;                                 " +
			   "                                 ,                           " +
			   "\n" +
               "                         .:                                  " +
			   "                                   `                         " +
			   "\n" +
               "                        :                                    " +
			   "                                    ,                        " +
			   "\n" +
               "                       ;                                     " +
			   "                                     ,                       " +
			   "\n" +
               "                     `;                                      " +
			   "                                      .`                     " +
			   "\n" +
               "                    .:                                       " +
			   "                                        .                    " +
			   "\n" +
               "                   ,:                                        " +
			   "                                         .                   " +
			   "\n" +
               "                  ::                                         " +
			   "                                          .                  " +
			   "\n" +
               "                 ::                                          " +
			   "                                           ,                 " +
			   "\n" +
               "                ::                                           " +
			   "                                            .                " +
			   "\n" +
               "               ,:                                            " +
			   "                                            `.               " +
			   "\n" +
               "              .;                                             " +
			   "                                             .`              " +
			   "\n" +
               "              :           :''':.                     ;;;;;;;;" +
			   ";;.                                           ,              " +
			   "\n" +
               "             ;            ;;'';;;;;;':.`             ;;;;:;;;" +
			   ";;.                                            ,             " +
			   "\n" +
               "            :,            ';;;;;;;;;;;;;;;;:.`       ;" +
			   "::::::::;.                                             ,      " +
			   "      \n" +
               "           ,;            `;;;`.:';;;;::;:;;:::::;::," +
			   ".:::::::::;.                                             ..   " +
			   "        \n" +
               "           :             .;;;      `;::;::;:::,,::::::::::,," +
			   "::;.                                              ,           " +
			   "\n" +
               "          ',             ;;;.      `;::::::::,,,,::::::,:,," +
			   ":::::::.                                            ,         " +
			   " \n" +
               "         ;'              ;;;       ,;:::::::,,:,,,:::::,,,,," +
			   ":,:::::;;,;                                        ..         " +
			   "\n" +
               "         ;               ;;;       :;:::::::,,,,,,::::,,:,,,," +
			   ",,,::::;,;                                         ,         " +
			   "\n" +
               "        ''               ';;       ;;:::,,:,,,,,,,,:,:,,,,.,," +
			   ",,,,::::,:                                          ,        " +
			   "\n" +
               "       `+                ;;;       ;::::,,,,,,,..,,,,,,,,,,,," +
			   ",,,,,,::,:                                          ,`       " +
			   "\n" +
               "       ';                ;;;       ;::,:,,,,,,.,..,,,,,,,,,.," +
			   ",,:,,,::.;                                           ,       " +
			   "\n" +
               "      .'                 ;;;       ;;::,,,,,,,.......,,,,,,.," +
			   ",,:,,:,:,:                                           ,`      " +
			   "\n" +
               "      ''                 ;;;       ;:::,,,,,,,,......,,,,,,," +
			   "..,,,,,:,,,                                            ,      " +
			   "\n" +
               "     .'                  ;;;       ;:::,,,,,,,,.......,,,,,,," +
			   ",.,,,,::,.                                            ,`     " +
			   "\n" +
               "     '+                  ;;;       ;:::,,,,,,,,,......,,,,,,," +
			   ",.,,,,,:;,                                             ,     " +
			   "\n" +
               "     '                   ;;;       ;;::,,,,,,,,,,.....,,,,,,," +
			   ",,,,,,:::,                                             ,     " +
			   "\n" +
               "    ;:                   ;;;       ;::::,,,,,,,,,....,,,,,,,," +
			   ",,,,,,,::,                                             ,,    " +
			   "\n" +
               "    :,                   ;;;       ,:::,,,,,,,,,,......,,,,,," +
			   ",,,,,,::,:                                              ,    " +
			   "\n" +
               "   .'                    ;''`      `::::,,,,,,,,,,,...,,,,,,," +
			   ",,,,,,,:,,                                              ,`   " +
			   "\n" +
               "   ;:                    ,'':       ::::,,,,,,,,,,,...,.,,,,," +
			   ",,,,,,::;:                                              .:   " +
			   "\n" +
               "   ',                    `++'       :::,:,,,,,,,,,,,..,.,,,,," +
			   ",,:,,:,:;,                                               :   " +
			   "\n" +
               "  `:                      +''       ;::::,,,,,,,,,,,.....,,,," +
			   ",,,,`` ```                                               :   " +
			   "\n" +
               "  ;:                      ;'+       ,:::::,,,:,,,:,...,..,,,," +
			   ",,:,                                                     :,  " +
			   "\n" +
               "  ';                      ;';'      `:::::,::::,::,,,,,..,,,," +
			   ":,::                                                     `:  " +
			   "\n" +
               "  ':                      .+';       :::::::::::``:,,,,..,,,," +
			   ",:::                                                      :  " +
			   "\n" +
               " `'                        '+'.      ':::::::,   `::,,,...,,," +
			   "::::                                                      :  " +
			   "\n" +
               " .+                        ,;';   `';;;::::.     `::,,,....,," +
			   "::::                                                      :, " +
			   "\n" +
               " ::                         ;++.`:;+'';;:`       `::,,,...,,," +
			   ",:::                                                      ,, " +
			   "\n" +
               " +,                         ,++;;;::'+,          .:::,,...,,," +
			   ",::;                                                      `: " +
			   "\n" +
               " ;'                          ''';';+`            `:::,,,.,,,," +
			   ",::;                                                       : " +
			   "\n" +
               " ;.                          `+'+;               `::;,,,.,,,," +
			   ",::;                                                       : " +
			   "\n" +
               " :                            :,                 `;:;:,,,,,,," +
			   ",::,                                                       : " +
			   "\n" +
               ".;                                                ;:;:,,,,,,," +
			   ",::                                                        " +
			   ":`\n" +
               ",+                                                :;;;,,,,,,," +
			   ":::                                                        :" +
			   ".\n" +
               ",+                                                 '':,:,,:,," +
			   ":::                                                        :" +
			   ".\n" +
               ",+                                                 :'':,,,,,," +
			   "::.                                                        :," +
			   "\n" +
               ":'                                                 .;';:::," +
			   "::::.                                                        " +
			   ":,\n" +
               ",;                                                           " +
			   "                                                           :," +
			   "\n" +
               ":,                                                           " +
			   "                                                           :," +
			   "\n" +
               ",'                                       ;;'::+;:;:;;;:',," +
			   "::::,::::::::;;                                               " +
			   ":,\n" +
               ";,                                       ';;,#;+,,':.;;;;;" +
			   ":::,,:,:::::::::                                              " +
			   ":,\n" +
               ",:                                       ;+:'':;;';,,.;';,;;" +
			   ":,,,,;,:,:::::`                                             :" +
			   ".\n" +
               "`+                                       +;';::;'''':.;.'':," +
			   ".;,:,:,:::,,:::                                             ," +
			   "`\n" +
               "`:                                       '+::::,+'';';::,.':;" +
			   ";:,,;;:.,,:::::                                            ; " +
			   "\n" +
               " +,                                      ''';'':,:+;.:+:.. ;;" +
			   ";;:.:;;:.,,,:::`                                           : " +
			   "\n" +
               " :,                                                 ',;;:`:,;" +
			   ";;;,,`.,:,,,,,,,:,::,::::;;::,;::::;::;:;,                 : " +
			   "\n" +
               " ,;                                                 :::;;'," +
			   ":..,:.;:;;,::,,,,,:,:,;:,,:,:.:,::,,,::,::;,                `:" +
			   " \n" +
               " ;;                                      ```````````'::.;''," +
			   ".';,;;;:;;,:,,,,:,,,:,,,:,,,,,:,:,,,,::::;,                :: " +
			   "\n" +
               " ;'                                      ;';;:'''',;,,::+;;,;" +
			   "::`.':`',:,,;,,,,,,,,,,,,,,,,,,:,:,:,::::,                :: " +
			   "\n" +
               " .'                                      ';:;,.,'':;;:::.:,;," +
			   ".;,,,:;,:;;:.;:,,:,,,,,,,,,,,,,,:,,,,:,::,                ;. " +
			   "\n" +
               "  ',                                     +'':;::,+:;;';;;,;.," +
			   ",,':;;,;,,:;,,,;,,,,,,,,,::,,,,,,,,,,,::;,                :  " +
			   "\n" +
               "  ;;                                     ;:+''';,::'''':;,'," +
			   "``,:;',:;,.`.;,,.,,;,.,,.,,;,,,,,::,,::::;,               `;  " +
			   "\n" +
               "  '+                                     ++:';;,':,,'';':;" +
			   "'...`',:;':::,,.:;':.:,,.,,,,::,,,,,,,,:::;;,               ;;" +
			   "  \n" +
               "  ,;                                     ::,,,:,..,`:;'''," +
			   ".`..;,:,;;:`,.;;::;;;:,,,:,,,:,,,,,,,:,::;:;,               ;." +
			   "  \n" +
               "   ';                                               .::';;:';" +
			   "+',`:.;:;,,';:.::',.;:.;,,::,,,:,,:,;:;:;,               ;   " +
			   "\n" +
               "   +'                                               `,::`;.::" +
			   " .:..`,;';';,,::;;;:;,;,::;:,,,,,,,,:::;:,              :;   " +
			   "\n" +
               "   :;                                    ..........`;..,;`:.;" +
			   ";,#,','::';;;,`.;;;,;;.::,::;,:,::::,,:;:,              ;,   " +
			   "\n" +
               "    ':                                   +';;''';',;;,:`.;;.`" +
			   ".;'::;+..'',;;`.:;';;;:;:;:..;,;,,:;,,::;,              ;    " +
			   "\n" +
               "    ;;                                   +;;'++;'+';+,.';`" +
			   ".::;``:`';:;.;;;,:,;:,`'::';':,;,;;.:::,+:;:,             :;  " +
			   "  \n" +
               "    `;                                   +;;;::+::';:.;;:," +
			   "#`::;',..';;.,::;,.:;;;,::,,,,,:':,,,,:;';::,             :`  " +
			   "  \n" +
               "     ;'                                  +;';,':':;;+''::.'.," +
			   "',:`;.:''.;:,:;.+';:,,;,;:;::,;,:,,';,+::,             :     " +
			   "\n" +
               "     ,;                                  ';;',',::;::@;+';:,;" +
			   "',;''.:.';::,,,:,.;.'';:''',;;,;,::,;''';,            ;,     " +
			   "\n" +
               "      :;                                 ';;+;';,;,:;;;.,;:" +
			   ".';:.:'': .';::'',`,;'';,;:'''';:;:,;.';::',            ;     " +
			   " \n" +
               "      :+                                            ;:;;.:;," +
			   ":;;`, :'.:,+';;::,..;,';'':;'':':'+::;,;;':           ':      " +
			   "\n" +
               "       +;                                           .::;,;;,;" +
			   ";::'',+',',:`;+;;':,,+;:;;''''''''''';;;',           ,       " +
			   "\n" +
               "       ,'                                ,,.,..,..`.,:;:'';,;" +
			   ";;,,#;,,,;:.',,';;';:;:''''''++''''';+;'':          ;,       " +
			   "\n" +
               "        ''                               ++;';;+:;:;:;;;.'':;" +
			   ",:,;:``+:;:';,..                                    '        " +
			   "\n" +
               "        ,;                               '';+''+;;;':':;;';:;" +
			   ":,,':,.':'',;:,                                    ;.        " +
			   "\n" +
               "         ;'                              +'+';'+'':',;;;,':," +
			   "':;:;';:`:+::::                                    ,+         " +
			   "\n" +
               "          ;,                             #+'++'+'';';:;':+::;" +
			   ";;,:'::,,';:'                                     ;          " +
			   "\n" +
               "          ,+                             ''';+++'+'''::;,:,;" +
			   "':::;':;;'+;;.                                    ;,          " +
			   "\n" +
               "           ++                            :::::;;::::..::,,," +
			   ":::.,:::::;:;,                                    ,'          " +
			   " \n" +
               "            :.                                               " +
			   "                                                ;            " +
			   "\n" +
               "            `'                                               " +
			   "                                               ,`            " +
			   "\n" +
               "             .'                                  ';;';" +
			   ":'+:':+''.                                           ',       " +
			   "      \n" +
               "              :'                                 '':'',+'';;" +
			   "'+'.                                          :'              " +
			   "\n" +
               "               ;'                               ,#+;:+;;';;;" +
			   "'''.                                         `;               " +
			   "\n" +
               "                ''                              ';+:'';'+':+;" +
			   "';,                                         ;                " +
			   "\n" +
               "                 #'                            ;+'+:'+;''+'+;" +
			   "+',                                        '                 " +
			   "\n" +
               "                  ;;                             `,;+;'''#++;" +
			   "'#,                                       +                  " +
			   "\n" +
               "                   +'                                     .,;" +
			   "',                                       '                   " +
			   "\n" +
               "                    +'                                       " +
			   "                                        ;                    " +
			   "\n" +
               "                     '+                                      " +
			   "                                      ,:                     " +
			   "\n" +
               "                      :'                                     " +
			   "                                     +,                      " +
			   "\n" +
               "                        ;                                    " +
			   "                                    '                        " +
			   "\n" +
               "                         ';                                  " +
			   "                                   ;                         " +
			   "\n" +
               "                          .#                                 " +
			   "                                 +.                          " +
			   "\n" +
               "                            ':                               " +
			   "                                +                            " +
			   "\n" +
               "                             `:                              " +
			   "                              ;`                             \n" +
               "                               ,:                                                        ',                               \n" +
               "                                 ,'                                                    ',                                 \n" +
               "                                   :#                                                ':                                   \n" +
               "                                     `+,                                           +`                                     \n" +
               "                                        ,+`                                     ',                                        \n" +
               "                                           ,::                              `;.                                           \n" +
               "                                               :+;                      ,':                                               \n" +
               "                                                    .:'+':,`   .,,';:`                                                    ";
    }
}
