package fr.ensimag.deca;

import java.io.File;

/**
 * Allows to run compilation for one file in a new thread
 *
 * @author roussea2
 */
public class DecacCompilerCallable implements Runnable {
    private CompilerOptions compilerOptions;
    private File            source;
    private boolean         isFinished;
    private boolean         isFinishedWithSuccess;

    public DecacCompilerCallable(CompilerOptions compilerOptions, File source) {
        this.compilerOptions = compilerOptions;
        this.source = source;
        this.isFinished = false;
        this.isFinishedWithSuccess = true;
    }

    @Override
    public void run() {

        DecacCompiler compiler = new DecacCompiler(compilerOptions, source);
        if (compiler.compile()) {
            this.isFinishedWithSuccess = false;
        }
        this.isFinished = true;
    }

    public boolean hasFinished() {
        return this.isFinished;
    }

    public boolean hasFinishedWithSuccess() {
        return this.isFinishedWithSuccess;
    }
}
