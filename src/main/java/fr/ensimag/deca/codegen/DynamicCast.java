package fr.ensimag.deca.codegen;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassType;
import fr.ensimag.deca.tools.LabelManager;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.deca.tree.Instanceof;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;

public class DynamicCast extends AbstractCastGen {
    private ClassType dest;

    public DynamicCast(ClassType dest) {
        this.dest = dest;
    }

    @Override
    public void codeGen(DecacCompiler compiler, NonScratchManager mgr) {
        compiler.addInstruction(new PUSH(mgr.getReg()));

        LabelManager lm = compiler.getLabelManager();
        Label success = lm.adhoc("dyncast.success");

        Instanceof.codeGenAncestryLookup(compiler, dest, mgr.getReg(), true,
                                         success);

        compiler.addInstruction(new BRA(lm.dynCastError));
        compiler.addLabel(success);
        compiler.addInstruction(new POP(mgr.getReg()));
    }
}
