package fr.ensimag.deca.codegen;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.NonScratchManager;

public abstract class AbstractCastGen {
    /**
     * Generate cast. Upon calling this function, it is guaranteed that the
     * expression has been evaluated in register mgr.getReg().
     */
    public void codeGen(DecacCompiler compiler, NonScratchManager mgr) {
        throw new UnsupportedOperationException(
                "Compiler bug: unimplemented cast");
    }
}
