/**
 * Utilities used for IMA code generation.
 * <p/>
 * The code generation itself is implement as methods of the {@link
 * fr.ensimag.deca.tree.Tree} class.
 */
package fr.ensimag.deca.codegen;
