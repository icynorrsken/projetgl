package fr.ensimag.deca.codegen;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.instructions.INT;

public class Float2IntCast extends AbstractCastGen {
    @Override
    public void codeGen(DecacCompiler compiler, NonScratchManager mgr) {
        compiler.addInstruction(new INT(mgr.getReg(), mgr.getReg()));
    }
}
