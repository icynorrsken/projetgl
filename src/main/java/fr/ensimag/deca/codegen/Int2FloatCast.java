package fr.ensimag.deca.codegen;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.instructions.FLOAT;

public class Int2FloatCast extends AbstractCastGen {
    @Override
    public void codeGen(DecacCompiler compiler, NonScratchManager mgr) {
        compiler.addInstruction(new FLOAT(mgr.getReg(), mgr.getReg()));
    }
}
