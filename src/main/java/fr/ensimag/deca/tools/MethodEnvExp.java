package fr.ensimag.deca.tools;

import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.MethodDefinition;
import fr.ensimag.deca.context.NonTypeDefinition;
import fr.ensimag.ima.pseudocode.RegisterOffset;

import java.util.concurrent.atomic.AtomicInteger;

public class MethodEnvExp extends EnvironmentExp {
    private final RegisterOffset   base;
    private       AtomicInteger    currentLocalOffset;
    private       AtomicInteger    currentParamOffset;
    private       MethodDefinition forMethod;

    public MethodEnvExp(EnvironmentExp parent, RegisterOffset base,
                        MethodDefinition md) {
        super(parent);
        this.base = base;
        this.forMethod = md;

        // les variables locales sont à partir de 1(LB) et vers le haut
        // this est à -2(LB)
        // les paramètres à partir de -3(LB) et vers le bas
        this.currentLocalOffset = new AtomicInteger(base.getOffset());
        this.currentParamOffset = new AtomicInteger(-3);
    }

    @Override
    protected void reserve(NonTypeDefinition def) {
        int offset;
        if (def.isParam()) {
            offset = currentParamOffset.getAndDecrement();
        } else {
            offset = currentLocalOffset.getAndIncrement();
        }
        RegisterOffset pos = new RegisterOffset(offset, base.getRegister());
        def.setDAddr(pos);
    }

    public int getReservedWords() {
        return currentLocalOffset.get() - base.getOffset();
    }

    /**
     * Return the environment's method
     *
     * @return
     */
    public MethodDefinition getMethodDefinition() {
        return this.forMethod;
    }

    @Override
    public boolean isMethodEnv() {
        return true;
    }
}
