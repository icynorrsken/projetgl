package fr.ensimag.deca.tools;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tree.AbstractExpr;
import fr.ensimag.ima.pseudocode.ImmediateInteger;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

public class BoolUtils {
    public static void codeGenBool(DecacCompiler compiler,
                                   NonScratchManager mgr, AbstractExpr operand,
                                   boolean branchIfTrue, String description) {
        LabelManager lm = compiler.getLabelManager();
        Label elseLabel =
                lm.adhoc(description + "." + Boolean.toString(branchIfTrue));
        Label finalLabel = lm.adhoc(description + ".end");

        operand.codeGenCond(compiler, mgr, branchIfTrue, elseLabel);
        compiler.addInstruction(
                new LOAD(new ImmediateInteger(1), mgr.getReg()));
        compiler.addInstruction(new BRA(finalLabel));
        compiler.addLabel(elseLabel);
        compiler.addInstruction(
                new LOAD(new ImmediateInteger(0), mgr.getReg()));
        compiler.addLabel(finalLabel);
    }
}
