package fr.ensimag.deca.tools;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;

import java.util.concurrent.atomic.AtomicInteger;

//TODO: A commenter
public class NonScratchManager {
    private final GPRegister    register;
    private final int           maxRegister;
    private final AtomicInteger maxReached;
    private boolean isUsed = false;

    private NonScratchManager(GPRegister register, int maxRegister,
                              AtomicInteger maxReached) {
        assert register.getNumber() > 1;

        this.register = register;
        this.maxRegister = maxRegister;
        this.maxReached = maxReached;
    }

    private NonScratchManager(GPRegister register, int maxRegister) {
        this(register, maxRegister, new AtomicInteger(register.getNumber()));
    }

    /**
     * Creates a new non-scratch GPR manager starting at R2.
     */
    public static NonScratchManager newManagerR2(DecacCompiler compiler) {
        return new NonScratchManager(Register.R2, compiler.getCompilerOptions()
                .getLastRegister());
    }

    public NonScratchManager next() {
        assert maxReached.get() <= maxRegister;

        if (register.getNumber() == maxReached.get()) {
            maxReached.incrementAndGet();
        }

        NonScratchManager nsm =
                new NonScratchManager(Register.getR(register.getNumber() + 1),
                                      maxRegister, maxReached);

        assert nsm.maxReached == this.maxReached;
        return nsm;
    }

    public boolean isUsed() {
        return this.isUsed;
    }

    public boolean hasReachedLimit() {
        return maxReached.get() == maxRegister;
    }

    public GPRegister getReg() {
        isUsed = true;
        return register;
    }

    public int getMaxReached() {
        return maxReached.get();
    }

    public void codeGenRestoreRegs(DecacCompiler compiler,
                                   int pushInsertionPoint) {
        compiler.addComment("Non-scratch: maxReached " + maxReached.get());
        if (isUsed()) {
            for (int i = register.getNumber(); i <= maxReached.get(); i++) {
                compiler.insertInstruction(pushInsertionPoint,
                                           new PUSH(Register.getR(i)),
                                           "save non-scratch register");
                compiler.addInstruction(new POP(Register.getR(i)),
                                        "restore non-scratch register");
            }
        }
    }
}
