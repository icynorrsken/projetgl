package fr.ensimag.deca.tools;

import java.util.HashMap;
import java.util.Map;

/**
 * Manage unique symbols.
 * <p/>
 * A Symbol contains the same information as a String, but the SymbolTable
 * ensures the uniqueness of a Symbol for a given String value. Therefore,
 * Symbol comparison can be done by comparing references, and the hashCode()
 * method of Symbols can be used to define efficient HashMap (no string
 * comparison or hashing required).
 *
 * @author @AUTHOR@
 */
public class SymbolTable {
    private Map<String, Symbol> map = new HashMap<String, Symbol>();

    /**
     * Create or reuse a symbol.
     * <p/>
     * If a symbol already exists with the same name in this table, then return
     * this Symbol. Otherwise, create a new Symbol and add it to the table.
     */
    public Symbol create(String name) {
        assert (name != null);
        assert (!name.isEmpty());

        Symbol symbol = map.get(name);
        if (null == symbol) {
            symbol = new Symbol(name);
            map.put(name, symbol);
        }
        return symbol;
    }

    public static class Symbol {
        // Constructor is private, so that Symbol instances can only be created
        // through SymbolTable.create factory (which thus ensures uniqueness
        // of symbols).
        private Symbol(String name) {
            super();
            this.name = name;
        }

        public String getName() {
            return name;
        }

        /**
         * getUniqueName is used to differentiate strings with only case
         * differences, as IMA is not case sensitive on labels. Math.abs ensure
         * we don't get a dash in the label, which is not allowed
         *
         * @return unique name for this symbol, suitable for labels
         */
        public String getUniqueName() {
            return "uid" + Math.abs(name.hashCode()) + "." + name;
        }

        @Override
        public String toString() {
            return name;
        }

        private String name;
    }
}
