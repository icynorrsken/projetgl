package fr.ensimag.deca.tools;

import fr.ensimag.deca.tree.AbstractIdentifier;
import fr.ensimag.deca.tree.DeclMethod;
import fr.ensimag.ima.pseudocode.Label;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Manages label creation. Prevents name clashes (homonyms) and illegal
 * characters. Provides predefined labels as public final members.
 */
public class LabelManager {
    /**
     * Labels suitable for referencing across the entire program.
     */
    private HashMap<String, Label> xref = new HashMap<>();

    /**
     * Keeps track of already-used unique labels. All items in this set must be
     * uppercase because IMA is case insensitive.
     */
    private HashSet<String> homonyms = new HashSet<>();

    /**
     * String with which all ad-hoc labels will be prefixed. Must not be null by
     * the time you call adhoc().
     */
    private String adhocPrefix = null;

    /**
     * Keywords that can't be used as a label.
     */
    private static final Set<String> keywords =
            new HashSet<>(Arrays.asList(("GB LB SP " +
                                         "ADD ADDSP BEQ BGE BGT BLE BLT BNE " +
                                         "BOV BRA BSR CMP DEL " +
                                         "DIV ERROR FLOAT FMA HALT INT LEA " +
                                         "LOAD MUL NEW OPP PEA " +
                                         "POP PUSH QUO REM RFLOAT RINT RTS " +
                                         "SEQ SETROUND_DOWNWARD " +
                                         "SETROUND_TONEAREST " +
                                         "SETROUND_TOWARDZERO SETROUND_UPWARD" +
                                         " " +
                                         "SGE SGT SHL SHR SLE SLT SNE SOV " +
                                         "STORE SUB SUBSP TSTO " +
                                         "WFLOAT WFLOATX WINT WNL WSTR")
                                                .split(" ")));

    // predefined labels
    public final Label nullDeref                = errorHandler("nullPtr"),
            arithError                          = errorHandler("arith"),
            dynCastError                        = errorHandler("dynCast"),
            readIntError                        = errorHandler("readInt"),
            readFloatError                      = errorHandler("readFloat"),
            stackOverflow                       = errorHandler("stackOverflow"),
            heapFull                            = errorHandler("heapFull"),
            death                               = errorHandler("die");

    /**
     * Creates a new ad-hoc label that is not meant to be reused across the
     * entire program. Perfect for one-off uses within a Deca statement.
     * <p/>
     * All ad-hoc labels are prefixed with a string representing the current
     * method in a standardized format. This prefix must have been set prior to
     * calling this method.
     *
     * @param description A string detailing the purpose of  the label. May only
     * contain characters suitable for IMA labels.
     */
    public Label adhoc(String description) {
        assert adhocPrefix != null;
        return createUnique(adhocPrefix + "." + description);
    }

    /**
     * Sets the method identifier string with which all new ad-hoc labels will
     * be prefixed.
     *
     * @param method Method containing subsequent labels. If null, a default
     * "main" prefix will be used.
     */
    public void setAdhocPrefix(DeclMethod method) {
        assert adhocPrefix == null;
        adhocPrefix = classDotMethod(method);
    }

    /**
     * Clears ad-hoc prefix string. The prefix must be set again before you can
     * call adhoc() again.
     */
    public void clearAdhocPrefix() {
        adhocPrefix = null;
    }

    /**
     * Returns a label for a method's entry point, in a standardized format.
     */
    public Label getMethodLabel(DeclMethod method) {
        return getXRef(classDotMethod(method));
    }

    /**
     * Returns a label for a method's final return point, in a standardized
     * format.
     */
    public Label getReturn(DeclMethod method) {
        return getXRef(classDotMethod(method) + ".return");
    }

    /**
     * Returns a label for a class's automatic initializer method, in a
     * standardized format.
     */
    public Label getInit(AbstractIdentifier className) {
        return getXRef(className + ".0init");
    }

    /**
     * Returns true if the given string may not be used as a label.
     */
    private static boolean isIllegal(String s) {
        s = s.toUpperCase();
        return s.matches("^([^A-Z]|USER|OOPS\\.).*") || // prefixes
               s.matches("^R(0|[1-9][0-9]*)$") || // GP registers
               keywords.contains(s) || // instructions
               s.length() > 100; // length
    }

    /**
     * Returns a unique Label based on a core string, suitable for use in IMA
     * (takes care of forbidden prefixes). Will append a number if a label
     * already exists for the core string.
     */
    protected Label createUnique(String core) {
        // prevent forbidden prefixes
        core = (isIllegal(core) ? "OOPS." : "") + core;
        
        if (core.length() > 100) {
            core = core.substring(0, 100);
        }
        
        String fixed = core;
        String upper = fixed.toUpperCase();

        // find a suitable suffix in case of homonyms
        for (int i = 2; homonyms.contains(upper); i++) {
            fixed = core + "." + i;
            upper = fixed.toUpperCase();
        }

        homonyms.add(upper);
        return new Label(fixed);
    }

    /**
     * Gets or creates a cross-referenceable label.
     */
    private Label getXRef(String s) {
        Label label = xref.get(s);
        if (label == null) {
            label = createUnique(s);
            xref.put(s, label);
        }
        return label;
    }

    /**
     * Creates an error handler label.
     */
    private Label errorHandler(String description) {
        return createUnique("Err." + description);
    }

    /**
     * Utility function to format methods in a standard way. Returns "main" if
     * method is null.
     */
    private static String classDotMethod(DeclMethod method) {
        if (method == null) {
            return "main";
        } else {
            return method.getMethodName().getMethodDefinition()
                           .getContainingClass().getType() +
                   "." + method.getMethodName();
        }
    }
}
