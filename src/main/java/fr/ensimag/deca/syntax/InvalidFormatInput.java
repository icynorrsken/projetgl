package fr.ensimag.deca.syntax;

import org.antlr.v4.runtime.ParserRuleContext;

/**
 * Syntax error for any oversized affectation.
 * <p/>
 * ex : int x = 5840510685128405406518;
 *
 * @author Jean
 */

public class InvalidFormatInput extends DecaRecognitionException {

    private static final long serialVersionUID = -7618344141973292381L;

    public InvalidFormatInput(DecaParser recognizer, ParserRuleContext ctx) {
        super(recognizer, ctx);
    }

    public InvalidFormatInput(String sortOfInvalidInput, DecaParser recognizer,
                              ParserRuleContext ctx) {
        super(sortOfInvalidInput, recognizer, ctx);
    }

    @Override
    public String getMessage() {
        return "oversized input";
    }
}
