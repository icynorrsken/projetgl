package fr.ensimag.deca;

import fr.ensimag.About;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User-specified options influencing the compilation.
 *
 * @author @AUTHOR@
 */
public class CompilerOptions {
    public static final int QUIET = 0;
    public static final int INFO  = 1;
    public static final int DEBUG = 2;
    public static final int TRACE = 3;

    private static final int NB_REGISTERS_MIN = 4;
    private static final int NB_REGISTERS_MAX = 16;

    /**
     * Returns true if arithmetic oversizing should not be detected
     *
     * @return
     */
    public boolean getNoCheck() {
        return this.noCheck;
    }

    public int getDebug() {
        return debug;
    }

    public boolean getParallel() {
        return parallel;
    }

    public boolean getPrintBanner() {
        return printBanner;
    }

    public boolean getParseOnly() {
        return parseOnly;
    }

    public boolean getVerifOnly() {
        return verifOnly;
    }

    public List<File> getSourceFiles() {
        return Collections.unmodifiableList(sourceFiles);
    }

    /**
     * Return the number of registers we could use
     *
     * @return
     */
    public int getNbRegisters() {
        return this.nbRegisters;
    }

    /**
     * Return the last register usable
     *
     * @return
     */
    public int getLastRegister() {
        return this.nbRegisters - 1;
    }

    /**
     * Returns true is warning messages whould be emitted
     *
     * @return
     */
    public boolean displayWarnings() {
        return this.displayWarnings;
    }

    protected void setMaxRegister(int nb) throws CLIException {
        if (nb < NB_REGISTERS_MIN || nb > NB_REGISTERS_MAX)
            throw new CLIException(
                    "The maximum number of usable registers must be between " +
                    NB_REGISTERS_MIN + " and " + NB_REGISTERS_MAX);

        this.nbRegisters = nb;
    }

    private int     debug           = 0;
    private boolean parallel        = false;
    private boolean printBanner     = false;
    private boolean parseOnly       = false;
    private boolean verifOnly       = false;
    private boolean noCheck         = false;
    private boolean displayWarnings = false;

    private List<File> sourceFiles = new ArrayList<File>();

    private int nbRegisters = 4;
            //maximal register we could use
            // NB_REGISTERS_MIN<=nb<=NB_REGISTERS_MAX

    /**
     * Get all arguments the user specified running decac and set members of
     * this consequently
     *
     * @param args
     *
     * @throws CLIException
     */
    public void parseArgs(String[] args) throws CLIException {
        boolean argR = false; //true if previous string arg was '-r'

        for (String arg : args) {
            if (printBanner)    //argument b is not used alone => error
                break;//if -b is used, other arguments are ignored and there
                // is no compilation

            switch (arg) {
                case "-b":
                    printBanner = true;
                    break;

                case "-p":
                    if (this.verifOnly)
                        throw new CLIException(
                                "Arguments -p and -v are incompatible");

                    this.parseOnly = true;
                    break;

                case "-P":
                    this.parallel = true;
                    break;

                case "-v":
                    if (this.parseOnly)
                        throw new CLIException(
                                "Arguments -p and -v are incompatible");

                    this.verifOnly = true;
                    break;
                case "-n":
                    noCheck = true;
                    break;

                case "-d"://Repeat this argument a few times to get more
                // informations in log
                    if (this.debug < TRACE)
                        this.debug++;
                    break;

                case "-r":
                    argR = true;
                    break;

                case "-w":
                    this.displayWarnings = true;
                    break;

                default:
                    if (!argR)
                        sourceFiles.add(new File(arg));
                    else//get the specified number after the argument -r
                    {
                        try {
                            int nb = Integer.parseInt(arg);
                            this.setMaxRegister(nb);
                        } catch (NumberFormatException nfe) {
                            throw new CLIException(
                                    "The correct syntax of -r is -r X where X" +
                                    " is an integer");
                        }

                        argR = false;
                    }

                    break;
            }
        }

        if (argR)//'-r' is specified without value
            throw new CLIException(
                    "The correct syntax of -r is -r X where X is an integer");

        Logger logger = Logger.getRootLogger();
        // map command-line debug option to log4j's level.
        switch (getDebug()) {
            case QUIET:
                logger.setLevel(Level.WARN);
                break; // keep default
            case INFO:
                logger.setLevel(Level.INFO);
                break;
            case DEBUG:
                logger.setLevel(Level.DEBUG);
                break;
            case TRACE:
                logger.setLevel(Level.TRACE);
                break;
            default:
                logger.setLevel(Level.ALL);
                break;
        }
        logger.info("Application-wide trace level set to " + logger.getLevel());

        boolean assertsEnabled = false;
        assert assertsEnabled = true; // Intentional side effect!!!
        if (assertsEnabled) {
            logger.info("Java assertions enabled");
        } else {
            logger.info("Java assertions disabled");
        }
    }

    protected void displayUsage() {
        System.out.println(About.DECAC_SYNTAXE);
    }
}
