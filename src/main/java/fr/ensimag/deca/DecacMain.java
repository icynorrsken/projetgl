package fr.ensimag.deca;

import fr.ensimag.About;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Main class for the command-line Deca compiler.
 *
 * @author @AUTHOR@
 */
public class DecacMain {
    private static Logger LOG = Logger.getLogger(DecacMain.class);

    public static void main(String[] args) {
        boolean error = false;
        final CompilerOptions options = new CompilerOptions();

        try {
            options.parseArgs(args);
        } catch (CLIException e) {
            System.err
                    .println("Error during option parsing:\n" + e.getMessage());
            options.displayUsage();
            System.exit(1);
        }
        // example log4j message.
        LOG.info("Decac compiler started");
        LOG.debug("Debug mode activated");
        LOG.trace("Trace mode activated");

        if (options.getPrintBanner()) {
            System.out.println(About.TEAM_BANNER);
            System.exit(error ? 1
                              : 0);    //if -b is used, other arguments are
                              // ignored and there is no compilation
        }

        if (options.getSourceFiles().isEmpty()) {
            System.out.println(About.DECAC_SYNTAXE);
            System.exit(error ? 1 : 0);
        }

        if (options.getParallel()) {
            // Step 1: Configure Executor
            // Uses FixedThreadPool executor
            int nbAvalaibleProc =
                    java.lang.Runtime.getRuntime().availableProcessors();
            ExecutorService executor =
                    Executors.newFixedThreadPool(nbAvalaibleProc);

            //Step2 : Initialisation erreur
            error = false;
            ArrayList<DecacCompilerCallable> compilerList =
                    new ArrayList<DecacCompilerCallable>();

            //Step3 : Create different threads
            for (int i = 0; i < options.getSourceFiles().size(); i++) {
                DecacCompilerCallable compiler =
                        new DecacCompilerCallable(options,
                                                  options.getSourceFiles()
                                                          .get(i));
                compilerList.add(compiler);
                executor.submit(compiler);
            }

            for (DecacCompilerCallable compiler : compilerList) {
                while (!compiler.hasFinished()) {
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                    }
                }
                error = error || compiler.hasFinishedWithSuccess();
            }
        } else {
            for (File source : options.getSourceFiles()) {
                DecacCompiler compiler = new DecacCompiler(options, source);
                if (compiler.compile()) {
                    error = true;
                }
            }
        }
        System.exit(error ? 1 : 0);
    }
}
