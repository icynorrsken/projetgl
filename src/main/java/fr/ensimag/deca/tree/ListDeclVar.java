package fr.ensimag.deca.tree;

import fr.ensimag.deca.tools.IndentPrintStream;

/**
 * List of variables within a declaration (e.g. x, y=42).
 *
 * @author @AUTHOR@
 */
public class ListDeclVar extends TreeList<AbstractDeclVar> {

    @Override
    public void decompile(IndentPrintStream s) {
        String prefix = "";
        for (AbstractDeclVar var : this.getList()) {
            s.print(prefix);
            var.decompile(s);
            prefix = ", ";
        }
    }
}
