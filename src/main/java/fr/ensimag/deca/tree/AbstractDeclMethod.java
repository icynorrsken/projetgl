package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;

public abstract class AbstractDeclMethod extends Tree {
    public abstract void verifySignature(DecacCompiler compiler,
                                         EnvironmentExp localEnv,
                                         ClassDefinition currentClassDef)
            throws ContextualError;

    public abstract void verifyBody(DecacCompiler compiler,
                                    EnvironmentExp localEnv,
                                    ClassDefinition currentClass)
            throws ContextualError;

    public abstract void codeGenMethod(DecacCompiler compiler,
                                       ClassDefinition currentClass);
}
