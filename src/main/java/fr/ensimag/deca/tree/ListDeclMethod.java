package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.tools.IndentPrintStream;

public class ListDeclMethod extends TreeList<AbstractDeclMethod> {
    @Override
    public void decompile(IndentPrintStream s) {
        for (AbstractDeclMethod m : getList()) {
            m.decompile(s);
        }
    }

    public void verifySignatures(DecacCompiler compiler,
                                 ClassDefinition currentClassDef)
            throws ContextualError {
        for (AbstractDeclMethod d : getList()) {
            d.verifySignature(compiler, currentClassDef.getMembers(),
                              currentClassDef);
        }
    }

    public void verifyBodies(DecacCompiler compiler,
                             ClassDefinition currentClass)
            throws ContextualError {
        for (AbstractDeclMethod d : getList()) {
            d.verifyBody(compiler, currentClass.getMembers(), currentClass);
        }
    }
}
