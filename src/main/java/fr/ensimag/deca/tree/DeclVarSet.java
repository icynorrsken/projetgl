package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;
import org.apache.commons.lang.Validate;

import java.io.PrintStream;

/**
 * @author @AUTHOR@
 */
public class DeclVarSet extends AbstractDeclVarSet {
    public AbstractIdentifier getType() {
        return type;
    }

    public ListDeclVar getDeclVars() {
        return declVars;
    }

    private AbstractIdentifier type;
    private ListDeclVar        declVars;

    public DeclVarSet(AbstractIdentifier type, ListDeclVar declVars) {
        super();
        Validate.notNull(type);
        Validate.notNull(declVars);
        Validate.isTrue(!declVars.isEmpty(),
                        "A list of variable declarations cannot be empty");
        this.type = type;
        this.declVars = declVars;
    }

    @Override
    protected Type verifyDeclVarSet(DecacCompiler compiler,
                                    EnvironmentExp localEnv,
                                    ClassDefinition currentClass)
            throws ContextualError {
        Type t = type.verifyType(compiler);
        if (t.isVoid()) {
            throw new ContextualError(
                    "can't declare void variables (rule 3.19)",
                    type.getLocation());
        }

        for (AbstractDeclVar declVar : declVars.getList()) {
            declVar.verifyDeclVar(t, compiler, localEnv, currentClass);
        }

        return t;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        type.decompile(s);
        s.print(" ");
        declVars.decompile(s);
        s.print(";");
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        type.iter(f);
        declVars.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        type.prettyPrint(s, prefix, false);
        declVars.prettyPrint(s, prefix, true);
    }

    @Override
    public void codeGenDeclVarSet(DecacCompiler compiler,
                                  NonScratchManager mgr) {
        for (AbstractDeclVar adv : declVars.getList()) {
            adv.codeGenDeclVar(compiler, mgr);
        }
    }

    @Override
    public int getNumberOfVars() {
        return declVars.size();
    }
}
