package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;

/**
 * List of declarations (e.g. int x; float y,z).
 *
 * @author @AUTHOR@
 */
public class ListDeclVarSet extends TreeList<AbstractDeclVarSet> {

    @Override
    public void decompile(IndentPrintStream s) {
        for (AbstractDeclVarSet advs : getList()) {
            advs.decompile(s);
            s.println();
        }
    }

    public void codeGenListDeclVarSet(DecacCompiler compiler,
                                      NonScratchManager mgr) {
        for (AbstractDeclVarSet advs : getList()) {
            compiler.addComment(advs.decompile());
            advs.codeGenDeclVarSet(compiler, mgr);
        }
    }

    public int getTotalVariableCount() {
        int sum = 0;
        for (AbstractDeclVarSet advs : getList()) {
            sum += advs.getNumberOfVars();
        }
        return sum;
    }
}
