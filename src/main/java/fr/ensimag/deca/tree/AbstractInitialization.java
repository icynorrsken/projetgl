package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.NonScratchManager;

/**
 * Initialization (of variable, field, ...)
 *
 * @author @AUTHOR@
 */
public abstract class AbstractInitialization extends Tree {

    protected abstract void verifyInitialization(DecacCompiler compiler, Type t,
                                                 EnvironmentExp localEnv,
                                                 ClassDefinition currentClass)
            throws ContextualError;

    public abstract void codeGen(DecacCompiler compiler, NonScratchManager mgr);
}
