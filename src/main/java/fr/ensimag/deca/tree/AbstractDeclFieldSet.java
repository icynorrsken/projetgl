package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.NonScratchManager;

public abstract class AbstractDeclFieldSet extends Tree {

    protected abstract Type verify(DecacCompiler compiler,
                                   EnvironmentExp localEnv,
                                   ClassDefinition currentClass)
            throws ContextualError;

    public abstract void codeGen(DecacCompiler compiler, NonScratchManager mgr);
}
