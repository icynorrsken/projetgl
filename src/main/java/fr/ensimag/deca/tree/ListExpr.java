package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Signature;
import fr.ensimag.deca.tools.IndentPrintStream;

/**
 * List of expressions (eg list of parameters).
 *
 * @author @AUTHOR@
 */
public class ListExpr extends TreeList<AbstractExpr> {
    public void verifyParams(DecacCompiler compiler, EnvironmentExp localEnv,
                             ClassDefinition currentClass, Signature signature,
                             Location location) throws ContextualError {

        if (size() < signature.size()) {
            throw new ContextualError(
                    "Missing parameters : " + size() + " found vs " +
                    signature.size() + " expected", getLocation());
        } else if (size() > signature.size()) {
            throw new ContextualError(
                    "Too many parameters : " + size() + " found vs " +
                    signature.size() + " expected", getLocation());
        }

        for (int i = 0; i < getList().size(); i++) {
            AbstractExpr newArg =
                    Assign.compatibleRHS(compiler, localEnv, currentClass,
                                         signature.paramNumber(i),
                                         getList().get(i));
            set(i, newArg);
        }
    }

    @Override
    public void decompile(IndentPrintStream s) {
        String prefix = "";
        for (AbstractExpr expr : getList()) {
            s.print(prefix);
            expr.decompile(s);
            prefix = ", ";
        }
    }
}
