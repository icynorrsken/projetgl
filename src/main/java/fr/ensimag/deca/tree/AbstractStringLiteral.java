package fr.ensimag.deca.tree;

/**
 * @author @AUTHOR@
 */
public abstract class AbstractStringLiteral extends AbstractExpr {

    public abstract String getValue();
}
