package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.MethodEnvExp;
import fr.ensimag.ima.pseudocode.InlinePortion;

import java.io.PrintStream;

public class MethodAssBody extends AbstractBody {

    @Override
    String prettyPrintNode() {
        return "MethodAsmBody";
    }

    StringLiteral assCode;

    public MethodAssBody(StringLiteral assCode) {
        this.assCode = assCode;
    }

    @Override
    protected void codeGen(DecacCompiler compiler, DeclMethod method) {
        /* Handling indentation */
        String indentCode = assCode.getValue().replaceAll("\t",
                                                          ""); //removes all
                                                          // \t explicitly
                                                          // written by the
                                                          // deca dev
        indentCode = indentCode.replaceAll("[ ]{2,}",
                                           ""); //removes all \t (made with
                                           // spaces) explicitly written by
                                           // the deca dev
        indentCode = "\t" + indentCode.replaceAll("\n",
                                                  "\n\t"); //write a \t after
                                                  // each instruction on a
                                                  // new line

        InlinePortion inlineAssCode = new InlinePortion(indentCode);
        compiler.add(inlineAssCode);
    }

    @Override
    protected void verifyBody(DecacCompiler compiler, MethodEnvExp localEnv,
                              ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        assCode.verifyExpr(compiler, localEnv, currentClass);
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("asm (");
        assCode.decompile(s);
        s.print(");");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        assCode.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        assCode.iter(f);
    }
}
