package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

import java.io.PrintStream;

public class Selection extends AbstractLValue {

    private AbstractExpr       leftOperand;
    private AbstractIdentifier rightOperand;

    public Selection(AbstractExpr leftOperand,
                     AbstractIdentifier rightOperand) {
        this.leftOperand = leftOperand;
        this.rightOperand = rightOperand;
    }

    @Override
    public DAddr codeGenAddress(DecacCompiler compiler, NonScratchManager mgr) {
        DAddr leftOpAddr = leftOperand.codeGenAddress(compiler, mgr);

        RegisterOffset addrOff =
                new RegisterOffset(rightOperand.getFieldDefinition().getIndex(),
                                   mgr.getReg());

        compiler.addInstruction(new LOAD(leftOpAddr, mgr.getReg()));
        compiler.checkNullDeref(mgr.getReg());
        return addrOff;
    }

    @Override
    public Definition getDefinition() {
        return rightOperand.getDefinition();
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
                           ClassDefinition currentClass)
            throws ContextualError {
        Type leftType =
                leftOperand.verifyExpr(compiler, localEnv, currentClass);

        if (!leftType.isClass()) {
            throw new ContextualError(
                    "Left operand in selection must be class instance (rule 3" +
                    ".70)",
                    leftOperand.getLocation());
        }

        assert leftType instanceof ClassType;
        ClassType classType = (ClassType) leftType;

        Type fieldType = rightOperand
                .verifyExpr(compiler, classType.getDefinition().getMembers(),
                            classType.getDefinition());

        if (!rightOperand.getDefinition().isField()) {
            throw new ContextualError(
                    "Right operand in selection must be a field (rule 3.75)",
                    getLocation());
        }

        FieldDefinition selectedDef = rightOperand.getFieldDefinition();
        assert null != selectedDef;

        if (Visibility.PROTECTED == selectedDef.getVisibility()) {
            // throw new UnsupportedOperationException("implement me -
            // protected field check");
            // If we're derivating the class showned in the left operand,
            // we can acces to the field, if not, well, not.
            if (currentClass == null) {
                // We're in the main, cannot access the field, never, EVER
                throw new ContextualError(
                        "Cannot access a protected field from" +
                        " main code (rule 3.70)", getLocation());
            }

            if (!leftType.assignCompatible(currentClass.getType())) {
                throw new ContextualError(
                        "Cannot access a protected field from" +
                        " outside a class that doesn't extends the class" +
                        " that declare the field (rule 3.71)", getLocation());
            }

            if (!currentClass.getType().assignCompatible(leftType)) {
                throw new ContextualError(
                        "Cannot access a protected field from" +
                        " another instance of the same class (or one of its " +
                        "parent)",
                        getLocation());
            }
        }

        setType(fieldType);
        return fieldType;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        leftOperand.decompile(s);
        s.print(".");
        rightOperand.decompile(s);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        leftOperand.prettyPrint(s, prefix, false);
        rightOperand.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        leftOperand.iter(f);
        rightOperand.iter(f);
    }
}
