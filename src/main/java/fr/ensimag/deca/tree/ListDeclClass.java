package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.tools.IndentPrintStream;
import org.apache.log4j.Logger;

/**
 * @author @AUTHOR@
 */
public class ListDeclClass extends TreeList<AbstractDeclClass> {
    private static final Logger LOG = Logger.getLogger(ListDeclClass.class);

    @Override
    public void decompile(IndentPrintStream s) {
        for (AbstractDeclClass c : getList()) {
            c.decompile(s);
            s.println();
        }
    }

    public void allocateMethodsTable() {
        int indexFromGb =
                1;//TODO : set to 3 when equals method is implemented to
                // object class

        for (AbstractDeclClass c : getList()) {
            //if(this.superClassName==null) //if the superClass is the
            // default Object class

            c.allocateMethodsTable(indexFromGb);
            indexFromGb += c.getTotalNumberOfMethods() + 1;
        }
    }

    /**
     * @param compiler
     *
     * @return Last offset non used from GB
     */
    public void codeGenMethodsTable(DecacCompiler compiler) {
        //Table des méthodes de Object
        // No longer used since Object is included in classes declarations
        // compiler.addInstruction(new LOAD(new NullOperand(),Register.getR
        // (0)));
        // compiler.addInstruction(new STORE(Register.getR(0),new
        // RegisterOffset(1, Register.GB)));

        for (AbstractDeclClass c : getList()) {
            c.codeGenMethodsTable(compiler);
        }
    }

    public AbstractDeclClass getLastClass() {
        if (getList().size() == 0)
            return null;

        return getList().get(getList().size() - 1);
    }

    /**
     * Attribute grammar's pass 1
     */
    void verifyListClass(DecacCompiler compiler) throws ContextualError {
        LOG.debug("verify listClass: start");
        for (AbstractDeclClass c : getList()) {
            c.verifyClass(compiler);
        }
    }

    /**
     * Attribute grammar's pass 2
     */
    public void verifyListClassMembers(DecacCompiler compiler)
            throws ContextualError {
        for (AbstractDeclClass c : getList()) {
            c.verifyClassMembers(compiler);
        }
    }

    /**
     * Attribute grammar's pass 3
     */
    public void verifyListClassBody(DecacCompiler compiler)
            throws ContextualError {
        for (AbstractDeclClass c : getList()) {
            c.verifyClassBody(compiler);
        }
    }

    public void codeGenClasses(DecacCompiler compiler) {
        for (AbstractDeclClass c : this.getList()) {
            c.generateInitMethod(compiler);
            c.generateMethods(compiler);
        }
    }
}
