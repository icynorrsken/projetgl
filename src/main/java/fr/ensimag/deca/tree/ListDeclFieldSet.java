package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.tools.IndentPrintStream;

public class ListDeclFieldSet extends TreeList<AbstractDeclFieldSet> {

    @Override
    public void decompile(IndentPrintStream s) {
        for (AbstractDeclFieldSet adfs : getList()) {
            adfs.decompile(s);
            s.println();
        }
    }

    public void verifyFieldSet(DecacCompiler compiler,
                               ClassDefinition currentClass)
            throws ContextualError {
        for (AbstractDeclFieldSet d : getList()) {
            d.verify(compiler, currentClass.getMembers(), currentClass);
        }
    }
}
