package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Signature;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.MethodEnvExp;

public class ListDeclParam extends TreeList<AbstractDeclParam> {

    private Signature sig;

    public Signature getSignature() {
        assert null != sig;
        return sig;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("(");
        String prefix = "";
        for (AbstractDeclParam dp : getList()) {
            s.print(prefix);
            dp.decompile(s);
            prefix = ", ";
        }
        s.print(")");
    }

    public Signature verifyParams(DecacCompiler compiler)
            throws ContextualError {
        assert null == sig; // don't verify twice

        sig = new Signature();

        for (AbstractDeclParam adp : getList()) {
            adp.verifyFormalParam(compiler);
            sig.add(adp.getType());
        }

        return sig;
    }

    public void addParams(DecacCompiler compiler, MethodEnvExp methodEnv,
                          ClassDefinition currentClass) throws ContextualError {
        for (AbstractDeclParam dp : getList()) {
            try {
                methodEnv.declare(dp.getParamName().getName(),
                                  dp.getParamName().getNonTypeDefinition());
            } catch (EnvironmentExp.DoubleDefException e) {
                throw new ContextualError(
                        "Parameter " + dp.getParamName().getName() +
                        " is already defined in this method", getLocation());
            }
        }
    }
}
