package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;

import java.io.PrintStream;

public class DeclFieldSet extends AbstractDeclFieldSet {

    private Visibility         visibility;
    private AbstractIdentifier type;
    private ListDeclField      declFields;

    @Override
    String prettyPrintNode() {
        return "[visibility=" + visibility.name() + "] " +
               super.prettyPrintNode();
    }

    public DeclFieldSet(Visibility visibility, AbstractIdentifier type,
                        ListDeclField declFields) {
        this.visibility = visibility;
        this.type = type;
        this.declFields = declFields;
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        type.prettyPrint(s, prefix, false);
        declFields.prettyPrint(s, prefix, true);
    }

    @Override
    protected Type verify(DecacCompiler compiler, EnvironmentExp localEnv,
                          ClassDefinition currentClass) throws ContextualError {
        Type t = type.verifyType(compiler);
        if (t.equals(compiler.voidType)) {
            throw new ContextualError("Cannot declare void field (rule 2.5)",
                                      getLocation());
        }

        for (AbstractDeclField d : declFields.getList()) {
            FieldDefinition fd =
                    d.verifyDeclField(compiler, localEnv, currentClass, t,
                                      visibility);
        }

        return t;
    }

    @Override
    public void codeGen(DecacCompiler compiler, NonScratchManager mgr) {
        for (AbstractDeclField field : this.declFields.getList()) {
            field.generateFieldInit(compiler, mgr);
        }
    }

    @Override
    public void decompile(IndentPrintStream s) {
        visibility.decompile(s);
        type.decompile(s);
        s.print(" ");
        declFields.decompile(s);
        s.print(";");
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        type.iter(f);
        declFields.iter(f);
    }
}
