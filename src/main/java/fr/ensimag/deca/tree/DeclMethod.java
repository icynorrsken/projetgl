package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.LabelManager;
import fr.ensimag.deca.tools.MethodEnvExp;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;

import java.io.PrintStream;

public class DeclMethod extends AbstractDeclMethod {

    private AbstractIdentifier returnTypeName;
    private AbstractIdentifier methodName;
    private ListDeclParam      params;
    private AbstractBody       methodBody;

    private MethodEnvExp methodEnv;

    public DeclMethod(AbstractIdentifier returnTypeName,
                      AbstractIdentifier methodName, ListDeclParam params,
                      AbstractBody methodBody) {
        this.returnTypeName = returnTypeName;
        this.methodName = methodName;
        this.params = params;
        this.methodBody = methodBody;
    }

    public AbstractIdentifier getReturnTypeName() {

        return returnTypeName;
    }

    public AbstractIdentifier getMethodName() {
        return methodName;
    }

    public ListDeclParam getParams() {
        return params;
    }

    public AbstractBody getMethodBody() {
        return methodBody;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        returnTypeName.decompile(s);
        s.print(" ");
        methodName.decompile(s);
        params.decompile(s);
        s.print(" ");
        methodBody.decompile(s);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        returnTypeName.prettyPrint(s, prefix, false);
        methodName.prettyPrint(s, prefix, false);
        params.prettyPrint(s, prefix, false);
        methodBody.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        returnTypeName.iter(f);
        methodName.iter(f);
        params.iter(f);
        methodBody.iter(f);
    }

    @Override
    public void verifySignature(DecacCompiler compiler, EnvironmentExp localEnv,
                                ClassDefinition currentClassDef)
            throws ContextualError {
        LabelManager lm = compiler.getLabelManager();

        Type returnType = returnTypeName.verifyType(compiler);
        returnTypeName.setType(returnType);

        Signature sig = this.params.verifyParams(compiler);

        NonTypeDefinition d = localEnv.get(methodName.getName());

        if (d != null) {
            if (d.isMethod()) {
                // Checker si on est pas dans la même classe
                MethodDefinition md = (MethodDefinition) d;
                if (md.getContainingClass().equals(currentClassDef)) {
                    throw new ContextualError("Method " + methodName +
                                              " already declared in this class",
                                              getLocation());
                }
                // Checker si les paramètres coincident avec une réécriture
                if (!md.getSignature().equals(sig)) {
                    throw new ContextualError(
                            "Signature of method " + methodName +
                            " doesn't match overriden method", getLocation());
                }
                // Checker si le type de retour colle avec le tpe de la
                // méthode réécrite
                Type mdType = md.getType();
                if ((!mdType.isClass() && !mdType.sameType(returnType)) ||
                        (mdType.isClass() && !mdType.assignCompatible(returnType)))
                {
                    throw new ContextualError(
                            "Return type of method " + methodName +
                            " doesn't match overriden method", getLocation());
                }
            } else {
                throw new ContextualError(
                        "Cannot declare method " + methodName +
                        ", field with same name already exists at " +
                        d.getLocation(), getLocation());
            }
        }

        MethodDefinition nmd =
                new MethodDefinition(returnType, getLocation(), currentClassDef,
                                     sig, currentClassDef.incNumberOfMethods(),
                                     this);
        this.methodName.setDefinition(nmd);

        try {
            localEnv.declare(methodName.getName(), nmd);
        } catch (EnvironmentExp.DoubleDefException e) {
            throw new DecacInternalError(
                    "compiler bug - we've already checked" +
                    "for this method name's uniqueness in this class");
        }
        nmd.setLabel(lm.getMethodLabel(this));

        this.methodName.setType(returnType);
    }

    @Override
    public void verifyBody(DecacCompiler compiler, EnvironmentExp localEnv,
                           ClassDefinition currentClass)
            throws ContextualError {

        methodEnv =
                new MethodEnvExp(localEnv, new RegisterOffset(1, Register.LB),
                                 methodName.getMethodDefinition());

        this.params.addParams(compiler, methodEnv, currentClass);

        this.methodBody.verifyBody(compiler, methodEnv, currentClass,
                                   returnTypeName.getType());
    }

    @Override
    public void codeGenMethod(DecacCompiler compiler,
                              ClassDefinition currentClass) {
        // Method name
        compiler.addLabel(methodName.getMethodDefinition().getLabel());
        this.methodBody.codeGen(compiler, this);
    }
}
