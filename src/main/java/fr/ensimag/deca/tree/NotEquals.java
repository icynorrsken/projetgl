package fr.ensimag.deca.tree;

import fr.ensimag.ima.pseudocode.Instruction;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BNE;

/**
 * @author @AUTHOR@
 */
public class NotEquals extends AbstractOpExactCmp {

    public NotEquals(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected String getOperatorName() {
        return "!=";
    }

    @Override
    protected Instruction getBranchInstruction(boolean branchIfTrue,
                                               Label nextLabel) {
        if (branchIfTrue) {
            return new BNE(nextLabel);
        } else {
            return new BEQ(nextLabel);
        }
    }
}
