package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.instructions.DIV;
import fr.ensimag.ima.pseudocode.instructions.QUO;

/**
 * @author @AUTHOR@
 */
public class Divide extends AbstractOpArith {
    public Divide(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected String getOperatorName() {
        return "/";
    }

    @Override
    protected void calculateArithFrom(DecacCompiler compiler, GPRegister op1,
                                      GPRegister op2) {
        if (this.getType().isFloat()) {
            compiler.addInstruction(new DIV(op1, op2));
        } else {
            assert (this.getType().isInt());
            compiler.addInstruction(new QUO(op1, op2));
        }
    }
}
