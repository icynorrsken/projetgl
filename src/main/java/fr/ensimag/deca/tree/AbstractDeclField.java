package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.NonScratchManager;

public abstract class AbstractDeclField extends Tree {

    public abstract FieldDefinition verifyDeclField(DecacCompiler compiler,
                                                    EnvironmentExp localEnv,
                                                    ClassDefinition
                                                            currentClass,
                                                    Type t,
                                                    Visibility visibility)
            throws ContextualError;

    protected abstract void generateFieldInit(DecacCompiler compiler,
                                              NonScratchManager mgr);
}
