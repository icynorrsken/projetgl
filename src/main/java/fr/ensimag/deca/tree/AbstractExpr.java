package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.ImmediateInteger;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.*;

import org.apache.commons.lang.Validate;

import java.io.PrintStream;

/**
 * Expression, i.e. anything that has a value.
 *
 * @author @AUTHOR@
 */
public abstract class AbstractExpr extends AbstractInst {
    /**
     * @return true if the expression does not correspond to any concrete token
     * in the source code (and should be decompiled to the empty string).
     */
    boolean isImplicit() {
        return false;
    }

    /**
     * Get the type decoration associated to this expression (i.e. the type
     * computed by contextual verification).
     */
    public Type getType() {
        return type;
    }

    protected void setType(Type type) {
        Validate.notNull(type);
        this.type = type;
    }

    private Type type;

    @Override
    protected void checkDecoration() {
        if (getType() == null) {
            throw new DecacInternalError(
                    "Expression " + decompile() + " has no Type decoration");
        }
    }

    /**
     * Verify the expression for contextual error.
     *
     * @param compiler
     * @param localEnv Environment in which the expression should be checked
     * @param currentClass Definition of the class containing the
     * expression, or
     * null in the main program.
     *
     * @return the Type of the expression
     */
    public abstract Type verifyExpr(DecacCompiler compiler,
                                    EnvironmentExp localEnv,
                                    ClassDefinition currentClass)
            throws ContextualError;

    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
                              ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        // TODO on a le droit d'ignorer ce returnType ?
        Type t = verifyExpr(compiler, localEnv, currentClass);
        setType(t);
    }

    /**
     * Verify the expression as a condition, i.e. check that the type is
     * boolean.
     *
     * @param localEnv Environment in which the condition should be checked.
     * @param currentClass Definition of the class containing the
     * expression, or
     * null in the main program.
     */
    void verifyCondition(DecacCompiler compiler, EnvironmentExp localEnv,
                         ClassDefinition currentClass) throws ContextualError {
        this.verifyExpr(compiler, localEnv, currentClass);
        if (!getType().isBoolean()) {
            throw new ContextualError("Condition must be boolean (rule 3.34)",
                                      getLocation());
        }
    }

    /**
     * Generate code to print the expression
     *
     * @param compiler
     */
    protected void codeGenPrint(DecacCompiler compiler, NonScratchManager mgr,
                                boolean printHexa) {
        // don't use R1 as lowest register because the expression
        // might call println, which will overwrite R1
        codeGenInst(compiler, mgr);
        compiler.addInstruction(new LOAD(mgr.getReg(), Register.R1));
        if (type.isFloat()) {
            if (printHexa) {
                compiler.addInstruction(new WFLOATX());
            } else {
                compiler.addInstruction(new WFLOAT());
            }
        } else {
            assert (type.isInt());
            compiler.addInstruction(new WINT());
        }
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler, NonScratchManager mgr) {
        throw new UnsupportedOperationException("not yet implemented");
    }

    public void codeGenCond(DecacCompiler compiler, NonScratchManager mgr,
                            boolean branchIfTrue, Label nextLabel) {
        assert getType().isBoolean();

        this.codeGenInst(compiler, mgr);
        compiler.addInstruction(new CMP(new ImmediateInteger(0), mgr.getReg()));
        if (branchIfTrue) {
            compiler.addInstruction(new BNE(nextLabel));
        } else {
            compiler.addInstruction(new BEQ(nextLabel));
        }
    }

    public DAddr codeGenAddress(DecacCompiler compiler, NonScratchManager mgr) {
        throw new UnsupportedOperationException(
                "can't take address of this kind of abstract expression");
   
    }

    @Override
    protected void decompileInst(IndentPrintStream s) {
        decompile(s);
        s.print(";");
    }

    @Override
    protected void prettyPrintType(PrintStream s, String prefix) {
        Type t = getType();
        if (t != null) {
            s.print(prefix);
            s.print("type: ");
            s.print(t);
            s.println();
        }
    }
}
