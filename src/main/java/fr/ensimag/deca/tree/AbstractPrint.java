package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;
import org.apache.commons.lang.Validate;

import java.io.PrintStream;

/**
 * Print statement (print, println, ...).
 *
 * @author @AUTHOR@
 */
public abstract class AbstractPrint extends AbstractInst {

    private boolean printHex;
    private ListExpr arguments = new ListExpr();

    abstract String getSuffix();

    public AbstractPrint(boolean printHex, ListExpr arguments) {
        Validate.notNull(arguments);
        this.arguments = arguments;
        this.printHex = printHex;
    }

    public ListExpr getArguments() {
        return arguments;
    }

    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
                              ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        for (AbstractExpr arg : arguments.getList()) {
            arg.verifyExpr(compiler, localEnv, currentClass);
            Type t = arg.getType();
            
            assert (t != null);
            
            // Check if the format is correct
            if(arg instanceof Identifier) {
                Identifier id = (Identifier) arg;
                if(id.getDefinition().isMethod()) {
                    // Illogic method call
                    throw new ContextualError("Malformated methodCall",
                            id.getLocation());
                }
            }
            
            if (!t.isFloat() && !t.isInt() && !t.isString()) {
                throw new ContextualError(
                        "can't print values of type " + t.getName() +
                        " (rule 3.36)", arg.getLocation());
            }
        }
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler, NonScratchManager mgr) {
        for (AbstractExpr a : getArguments().getList()) {
            a.codeGenPrint(compiler, mgr, this.printHex);
        }
    }

    private boolean getPrintHex() {
        return printHex;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("print");
        s.print(getSuffix());
        if (printHex) {
            s.print("x");
        }
        s.print("(");
        getArguments().decompile(s);
        s.print(");");
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        arguments.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        arguments.prettyPrint(s, prefix, true);
    }
}
