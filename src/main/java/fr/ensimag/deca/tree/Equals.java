package fr.ensimag.deca.tree;

import fr.ensimag.ima.pseudocode.Instruction;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BNE;

/**
 * @author @AUTHOR@
 */
public class Equals extends AbstractOpExactCmp {

    public Equals(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected String getOperatorName() {
        return "==";
    }

    @Override
    protected Instruction getBranchInstruction(boolean branchIfTrue,
                                               Label nextLabel) {
        if (branchIfTrue) {
            return new BEQ(nextLabel);
        } else {
            return new BNE(nextLabel);
        }
    }
}
