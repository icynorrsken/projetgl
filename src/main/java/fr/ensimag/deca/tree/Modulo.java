package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.instructions.REM;

/**
 * @author @AUTHOR@
 */
public class Modulo extends AbstractOpArith {

    public Modulo(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
                           ClassDefinition currentClass)
            throws ContextualError {
        Type tleft = this.getLeftOperand()
                .verifyExpr(compiler, localEnv, currentClass);
        Type tright = this.getRightOperand()
                .verifyExpr(compiler, localEnv, currentClass);

        if (!tleft.isInt() || !tright.isInt()) {
            throw new ContextualError(
                    "The operands for modulo operation are not both integers " +
                    "(" +
                    tleft.getName() + ", " + tright.getName() + ")",
                    this.getLocation());
        }

        this.setType(tleft);

        return tleft;
    }

    @Override
    protected String getOperatorName() {
        return "%";
    }

    @Override
    protected void calculateArithFrom(DecacCompiler compiler, GPRegister op1,
                                      GPRegister op2) {
        compiler.addInstruction(new REM(op1, op2));
    }
}
