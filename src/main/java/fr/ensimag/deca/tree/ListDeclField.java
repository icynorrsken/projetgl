package fr.ensimag.deca.tree;

import fr.ensimag.deca.tools.IndentPrintStream;

public class ListDeclField extends TreeList<AbstractDeclField> {

    @Override
    public void decompile(IndentPrintStream s) {
        String prefix = "";
        for (AbstractDeclField var : this.getList()) {
            s.print(prefix);
            var.decompile(s);
            prefix = ", ";
        }
    }
}
