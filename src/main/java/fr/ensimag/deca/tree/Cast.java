package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.AbstractCastGen;
import fr.ensimag.deca.codegen.StaticCast;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;

/**
 * Type conversion operator
 */
public class Cast extends AbstractUnaryExpr {
    private final AbstractIdentifier typeIdent;
    private Type            srcType  = null;
    private Type            destType = null;
    private AbstractCastGen castGen  = null;

    public Cast(AbstractIdentifier typeIdent, AbstractExpr expr) {
        super(expr);
        this.typeIdent = typeIdent;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
                           ClassDefinition currentClass)
            throws ContextualError {
        destType = typeIdent.verifyType(compiler);
        srcType = getOperand().verifyExpr(compiler, localEnv, currentClass);

        if (destType.sameType(srcType)) {
            compiler.emitWarning("useless cast to same type", getLocation());
            castGen = new StaticCast();
        } else {
            castGen = srcType.getCastGen(destType);
        }

        if (castGen == null) {
            throw new ContextualError(
                    "can't cast " + srcType + " to " + destType,
                    this.getLocation());
        }

        setType(destType);
        return destType;
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler, NonScratchManager mgr) {
        assert castGen != null;
        getOperand().codeGenInst(compiler, mgr);
        castGen.codeGen(compiler, mgr);
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("(");
        typeIdent.decompile(s);
        s.print(")(");
        getOperand().decompile(s);
        s.print(")");
    }

    @Override
    protected String getOperatorName() {
        return "/* cast */";
    }

    @Override
    String prettyPrintNode() {
        return "Cast (" + srcType + " to " + destType + ")";
    }
}
