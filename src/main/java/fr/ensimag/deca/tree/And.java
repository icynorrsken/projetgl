package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.Label;

/**
 * @author @AUTHOR@
 */
public class And extends AbstractOpBool {

    public And(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected String getOperatorName() {
        return "&&";
    }

    @Override
    public void codeGenCond(DecacCompiler compiler, NonScratchManager mgr,
                            boolean branchIfTrue, Label nextLabel) {
        if (branchIfTrue) {
            Label l = compiler.getLabelManager().adhoc("and.rhs");
            this.getLeftOperand().codeGenCond(compiler, mgr, false, l);
            this.getRightOperand().codeGenCond(compiler, mgr, true, nextLabel);
            compiler.addLabel(l);
        } else {
            this.getLeftOperand().codeGenCond(compiler, mgr, false, nextLabel);
            this.getRightOperand().codeGenCond(compiler, mgr, false, nextLabel);
        }
    }
}
