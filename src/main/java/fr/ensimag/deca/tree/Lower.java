package fr.ensimag.deca.tree;

import fr.ensimag.ima.pseudocode.Instruction;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BGE;
import fr.ensimag.ima.pseudocode.instructions.BLT;

/**
 * @author @AUTHOR@
 */
public class Lower extends AbstractOpIneq {

    public Lower(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected String getOperatorName() {
        return "<";
    }

    @Override
    protected Instruction getBranchInstruction(boolean branchIfTrue,
                                               Label nextLabel) {
        if (branchIfTrue) {
            return new BLT(nextLabel);
        } else {
            return new BGE(nextLabel);
        }
    }
}
