package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.instructions.FLOAT;

/**
 * Conversion of an int into a float. Used for implicit conversions.
 *
 * @author @AUTHOR@
 */
public class ConvFloat extends AbstractUnaryExpr {
    public ConvFloat(AbstractExpr operand) {
        super(operand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
                           ClassDefinition currentClass) {
        Type operandT = getOperand().getType();
        if (operandT == null || !operandT.isInt()) {
            throw new UnsupportedOperationException("compiler bug: " +
                                                    "ConvFloat can only be " +
                                                    "applied to an " +
                                                    "already-verified " +
                                                    "integer expression");
        }

        setType(compiler.floatType);
        return compiler.floatType;
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler, NonScratchManager mgr) {
        GPRegister destReg = mgr.getReg();
        getOperand().codeGenInst(compiler, mgr);
        compiler.addInstruction(new FLOAT(destReg, destReg));
    }

    @Override
    public void decompile(IndentPrintStream s) {
        getOperand().decompile(s);
    }

    @Override
    protected String getOperatorName() {
        return "/* conv float */";
    }
}
