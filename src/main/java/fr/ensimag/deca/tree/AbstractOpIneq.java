package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;

/**
 * @author @AUTHOR@
 */
public abstract class AbstractOpIneq extends AbstractOpCmp {

    public AbstractOpIneq(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected void verifyCmp(DecacCompiler compiler, EnvironmentExp localEnv,
                             ClassDefinition currentClass)
            throws ContextualError {
        if (!this.getLeftOperand().getType().isArithOp()) {
            throw new ContextualError(
                    "Arguments of an inequality must be arithmetic",
                    getLocation());
        }
    }
}
