package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.ima.pseudocode.DAddr;

/**
 * Class declaration.
 *
 * @author @AUTHOR@
 */
public abstract class AbstractDeclClass extends Tree {

    public abstract void codeGenMethodsTable(DecacCompiler compiler);

    /**
     * Attribute grammar's pass 1. Verify that the class declaration is OK
     * without looking at its content.
     */
    protected abstract void verifyClass(DecacCompiler compiler)
            throws ContextualError;

    /**
     * Attribute grammar's pass 2. Verify that the class members (fields and
     * methods) are OK, without looking at method body and field
     * initialization.
     */
    protected abstract void verifyClassMembers(DecacCompiler compiler)
            throws ContextualError;

    /**
     * Attribute grammar's pass 3. Verify that instructions and expressions
     * contained in the class are OK.
     */
    protected abstract void verifyClassBody(DecacCompiler compiler)
            throws ContextualError;

    public abstract void generateInitMethod(DecacCompiler compiler);

    public abstract void generateMethods(DecacCompiler compiler);

    public abstract int getNumberOfMethodsExcludingInherited();

    public abstract int getTotalNumberOfMethods();

    public abstract void setClassDAddr(DAddr daddr);

    public abstract void allocateMethodsTable(int indexFromGb);
}
