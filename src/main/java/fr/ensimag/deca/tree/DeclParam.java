package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.ParamDefinition;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;

import java.io.PrintStream;

public class DeclParam extends AbstractDeclParam {

    // Step A
    private AbstractIdentifier typeName;
    private AbstractIdentifier paramName;

    // Step B
    private Type type;

    public DeclParam(AbstractIdentifier typeName,
                     AbstractIdentifier paramName) {
        this.typeName = typeName;
        this.paramName = paramName;
    }

    public AbstractIdentifier getParamName() {
        return paramName;
    }

    public Type getType() {
        assert null != type;
        return type;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        typeName.decompile(s);
        s.print(" ");
        paramName.decompile(s);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        typeName.prettyPrint(s, prefix, false);
        paramName.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        typeName.iter(f);
        paramName.iter(f);
    }

    public Type verifyFormalParam(DecacCompiler compiler)
            throws ContextualError {
        assert null == type; // don't verify twice
        type = typeName.verifyType(compiler);
        if (type.isVoid()) {
            throw new ContextualError("Formal method parameters can't be void",
                                      typeName.getLocation());
        }
        paramName.setDefinition(new ParamDefinition(type, getLocation()));
        return type;
    }
}
