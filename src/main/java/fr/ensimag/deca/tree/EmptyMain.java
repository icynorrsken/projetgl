package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.MethodEnvExp;

import java.io.PrintStream;

/**
 * Empty main Deca program
 *
 * @author @AUTHOR@
 */
public class EmptyMain extends AbstractBody {
    @Override
    protected void verifyBody(DecacCompiler compiler, MethodEnvExp localEnv,
                              ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        // nothing can go wrong
    }

    @Override
    protected void codeGen(DecacCompiler compiler, DeclMethod method) {
        // nothing to do
    }

    /**
     * Contains no real information => nothing to check.
     */
    @Override
    protected void checkLocation() {
        // nothing
    }

    @Override
    public void decompile(IndentPrintStream s) {
        // no main program => nothing
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }
}
