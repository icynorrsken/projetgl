package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.MethodEnvExp;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.instructions.*;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

import java.io.PrintStream;

/**
 * @author @AUTHOR@
 */
public class Body extends AbstractBody {
    private static final Logger LOG = Logger.getLogger(Body.class);

    private ListDeclVarSet declVariables;
    private ListInst       insts;

    public Body(ListDeclVarSet declVariables, ListInst insts) {
        Validate.notNull(declVariables);
        Validate.notNull(insts);
        this.declVariables = declVariables;
        this.insts = insts;
    }

    @Override
    protected void verifyBody(DecacCompiler compiler, MethodEnvExp localEnv,
                              ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        for (AbstractDeclVarSet set : declVariables.getList()) {
            set.verifyDeclVarSet(compiler, localEnv, currentClass);
        }

        for (AbstractInst inst : insts.getList()) {
            inst.verifyInst(compiler, localEnv, currentClass, returnType);
        }
    }

    @Override
    protected void codeGen(DecacCompiler compiler, DeclMethod method) {
        compiler.getLabelManager().setAdhocPrefix(method);

        compiler.enableTSTOMonitoring();

        NonScratchManager mgr = NonScratchManager.newManagerR2(compiler);

        // Save position before we add any instructions
        // method body may NOT insert any instructions before this index
        int bodyStart = compiler.getNbLines();

        // ADDSP
        if (!declVariables.isEmpty()) {
            compiler.addInstruction(
                    new ADDSP(declVariables.getTotalVariableCount()),
                    "prevent non-scratch PUSHes from overwriting local vars");
        }

        int regPushInsertionPoint = compiler.getNbLines();
        declVariables.codeGenListDeclVarSet(compiler, mgr);

        compiler.addComment("instructions below");
        insts.codeGenListInst(compiler, mgr);

        if (method != null && !method.getReturnTypeName().getType().isVoid()) {
            // Check if the method had effectively returned something
            compiler.addInstruction(new WSTR(
                    "Method " + method.getMethodName().getName() +
                    " ended without RETURN instruction"));

            compiler.addInstruction(new BRA(compiler.getLabelManager().death));

            // If so, just end the execution
            compiler.addLabel(compiler.getLabelManager().getReturn(method));
        }

        mgr.codeGenRestoreRegs(compiler, regPushInsertionPoint);

        // SUBSP
        if (!declVariables.isEmpty()) {
            compiler.addInstruction(
                    new SUBSP(declVariables.getTotalVariableCount()),
                    "local variable declarations");
        }

        if (method != null) {
            compiler.addInstruction(new RTS());
        }

        compiler.insertTSTO();

        compiler.getLabelManager().clearAdhocPrefix();
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.println("{");
        s.indent();
        declVariables.decompile(s);
        insts.decompile(s);
        s.unindent();
        s.println("}");
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        declVariables.iter(f);
        insts.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        declVariables.prettyPrint(s, prefix, false);
        insts.prettyPrint(s, prefix, true);
    }
}
