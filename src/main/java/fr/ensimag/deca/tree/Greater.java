package fr.ensimag.deca.tree;

import fr.ensimag.ima.pseudocode.Instruction;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BGT;
import fr.ensimag.ima.pseudocode.instructions.BLE;

/**
 * @author @AUTHOR@
 */
public class Greater extends AbstractOpIneq {

    public Greater(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected String getOperatorName() {
        return ">";
    }

    @Override
    protected Instruction getBranchInstruction(boolean branchIfTrue,
                                               Label nextLabel) {
        if (branchIfTrue) {
            return new BGT(nextLabel);
        } else {
            return new BLE(nextLabel);
        }
    }
}
