package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.STORE;

/**
 * Assignment, i.e. lvalue = expr.
 *
 * @author @AUTHOR@
 */
public class Assign extends AbstractBinaryExpr {

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("(");
        getLeftOperand().decompile(s);
        s.print(" " + getOperatorName() + " ");
        getRightOperand().decompile(s);
        s.print(")");
    }

    @Override
    public AbstractLValue getLeftOperand() {
        // The cast succeeds by construction, as the leftOperand has been set
        // as an AbstractLValue by the constructor.
        return (AbstractLValue) super.getLeftOperand();
    }

    public Assign(AbstractLValue leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    /**
     * Performs contextual verification of rhs. If lhsType is float and rhs's
     * type is int, returns a ConvFloat wrapping rhs. Otherwise, checks that
     * both types are identical. Always replace your RHS by the one returned by
     * this method.
     */
    public static AbstractExpr compatibleRHS(DecacCompiler compiler,
                                             EnvironmentExp localEnv,
                                             ClassDefinition currentClass,
                                             Type lhsType, AbstractExpr rhs)
            throws ContextualError {
        assert (rhs.getType() == null); // make sure RHS is still unchecked

        Type rhsType = rhs.verifyExpr(compiler, localEnv, currentClass);

        if (lhsType.isFloat() && rhsType.isInt()) {
            ConvFloat conv = new ConvFloat(rhs);
            rhsType = conv.verifyExpr(compiler, localEnv, currentClass);
            assert (lhsType.sameType(rhsType));
            conv.setLocation(rhs.getLocation());
            rhs = conv;
        }

        if (!lhsType.assignCompatible(rhsType)) {
            throw new ContextualError(
                    "Type " + rhsType + " is not assignable to type " +
                    lhsType + " (rule 3.33)", rhs.getLocation());
        }

        return rhs;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
                           ClassDefinition currentClass)
            throws ContextualError {
        if (getLeftOperand().isThis()) {
            // TODO : is there a rule in the spec for that ?
            // I know it's impossible in Java (because "this" is final)
            throw new ContextualError("Cannot assign a value to field \"this\"",
                                      getLocation());
        }

        Type lhsType =
                getLeftOperand().verifyExpr(compiler, localEnv, currentClass);

        if (getLeftOperand().getDefinition().isMethod()) {
            throw new ContextualError(
                    "Cannot assign a value to a method (rules 3.72, 3.73, 3" +
                    ".74)",
                    getLocation());
        }

        setRightOperand(compatibleRHS(compiler, localEnv, currentClass, lhsType,
                                      getRightOperand()));

        setType(lhsType);
        return lhsType;
    }

    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
                              ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        verifyExpr(compiler, localEnv, currentClass);
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler, NonScratchManager mgr) {
        GPRegister valueReg = mgr.getReg();
        NonScratchManager addrMgr = mgr.next();

        getRightOperand().codeGenInst(compiler, mgr);
        DAddr lhsAddr = getLeftOperand().codeGenAddress(compiler, addrMgr);
        compiler.addInstruction(new STORE(valueReg, lhsAddr));
    }

    @Override
    protected String getOperatorName() {
        return "=";
    }

    @Override
    public void codeGenCond(DecacCompiler compiler, NonScratchManager mgr,
                            boolean branchIfTrue, Label nextLabel) {
        getRightOperand().codeGenCond(compiler, mgr, branchIfTrue, nextLabel);
    }
}
