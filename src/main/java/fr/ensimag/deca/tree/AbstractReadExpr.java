package fr.ensimag.deca.tree;

/**
 * read...() statement.
 *
 * @author @AUTHOR@
 */
public abstract class AbstractReadExpr extends AbstractExpr {

    public AbstractReadExpr() {
        super();
    }
}
