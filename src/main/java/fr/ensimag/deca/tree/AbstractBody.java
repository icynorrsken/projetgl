package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.MethodEnvExp;

/**
 * Main block of a Deca program.
 *
 * @author @AUTHOR@
 */
public abstract class AbstractBody extends Tree {

    protected abstract void codeGen(DecacCompiler compiler, DeclMethod method);

    protected abstract void verifyBody(DecacCompiler compiler,
                                       MethodEnvExp localEnv,
                                       ClassDefinition currentClass,
                                       Type returnType) throws ContextualError;
}
