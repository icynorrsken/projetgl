package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.instructions.STORE;
import org.apache.commons.lang.Validate;

import java.io.PrintStream;

/**
 * @author @AUTHOR@
 */
public class DeclVar extends AbstractDeclVar {

    public AbstractIdentifier getVarName() {
        return varName;
    }

    public AbstractInitialization getInitialization() {
        return initialization;
    }

    private AbstractIdentifier     varName;
    private AbstractInitialization initialization;

    public DeclVar(AbstractIdentifier varName,
                   AbstractInitialization initialization) {
        Validate.notNull(varName);
        Validate.notNull(initialization);
        this.varName = varName;
        this.initialization = initialization;
    }

    /**
     * @param offset : Shift from GB to store the global variable
     */
    @Override
    protected void verifyDeclVar(Type t, DecacCompiler compiler,
                                 EnvironmentExp localEnv,
                                 ClassDefinition currentClass)
            throws ContextualError {
        assert (!t.isVoid()); // must've been checked by DeclVarSet

        VariableDefinition def =
                new VariableDefinition(t, varName.getLocation());

        // Ne pas bouger ce bloc en le mettant après le try
        initialization
                .verifyInitialization(compiler, t, localEnv, currentClass);
        // Il sert à éviter de passer int x = x;

        try {
            localEnv.declare(varName.getName(), def);
        } catch (EnvironmentExp.DoubleDefException ex) {
            throw new ContextualError(
                    "symbol \"" + varName + "\" already declared " +
                    "(rule 3.21)", varName.getLocation());
        }

        varName.setDefinition(def);
    }

    @Override
    public void decompile(IndentPrintStream s) {
        varName.decompile(s);
        initialization.decompile(s);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        varName.iter(f);
        initialization.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        varName.prettyPrint(s, prefix, false);
        initialization.prettyPrint(s, prefix, true);
    }

    @Override
    public void codeGenDeclVar(DecacCompiler compiler, NonScratchManager mgr) {
        VariableDefinition var = varName.getVariableDefinition();
        initialization.codeGen(compiler, mgr);
        compiler.addInstruction(new STORE(mgr.getReg(), var.getDAddr()));
    }
}
