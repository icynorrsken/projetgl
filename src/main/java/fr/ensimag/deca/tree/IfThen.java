package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.Label;
import org.apache.commons.lang.Validate;

import java.io.PrintStream;

/**
 * @author @AUTHOR@
 */
public class IfThen extends AbstractIfThen {

    public AbstractExpr getCondition() {
        return condition;
    }

    public ListInst getInstructions() {
        return instructions;
    }

    public IfThen(AbstractExpr condition, ListInst instructions) {
        Validate.notNull(condition);
        Validate.notNull(instructions);
        this.condition = condition;
        this.instructions = instructions;
    }

    private AbstractExpr condition;
    private ListInst     instructions;

    @Override
    protected void verifyIfThen(DecacCompiler compiler, EnvironmentExp localEnv,
                                ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        this.condition.verifyCondition(compiler, localEnv, currentClass);
        this.instructions
                .verifyListInst(compiler, localEnv, currentClass, returnType);
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("(");
        condition.decompile(s);
        s.print(")");
        s.println(" {");
        s.indent();
        instructions.decompile(s);
        s.unindent();
        s.print("}");
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        condition.iter(f);
        instructions.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        condition.prettyPrint(s, prefix, false);
        instructions.prettyPrint(s, prefix, true);
    }

    @Override
    void codeGenInst(DecacCompiler compiler, NonScratchManager mgr,
                     Label nextLabel) {
        condition.codeGenCond(compiler, mgr, false, nextLabel);
        for (AbstractInst i : instructions.getList()) {
            i.codeGenInst(compiler, mgr);
        }
    }
}
