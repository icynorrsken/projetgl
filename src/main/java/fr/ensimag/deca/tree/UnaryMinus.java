package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.instructions.OPP;

/**
 * @author @AUTHOR@
 */
public class UnaryMinus extends AbstractUnaryExpr {

    public UnaryMinus(AbstractExpr operand) {
        super(operand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
                           ClassDefinition currentClass)
            throws ContextualError {
        Type expr_type =
                this.getOperand().verifyExpr(compiler, localEnv, currentClass);
        if (!expr_type.isArithOp()) {
            throw new ContextualError(
                    "Cannot take the opposite of " + expr_type.getName(),
                    this.getLocation());
        }

        this.setType(expr_type);
        return expr_type;
    }

    @Override
    protected String getOperatorName() {
        return "-";
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler, NonScratchManager mgr) {
        this.getOperand().codeGenInst(compiler, mgr);
        compiler.addInstruction(new OPP(mgr.getReg(), mgr.getReg()));
    }
}
