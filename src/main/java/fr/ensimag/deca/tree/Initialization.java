package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;
import org.apache.commons.lang.Validate;

import java.io.PrintStream;

/**
 * @author @AUTHOR@
 */
public class Initialization extends AbstractInitialization {

    public AbstractExpr getExpression() {
        return expression;
    }

    private AbstractExpr expression;

    public void setExpression(AbstractExpr expression) {
        Validate.notNull(expression);
        this.expression = expression;
    }

    public Initialization(AbstractExpr expression) {
        Validate.notNull(expression);
        this.expression = expression;
    }

    @Override
    protected void verifyInitialization(DecacCompiler compiler, Type t,
                                        EnvironmentExp localEnv,
                                        ClassDefinition currentClass)
            throws ContextualError {
        setExpression(Assign.compatibleRHS(compiler, localEnv, currentClass, t,
                                           expression));
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print(" = ");
        expression.decompile(s);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        expression.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        expression.prettyPrint(s, prefix, true);
    }

	/*
    public void codeGen(DecacCompiler compiler, NonScratchManager mgr, Type
	type, DAddr daddr) {
		expression.codeGenInst(compiler, mgr);
		compiler.addInstruction(new STORE(mgr.getReg(), daddr));
	}

    @Override
    public void codeGenToGeneralRegister(DecacCompiler compiler,
    NonScratchManager mgr, Type type, GPRegister register) {
        expression.codeGenInst(compiler, mgr);
    }
    */

    @Override
    public void codeGen(DecacCompiler compiler, NonScratchManager mgr) {
        expression.codeGenInst(compiler, mgr);
    }
}
