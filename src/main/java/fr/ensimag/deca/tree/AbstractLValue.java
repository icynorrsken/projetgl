package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.Definition;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

/**
 * Left-hand side value of an assignment.
 *
 * @author @AUTHOR@
 */
public abstract class AbstractLValue extends AbstractExpr {

    /**
     * Returns a DAddr suitable for loading or storing this LValue. May generate
     * code to retrieve the address, using dest as a temporary register.
     */
    public abstract DAddr codeGenAddress(DecacCompiler compiler,
                                         NonScratchManager mgr);

    @Override
    protected void codeGenInst(DecacCompiler compiler, NonScratchManager mgr) {
        DAddr addr = codeGenAddress(compiler, mgr);
        compiler.addInstruction(new LOAD(addr, mgr.getReg()));
    }

    public abstract Definition getDefinition();

    public boolean isThis() {
        return false;
    }
}
