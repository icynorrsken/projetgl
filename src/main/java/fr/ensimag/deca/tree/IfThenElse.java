package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.LabelManager;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import org.apache.commons.lang.Validate;

import java.io.PrintStream;
import java.util.Iterator;

/**
 * Full if/else if/else statement.
 *
 * @author @AUTHOR@
 */
public class IfThenElse extends AbstractInst {
    private ListIfThen ifThen;
    private ListInst   elseBranch;

    public IfThenElse(ListIfThen ifThen, ListInst elseBranch) {
        Validate.notNull(ifThen);
        Validate.notNull(elseBranch);
        this.ifThen = ifThen;
        this.elseBranch = elseBranch;
    }

    public ListIfThen getIfThen() {
        return ifThen;
    }

    public ListInst getElseBranch() {
        return elseBranch;
    }

    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
                              ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        for (AbstractIfThen i : ifThen.getList()) {
            i.verifyIfThen(compiler, localEnv, currentClass, returnType);
        }
        elseBranch.verifyListInst(compiler, localEnv, currentClass, returnType);
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler, NonScratchManager mgr) {
        LabelManager lm = compiler.getLabelManager();

        Label finalLabel = lm.adhoc("endif");
        Label nextLabel = null;

        AbstractIfThen i;
        Iterator<AbstractIfThen> ifThenIter = ifThen.iterator();
        for (int j = 0; ifThenIter.hasNext(); j++) {
            i = ifThenIter.next();

            if (j != 0) {
                compiler.addLabel(nextLabel);
            }

            if (ifThenIter.hasNext()) {
                nextLabel = lm.adhoc("elif");
            } else if (!elseBranch.isEmpty()) {
                nextLabel = lm.adhoc("else");
            } else {
                nextLabel = finalLabel;
            }

            i.codeGenInst(compiler, mgr, nextLabel);
            compiler.addInstruction(new BRA(finalLabel));
        }

        if (!elseBranch.isEmpty()) {
            compiler.addLabel(nextLabel);
            elseBranch.codeGenListInst(compiler, mgr);
        }

        compiler.addLabel(finalLabel);
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("if ");
        Iterator<AbstractIfThen> ifThenIter = ifThen.iterator();
        IfThen i;
        while (ifThenIter.hasNext()) {
            i = (IfThen) ifThenIter.next();
            i.decompile(s);
            if (ifThenIter.hasNext()) {
                s.print(" else if ");
            }
        }
        if (!elseBranch.isEmpty()) {
            s.println(" else {");
            s.indent();
            elseBranch.decompile(s);
            s.unindent();
            s.println("}");
        }
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        ifThen.iter(f);
        elseBranch.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        ifThen.prettyPrint(s, prefix, false);
        elseBranch.prettyPrint(s, prefix, true);
    }
}
