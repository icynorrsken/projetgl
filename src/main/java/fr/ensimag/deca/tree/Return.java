package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.MethodEnvExp;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

import java.io.PrintStream;

public class Return extends AbstractInst {

    // This attribute is used to generate
    // the "return" label at the generation
    // time.
    // They are initialized at verification (in
    // verifyInst).
    private MethodDefinition inMethod;

    private AbstractExpr operand;

    public Return(AbstractExpr operand) {
        this.operand = operand;
    }

    public Return(AbstractExpr operand, MethodDefinition inMethod) {
        // Used for Object.equals
        this(operand);
        this.inMethod = inMethod;
    }

    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
                              ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        // This case only occurs when "return" is found in the main function
        if (currentClass == null) {
            throw new ContextualError(
                    "Unexpected return instruction in main function (rule 3" +
                    ".28)",
                    getLocation());
        }

        try {
            inMethod = ((MethodEnvExp) localEnv).getMethodDefinition();
        } catch (ClassCastException exception) {
            throw new DecacInternalError(
                    "localEnv is supposed to be a MethodEnvExp!");
        }

        Type methodRT = inMethod.getType();

        if (methodRT.isVoid()) {
            throw new ContextualError(
                    "Return statement forbidden in void methods (rule 3.28) ",
                    getLocation());
        }

        operand =
                Assign.compatibleRHS(compiler, localEnv, currentClass, methodRT,
                                     operand);
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler, NonScratchManager mgr) {
        // Return value must be placed in R0
        this.operand.codeGenInst(compiler, mgr);
        compiler.addInstruction(new LOAD(mgr.getReg(), Register.R0));
        compiler.addInstruction(new BRA(compiler.getLabelManager().getReturn(
                inMethod.getDecl())));
        // To check, the method must call RTS (like that)
        // after poping Rx (x > 1)
        // compiler.addInstruction(new RTS());
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("return " + this.operand.decompile() + ";");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        operand.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        operand.iter(f);
    }
}
