/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.*;

/**
 * @author barbjero
 */
public class NewInstance extends AbstractUnaryExpr {

    public NewInstance(AbstractExpr operand) {
        super(operand);
    }

    @Override
    protected String getOperatorName() {
        return "new";
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print(getOperatorName() + " ");
        this.getOperand().decompile(s);
        s.print("()");
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
                           ClassDefinition currentClass)
            throws ContextualError {
        Type typeDef = ((Identifier) this.getOperand()).verifyType(compiler);

        assert typeDef != null;

        if (!typeDef.isClass()) {
            throw new ContextualError("Cannot instanciate a variable with a " +
                                      "predefined type (rule 3.47)",
                                      this.getLocation());
        }

        this.setType(typeDef);

        return typeDef;
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler, NonScratchManager mgr) {
        GPRegister dest = mgr.getReg();

        ClassDefinition classDef = ((ClassType) this.getType()).getDefinition();
        // NEW size, Rdest
        compiler.addInstruction(
                new NEW(classDef.getNumberOfFields() + 1, dest));
        // + 1 for the ref to te type !

        // If we have to test the stack overflow
        if (!compiler.getCompilerOptions().getNoCheck()) {
            // BOV label
            compiler.addInstruction(
                    new BOV(compiler.getLabelManager().heapFull));
        }

        // ==== Store pointer to method table in heap ====

        // LEA Addr_Class, R0
        compiler.addInstruction(new LEA(classDef.getDAddr(), Register.R0));
        // STORE R0, 0(dest)
        compiler.addInstruction(
                new STORE(GPRegister.R0, new RegisterOffset(0, dest)));

        // ==== Call initialization method ====

        // push implicit 'this' parameter (object address)
        compiler.addInstruction(new PUSH(dest));

        // BSR init
        compiler.addInstruction(new BSR(compiler.getLabelManager().getInit(
                (AbstractIdentifier) getOperand())));

        // ==== Clean up ====

        // pop object address
        compiler.addInstruction(new POP(dest));

        // Final store (if any) is managed by Assign
    }

    @Override
    public DAddr codeGenAddress(DecacCompiler compiler, NonScratchManager mgr) {
        codeGenInst(compiler, mgr);
        return new RegisterOffset(0, mgr.getReg());
    }
}
