package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Type;

public abstract class AbstractDeclParam extends Tree {
    public abstract AbstractIdentifier getParamName();

    public abstract Type getType();

    public abstract Type verifyFormalParam(DecacCompiler compiler)
            throws ContextualError;
}
