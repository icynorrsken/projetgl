package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ClassType;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.MethodDefinition;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.LabelManager;
import fr.ensimag.deca.tools.MethodEnvExp;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.*;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

import java.io.PrintStream;

/**
 * Deca complete program (class definition plus main block)
 *
 * @author @AUTHOR@
 */
public class Program extends AbstractProgram {
    private static final Logger LOG = Logger.getLogger(Program.class);

    public Program(ListDeclClass classes, AbstractBody main) {
        Validate.notNull(classes);
        Validate.notNull(main);
        this.classes = classes;
        this.main = main;
        // TODO 3????
        this.methodTableSize = 3;
    }

    public ListDeclClass getClasses() {
        return classes;
    }

    public AbstractBody getMain() {
        return main;
    }

    private ListDeclClass classes;
    private AbstractBody  main;
    private int           methodTableSize;

    @Override
    public void verifyProgram(DecacCompiler compiler) throws ContextualError {
        LOG.debug("verify program: start");

        this.addObjectDefinition(compiler); // Added here to prevent the
        // compiler to reject this special class

        // Check class listing
        classes.verifyListClass(compiler);
        classes.verifyListClassMembers(compiler);
        classes.verifyListClassBody(compiler);

        this.classes.allocateMethodsTable(); //methods table generation
        
        /* Handling te global vars */
        DeclClass lastClass = (DeclClass) this.classes.getLastClass();
        assert lastClass != null;
        methodTableSize =
                ((RegisterOffset) lastClass.getClassName().getDefinition()
                        .getDAddr()).getOffset() +
                lastClass.getTotalNumberOfMethods();

        // On pourra se baser sur LB de la même manière que pour les
        // méthodes, qui pour le main sera
        // juste au dessus de la table des méthodes
        main.verifyBody(compiler, new MethodEnvExp(null, new RegisterOffset(
                1 + methodTableSize, Register.GB), null), null,
                        compiler.voidType);

        LOG.debug("verify program: end");
    }

    @Override
    public void codeGenProgram(DecacCompiler compiler) {
        LabelManager lm = compiler.getLabelManager();

        // A FAIRE: compléter ce squelette très rudimentaire de code
        compiler.addComment("------------------------------");
        compiler.addComment("-- Methods Table generation --");
        compiler.addComment("------------------------------");
        LOG.debug("Methods Table generation");

        if (!compiler.getCompilerOptions().getNoCheck()) {
            // Adds TSTO method_table_size
            compiler.addComment("Test if we can reserve " + methodTableSize +
                                " bytes in stack");
            compiler.addInstruction(new TSTO(methodTableSize));
            compiler.addInstruction(new BOV(lm.stackOverflow));
        }

        this.classes.codeGenMethodsTable(compiler); //methods table generation
        compiler.addInstruction(new ADDSP(methodTableSize));

        compiler.addComment("------------------");
        compiler.addComment("-- Main program --");
        compiler.addComment("------------------");
        LOG.debug("Main program");
        this.main.codeGen(compiler, null);

        compiler.addInstruction(new HALT());

        compiler.addComment("-----------------------");
        compiler.addComment("-- Class definitions --");
        compiler.addComment("-----------------------");
        LOG.debug("Class definitions");
        this.classes.codeGenClasses(compiler);

        compiler.addComment("--------------------");
        compiler.addComment("-- Error handlers --");
        compiler.addComment("--------------------");
        this.addErrorHandler(compiler, lm.readIntError,
                             "Unrecognized input, input is not an integer");
        this.addErrorHandler(compiler, lm.readFloatError,
                             "Unrecognized input, input is not a float");

        if (!compiler.getCompilerOptions()
                .getNoCheck()) {            //if not arg -n
            this.addErrorHandler(compiler, lm.arithError,
                                 "Arithmetic error: division by 0, value " +
                                 "higher than supported");
            this.addErrorHandler(compiler, lm.stackOverflow, "Stack overflow");
            this.addErrorHandler(compiler, lm.heapFull, "Heap full");
            this.addErrorHandler(compiler, lm.nullDeref,
                                 "Null pointer exception");
        }

        this.addErrorHandler(compiler, lm.dynCastError,
                             "Forbidden dynamic cast");

        compiler.addLabel(lm.death);
        compiler.addInstruction(new WNL());
        compiler.addInstruction(new ERROR());
    }

    /**
     * Add te definition of object "Object" to the classes
     *
     * @param compiler
     */
    protected void addObjectDefinition(DecacCompiler compiler) {
        // Object type for compiler
        compiler.objectType =
                new ClassType(compiler.getSymbol("Object"), Location.BUILTIN,
                              null);

        ClassDefinition objectDefinition =
                new ClassDefinition(compiler.objectType, Location.BUILTIN,
                                    null);

        compiler.addClassDefinition(objectDefinition);

        objectDefinition.hide(); // The compiler will not consider the first
        // double-declaration error

        // Generate class
        ListDeclParam ObjectEqualsParams = new ListDeclParam();
        ObjectEqualsParams.setLocation(Location.BUILTIN);

        ObjectEqualsParams.add(new DeclParam(new Identifier(
                                       compiler.objectType.getName(),
                                       Location.BUILTIN), new Identifier(
                                       compiler.getSymbol("otherObject"),
                                       Location.BUILTIN)));

        Identifier ObjectClassName =
                new Identifier(compiler.getSymbol("Object"), Location.BUILTIN);
        ObjectClassName.setDefinition(objectDefinition);

        Identifier ObjectSuperClass = null;

        // Mthos body
        ListInst ObjectBodyInstructions = new ListInst();
        ObjectBodyInstructions.setLocation(Location.BUILTIN);

        // return this == otherObject
        Equals eq = new Equals(
                new This(compiler.getSymbol("this"), Location.BUILTIN),
                new Identifier(compiler.getSymbol("otherObject"),
                               Location.BUILTIN));
        eq.setLocation(Location.BUILTIN);

        Return ret = new Return(eq);
        ret.setLocation(Location.BUILTIN);

        ObjectBodyInstructions.add(ret);

        Body ObjectBody =
                new Body(new ListDeclVarSet(), ObjectBodyInstructions);
        ObjectBody.setLocation(Location.BUILTIN);

        ListDeclMethod listDeclMethodsObject = new ListDeclMethod();
        listDeclMethodsObject.setLocation(Location.BUILTIN);

        Identifier ObjectEqualsMethodIdentifier =
                new Identifier(compiler.getSymbol("equals"), Location.BUILTIN);
        DeclMethod ObjectEqualsMethod = new DeclMethod(
                new Identifier(compiler.booleanType.getName(),
                               Location.BUILTIN), ObjectEqualsMethodIdentifier,
                ObjectEqualsParams, ObjectBody);

        ObjectEqualsMethodIdentifier.setDefinition(
                new MethodDefinition(compiler.booleanType, Location.BUILTIN,
                                     objectDefinition, null, methodTableSize,
                                     ObjectEqualsMethod));

        ObjectEqualsMethodIdentifier.getMethodDefinition().setLabel(
                compiler.getLabelManager().getMethodLabel(ObjectEqualsMethod));

        listDeclMethodsObject.add(ObjectEqualsMethod);

        // Fields, there is any
        ListDeclFieldSet ObjectDeclFieldSet = new ListDeclFieldSet();
        ObjectDeclFieldSet.setLocation(Location.BUILTIN);

        // Add virtual class to classes list
        this.classes.insert(0, new DeclClass(ObjectClassName, ObjectSuperClass,
                                             ObjectDeclFieldSet,
                                             listDeclMethodsObject));
    }

    @Override
    public void decompile(IndentPrintStream s) {
        getClasses().decompile(s);
        getMain().decompile(s);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        classes.iter(f);
        main.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        classes.prettyPrint(s, prefix, false);
        main.prettyPrint(s, prefix, true);
    }

    protected void addErrorHandler(DecacCompiler compiler, Label error_label,
                                   String message) {
        compiler.addLabel(error_label);
        compiler.addInstruction(new WSTR(message));
        compiler.addInstruction(new BRA(compiler.getLabelManager().death));
    }
}
