package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.BoolUtils;
import fr.ensimag.deca.tools.NonScratchManager;

/**
 * @author @AUTHOR@
 */
public abstract class AbstractOpBool extends AbstractBinaryExpr {

    public AbstractOpBool(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
                           ClassDefinition currentClass)
            throws ContextualError {
        Type leftType =
                getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
        Type rightType =
                getRightOperand().verifyExpr(compiler, localEnv, currentClass);
        if (!leftType.isBoolean() || !rightType.isBoolean()) {
            throw new ContextualError(
                    "Operators of a logic operation must be boolean",
                    this.getLocation());
        }
        this.setType(leftType);
        return leftType;
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler, NonScratchManager mgr) {
        BoolUtils.codeGenBool(compiler, mgr, this, false,
                              getClass().getSimpleName());
    }
}
