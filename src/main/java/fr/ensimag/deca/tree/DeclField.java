package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.STORE;

import java.io.PrintStream;

public class DeclField extends AbstractDeclField {

    private AbstractIdentifier     fieldName;
    private AbstractInitialization initialization;

    public DeclField(AbstractIdentifier fieldName,
                     AbstractInitialization initialization) {
        this.fieldName = fieldName;
        this.initialization = initialization;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        fieldName.decompile(s);
        initialization.decompile(s);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        fieldName.prettyPrint(s, prefix, false);
        initialization.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        fieldName.iter(f);
        initialization.iter(f);
    }

    @Override
    public FieldDefinition verifyDeclField(DecacCompiler compiler,
                                           EnvironmentExp localEnv,
                                           ClassDefinition currentClass, Type t,
                                           Visibility visibility)
            throws ContextualError {
        // Check du type de l'initialisation
        this.initialization
                .verifyInitialization(compiler, t, localEnv, currentClass);

        NonTypeDefinition fd = localEnv.get(fieldName.getName());

        if (fd != null) {
            if (!fd.isField()) {
                throw new ContextualError(
                        fieldName + " already declared as a method at " +
                        fd.getLocation().toString() + " (rule 2.7)",
                        getLocation());
            }

            FieldDefinition fdd = (FieldDefinition) fd;
            if (fdd.getContainingClass().equals(currentClass)) {
                throw new ContextualError("Field " + fieldName +
                                          " already exists in current class " +
                                          "(rule 2.7)",
                                          getLocation());
            }
        }

        FieldDefinition nfd =
                new FieldDefinition(t, getLocation(), visibility, currentClass,
                                    currentClass.incNumberOfFields());
        try {
            localEnv.declare(fieldName.getName(), nfd);
        } catch (EnvironmentExp.DoubleDefException e) {
            throw new DecacInternalError(
                    "compiler bug - we've already checked" +
                    "for this field name's uniqueness in this class");
        }
        this.fieldName.verifyExpr(compiler, localEnv, currentClass);
        return this.fieldName.getFieldDefinition();
    }

    @Override
    protected void generateFieldInit(DecacCompiler compiler,
                                     NonScratchManager mgr) {
        GPRegister dest = mgr.getReg();
        GPRegister tmp = mgr.next().getReg();

        // LOAD           the_value,           R0
        initialization.codeGen(compiler, mgr);

        // Load at the right position in the heap
        // LOAD           -2           (LB),           R1
        compiler.addInstruction(
                new LOAD(new RegisterOffset(-2, Register.LB), tmp));

        // STORE           R0,           index           (R1)
        // index(R1) :
        RegisterOffset position = new RegisterOffset(
                this.fieldName.getFieldDefinition().getIndex(), tmp);
        compiler.addInstruction(new STORE(dest, position));
    }
}
