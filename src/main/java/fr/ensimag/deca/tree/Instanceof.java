package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.*;

import java.io.PrintStream;

public class Instanceof extends AbstractExpr {

    private final AbstractExpr       objectExpr;
    private final AbstractIdentifier className;

    // synthesized in pass B
    private ClassType testedClass;

    public Instanceof(AbstractExpr objectExpr, AbstractIdentifier className) {
        this.objectExpr = objectExpr;
        this.className = className;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
                           ClassDefinition currentClass)
            throws ContextualError {
        Type objectType =
                objectExpr.verifyExpr(compiler, localEnv, currentClass);
        Type classGenericType = className.verifyType(compiler);

        if (!objectType.isClassOrNull()) {
            throw new ContextualError(
                    "instanceof left operand must be an object or null",
                    objectExpr.getLocation());
        }

        if (!classGenericType.isClass()) {
            throw new ContextualError(
                    "instanceof right operand must be a class name",
                    className.getLocation());
        }

        testedClass = (ClassType) classGenericType;

        setType(compiler.booleanType);
        return compiler.booleanType;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("(");
        objectExpr.decompile(s);
        s.print(" instanceof ");
        className.decompile(s);
        s.print(")");
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        objectExpr.iter(f);
        className.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        objectExpr.prettyPrint(s, prefix, false);
        className.prettyPrint(s, prefix, true);
    }

    @Override
    public void codeGenCond(DecacCompiler compiler, NonScratchManager mgr,
                            boolean branchIfTrue, Label nextLabel) {
        // retrieve method table entry for the object
        compiler.addComment(
                "instanceof: retrieve method table entry for object");
        objectExpr.codeGenInst(compiler, mgr);

        codeGenAncestryLookup(compiler, testedClass, mgr.getReg(), branchIfTrue,
                              nextLabel);
    }

    /**
     * @param reg contents will be destroyed
     */
    public static void codeGenAncestryLookup(DecacCompiler compiler,
                                             ClassType target, GPRegister reg,
                                             boolean branchIfTrue,
                                             Label nextLabel) {
        DAddr targetEntryAddr = target.getDefinition().getDAddr();

        Label retry = compiler.getLabelManager().adhoc("ancestry.loop");
        Label end = compiler.getLabelManager().adhoc("ancestry.end");
        Label success = branchIfTrue ? nextLabel : end;
        Label failure = branchIfTrue ? end : nextLabel;

        compiler.addInstruction(new CMP(new NullOperand(), reg));
        compiler.addInstruction(new BEQ(failure),
                                "null is an instance of nothing");

        compiler.addComment(
                "ancestry lookup: traverse method table until we find target " +
                "class");
        compiler.addInstruction(new LEA(targetEntryAddr, Register.R0));
        compiler.addLabel(retry);
        compiler.addInstruction(new CMP(Register.R0, reg),
                                "compare to targetEntryAddr");
        compiler.addInstruction(new BEQ(success), "ancestry lookup success");
        compiler.addInstruction(new LOAD(new RegisterOffset(0, reg), reg));
        compiler.addInstruction(new BEQ(failure), "ancestry lookup failure");
        compiler.addInstruction(new BRA(retry), "retry");
        compiler.addLabel(end);
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler, NonScratchManager mgr) {
        Label yes = compiler.getLabelManager().adhoc("instanceof.success");
        Label end = compiler.getLabelManager().adhoc("instanceof.end");

        codeGenCond(compiler, mgr, true, yes);
        compiler.addInstruction(new LOAD(0, mgr.getReg()));
        compiler.addInstruction(new BRA(end));
        compiler.addLabel(yes);
        compiler.addInstruction(new LOAD(1, mgr.getReg()));
        compiler.addLabel(end);
    }
}
