package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.BoolUtils;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.Label;

/**
 * @author @AUTHOR@
 */
public class Not extends AbstractUnaryExpr {

    public Not(AbstractExpr operand) {
        super(operand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
                           ClassDefinition currentClass)
            throws ContextualError {
        Type leftType =
                getOperand().verifyExpr(compiler, localEnv, currentClass);
        if (!leftType.isBoolean()) {
            throw new ContextualError("Operator of not must be boolean",
                                      getLocation());
        }
        this.setType(leftType);
        return leftType;
    }

    @Override
    public void codeGenCond(DecacCompiler compiler, NonScratchManager mgr,
                            boolean branchIfTrue, Label nextLabel) {
        getOperand().codeGenCond(compiler, mgr, !branchIfTrue, nextLabel);
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler, NonScratchManager mgr) {
        BoolUtils.codeGenBool(compiler, mgr, getOperand(), true, "not");
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("(");
        super.decompile(s);
        s.print(")");
    }

    @Override
    protected String getOperatorName() {
        return "!";
    }
}
