package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.deca.tools.SymbolTable.Symbol;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import org.apache.commons.lang.Validate;

import java.io.PrintStream;

/**
 * Deca Identifier
 *
 * @author @AUTHOR@
 */
public class Identifier extends AbstractIdentifier {

    @Override
    protected void checkDecoration() {
        if (getDefinition() == null) {
            throw new DecacInternalError("Identifier " + this.getName() +
                                         " has no attached Definition");
        }
    }

    @Override
    public Definition getDefinition() {
        return definition;
    }

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * ClassDefinition.
     * <p/>
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     *
     * @throws DecacInternalError if the definition is not a class definition.
     */
    @Override
    public ClassDefinition getClassDefinition() {
        try {
            return (ClassDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError("Identifier " + getName() +
                                         " is not a class identifier, you " +
                                         "can't call getClassDefinition on it");
        }
    }

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * MethodDefinition.
     * <p/>
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     *
     * @throws DecacInternalError if the definition is not a method definition.
     */
    @Override
    public MethodDefinition getMethodDefinition() {
        try {
            return (MethodDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError("Identifier " + getName() +
                                         " is not a method identifier, you " +
                                         "can't call getMethodDefinition on " +
                                         "it");
        }
    }

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * FieldDefinition.
     * <p/>
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     *
     * @throws DecacInternalError if the definition is not a field definition.
     */
    @Override
    public FieldDefinition getFieldDefinition() {
        try {
            return (FieldDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError("Identifier " + getName() +
                                         " is not a field identifier, you " +
                                         "can't call getFieldDefinition on it");
        }
    }

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * VariableDefinition.
     * <p/>
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     *
     * @throws DecacInternalError if the definition is not a field definition.
     */
    @Override
    public VariableDefinition getVariableDefinition() {
        try {
            return (VariableDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError("Identifier " + getName() +
                                         " is not a variable identifier, you " +
                                         "can't call getVariableDefinition on" +
                                         " it");
        }
    }

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * NonTypeDefinition.
     * <p/>
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     *
     * @throws DecacInternalError if the definition is not a field definition.
     */
    @Override
    public NonTypeDefinition getNonTypeDefinition() {
        try {
            return (NonTypeDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError("Identifier " + getName() +
                                         " is not a NonType identifier, you " +
                                         "can't call getNonTypeDefinition on " +
                                         "it");
        }
    }

    @Override
    public ParamDefinition getParamDefinition() {
        try {
            return (ParamDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError("Identifier " + getName() +
                                         " is not a parameter identifier, you" +
                                         " can't call getParamDefinition on " +
                                         "it");
        }
    }

    @Override
    public void setDefinition(Definition definition) {
        this.definition = definition;
    }

    @Override
    public Symbol getName() {
        return name;
    }

    private Symbol name;

    public Identifier(Symbol name) {
        Validate.notNull(name);
        this.name = name;
    }

    public Identifier(Symbol name, Location location) {
        this(name);
        setLocation(location);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
                           ClassDefinition currentClass)
            throws ContextualError {
        NonTypeDefinition ntd = localEnv.get(name);
        setDefinition(ntd);
        if (ntd == null) {
            throw new ContextualError(
                    "can't find symbol \"" + name + "\" in local " +
                    "environment and its ancestors (rule 0.1)", getLocation());
        }
        
        setType(ntd.getType());
        
        return ntd.getType();
    }

    @Override
    public Type verifyType(DecacCompiler compiler) throws ContextualError {
        TypeDefinition type = compiler.getTypeDefinition(this.name);
        setDefinition(type);
        if (type == null) {
            throw new ContextualError(
                    "undefined type \"" + name + "\" (rule 0.2)",
                    getLocation());
        }
        return type.getType();
    }

    @Override
    public DAddr codeGenAddress(DecacCompiler compiler, NonScratchManager mgr) {
        NonTypeDefinition definition = this.getNonTypeDefinition();
        DAddr addrToReturn = null;

        if (definition.isField()) {
            // Load this in R0
            compiler.addInstruction(
                    new LOAD(new RegisterOffset(-2, Register.LB), Register.R0));

            // Return index(R0)
            addrToReturn = new RegisterOffset(
                    ((FieldDefinition) definition).getIndex(), Register.R0);
        } else if (definition.isParam()) {
            addrToReturn = getParamDefinition().getDAddr();
        } else {
            addrToReturn = getVariableDefinition().getDAddr();
        }

        return addrToReturn;
    }

    private Definition definition;

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print(name.toString());
    }

    @Override
    String prettyPrintNode() {
        return "Identifier (" + getName() + ")";
    }

    @Override
    protected void prettyPrintType(PrintStream s, String prefix) {
        Definition d = getDefinition();
        if (d != null) {
            s.print(prefix);
            s.print("definition: ");
            s.print(d);
            s.println();
        }
    }

    @Override
    public String toString() {
        return name.toString();
    }
}
