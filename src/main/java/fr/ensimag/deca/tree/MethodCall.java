package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.*;

import java.io.PrintStream;

public class MethodCall extends AbstractExpr {
    private AbstractExpr       object;
    private AbstractIdentifier methodName;
    private ListExpr           params;
    private int                stackRequirement;

    public MethodCall(AbstractExpr object, AbstractIdentifier methodName,
                      ListExpr params) {
        this.object = object;
        this.methodName = methodName;
        this.params = params;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
                           ClassDefinition currentClass)
            throws ContextualError {
        stackRequirement = 1 + params.size();

        Type leftType = object.verifyExpr(compiler, localEnv, currentClass);

        /*
            A virtual "this" is added by parser to properly manage
            references in classes. This "this" will return voidType if
            it had been added virtually and if we're not inside a class
            (that's means there was nothig here before)
        */
        if (leftType.isVoid()) {
            // That's means we wrote somethid like "printf('blabla')"
            // The parser generated a MethodCall(this, printf)
            // BUT if we're not in a class, this "This" doesn't mean
            // anyhing (so void).
            // We're in the case of something like a function call,
            // that is not supported by Deca

            // This exception could be generated in This object but
            // the message in this case could not be easily understandable
            throw new ContextualError(
                    "Method " + this.methodName.getName() + " is not defined",
                    this.methodName.getLocation());
        }

        if (!leftType.isClass()) {
            throw new ContextualError(
                    "Left operand in method call must be class instance (rule" +
                    " 3.70)",
                    object.getLocation());
        }

        assert leftType instanceof ClassType;
        ClassType classType = (ClassType) leftType;

        Type methodReturnType = methodName
                .verifyExpr(compiler, classType.getDefinition().getMembers(),
                            classType.getDefinition());

        MethodDefinition methodDef;
        try {
            methodDef = methodName.getMethodDefinition();
        } catch (DecacInternalError ex) {
            throw new ContextualError("class member " + methodName +
                                      " is not a method in class " + classType +
                                      " (rule 3.77)", object.getLocation());
        }

        assert methodDef != null;
        params.verifyParams(compiler, localEnv, currentClass,
                            methodDef.getSignature(), getLocation());

        setType(methodReturnType);
        return methodReturnType;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        object.decompile(s);
        s.print(".");
        methodName.decompile(s);
        s.print("(");
        params.decompile(s);
        s.print(")");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        object.prettyPrint(s, prefix, false, false);
        methodName.prettyPrint(s, prefix, false, false);
        params.prettyPrint(s, prefix, true, false);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        object.iter(f);
        methodName.iter(f);
        params.iter(f);
    }
    
    public DAddr codeGenAddress(DecacCompiler compiler, NonScratchManager mgr) {
        //throw new UnsupportedOperationException(
         //       "can't take address of this kind of abstract expression");
    	DAddr leftOpAddr = object.codeGenAddress(compiler, mgr);

        RegisterOffset addrOff =
                new RegisterOffset(methodName.getMethodDefinition().getIndex(),
                                   mgr.getReg());

        //compiler.addInstruction(new LOAD(leftOpAddr, mgr.getReg()));
        //compiler.checkNullDeref(mgr.getReg());
        // compiler.addInstruction(new LOAD(new RegisterOffset(0, mgr.getReg()), mgr.getReg()));
        return addrOff;
    }
    
    @Override
    protected void codeGenInst(DecacCompiler compiler, NonScratchManager mgr) {
        // reserve stack space for parameters
        compiler.addInstruction(new ADDSP(stackRequirement),
                                "reserve stack space for parameters");

        RegisterOffset paramSP = new RegisterOffset(-1, Register.SP);

        // parameters
        for (AbstractExpr p : params.getList()) {
            p.codeGenInst(compiler, mgr);
            compiler.addInstruction(new STORE(mgr.getReg(), paramSP));
            paramSP = paramSP.below();
        }

        // load implicit param address in mgr.getReg()
        object.codeGenInst(compiler, mgr);

        // ==============================================================
        // From this point on, mgr.getReg() contains the object's address
        // ==============================================================

        compiler.checkNullDeref(mgr.getReg());

        // first implicit effective parameter (object address)
        compiler.addInstruction(
                new STORE(mgr.getReg(), new RegisterOffset(0, Register.SP)),
                "store 'this' as first effective parameter");

        // load table method base address
        compiler.addInstruction(
                new LOAD(new RegisterOffset(0, mgr.getReg()), mgr.getReg()));

        compiler.addInstruction(new BSR(new RegisterOffset(
                                        methodName.getMethodDefinition()
                                                .getContainingClass()
                                                .getMembers().getListOfMethods()
                                                .indexOf(methodName
                                                                 .getMethodDefinition()) +
                                        1, mgr.getReg())),
                                "call " + object.decompile() + "." +
                                methodName.decompile());

        compiler.addInstruction(new SUBSP(stackRequirement),
                                "release parameters");

        if (!this.methodName.getDefinition().getType()
                .equals(compiler.voidType)) {
            compiler.addInstruction(new LOAD(Register.R0, mgr.getReg()));
        }
    }
}
