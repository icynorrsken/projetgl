package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;

public class This extends Identifier {

    private boolean explicit;

    public This(SymbolTable.Symbol name) {
        super(name);
        this.setExplicit();
    }

    public This(SymbolTable.Symbol name, Location location) {
        super(name, location);
        this.setExplicit();
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
                           ClassDefinition currentClass)
            throws ContextualError {
        // User volontarily used "this" outside of a class
        if (this.isExplicit() && (currentClass == null)) {
            throw new ContextualError("\"This\" operator cannot be used " +
                                      "elsewhere than in a class method (rule" +
                                      " 3.48)",
                                      getLocation());
        }

        // User voluntarily used "this" inside a class
        // Or user doesn't used "this" but is in a class however
        if (currentClass != null) {
            this.setType(currentClass.getType());
            this.setDefinition(
                    new FieldDefinition(this.getType(), this.getLocation(),
                                        Visibility.PROTECTED, currentClass, 0));

            return this.getType();
        }

        return compiler.voidType;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("this");
    }

    @Override
    public Type verifyType(DecacCompiler compiler) throws ContextualError {
        return this.getDefinition().getType();
    }

    @Override
    public boolean isThis() {
        return true;
    }

    public boolean isExplicit() {
        return this.explicit;
    }

    public void setExplicit() {
        this.explicit = true;
    }

    public void setNonExplicit() {
        this.explicit = false;
    }

    @Override
    public DAddr codeGenAddress(DecacCompiler compiler, NonScratchManager mgr) {
        return new RegisterOffset(-2, Register.LB);
    }
}
