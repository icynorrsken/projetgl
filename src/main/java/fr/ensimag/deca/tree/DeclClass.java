package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.LabelManager;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.*;

import java.io.PrintStream;

/**
 * Declaration of a class (<code>class name extends superClass
 * {members}<code>).
 *
 * @author @AUTHOR@
 */
public class DeclClass extends AbstractDeclClass {

    private AbstractIdentifier className;
    private AbstractIdentifier superClassName;
    private ListDeclFieldSet   fieldSets;
    private ListDeclMethod     methods;

    public DeclClass(AbstractIdentifier className,
                     AbstractIdentifier superClassName, ListDeclFieldSet fields,
                     ListDeclMethod methods) {
        this.className = className;
        this.superClassName = superClassName;
        this.fieldSets = fields;
        this.methods = methods;
        
      /*  if(superClassName==null) 
        {
        	assert(this.className!=null);
        	assert(this.className.getClassDefinition()!=null);
        	assert(GPRegister.GB!=null);
        	//set the address of the class in the methods table
        	this.className.getClassDefinition().setDAddr(new RegisterOffset
        	(indexFromGb, GPRegister.GB));
			//this.methods.add(new DeclMethod());
			
		}
		else
			//set the address of the class in the methods table
			this.className.getClassDefinition().setDAddr(new RegisterOffset
			(indexFromGb, GPRegister.GB));
		
		indexFromGb+=this.getNumberOfMethods()+1;*/
    }

    public AbstractIdentifier getClassName() {
        return this.className;
    }

    public int getNumberOfMethodsExcludingInherited() {
        return this.className.getClassDefinition()
                .getNumberOfMethodsExcludingInherited();
    }

    public int getTotalNumberOfMethods() {
        return this.className.getClassDefinition().getTotalNumberOfMethods();
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("class " + className.getName());
        if (superClassName != null) {
            s.print(" extends " + superClassName.getName());
        }
        s.println(" {");
        s.indent();
        fieldSets.decompile(s);
        methods.decompile(s);
        s.unindent();
        s.println("}");
    }

    /**
     * First pass of contextual check
     *
     * @param compiler
     *
     * @throws ContextualError
     */
    @Override
    protected void verifyClass(DecacCompiler compiler) throws ContextualError {
        TypeDefinition t = compiler.getTypeDefinition(className.getName());

        // If the class is hidden, we MUST not perform some of the tests
        // operated here (it's essentialy to manage Object)
        boolean isClassTypeHidden;

        if (t != null) {
            isClassTypeHidden = t.isHidden();
        } else {
            isClassTypeHidden = false;
        }

        if (t != null && !isClassTypeHidden) {
            throw new ContextualError(
                    "Type " + className + " already exists (rule 1.3)",
                    getLocation());
        }

        if (superClassName ==
            null) { // We create the Objects class => address = 1(GB)

            superClassName = new Identifier(compiler.objectType.getName());
            superClassName.setDefinition(
                    compiler.getTypeDefinition(compiler.objectType.getName()));

            this.superClassName.getDefinition()
                    .setDAddr(new RegisterOffset(1, GPRegister.GB));
        } else {
            TypeDefinition superType =
                    compiler.getTypeDefinition(superClassName.getName());

            if (superType == null && !isClassTypeHidden) {

                throw new ContextualError("Super-class " + superClassName +
                                          " does not exist (rule 1.3)",
                                          getLocation());
            } else if (!isClassTypeHidden && !superType.isClass()) {

                throw new ContextualError(superClassName +
                                          " cannot be used as super-class " +
                                          "(rule 1.3)",
                                          getLocation());
            }

            // Get proper definition for super class
            this.superClassName.setDefinition(superType);
        }

        ClassType ct = new ClassType(className.getName(), getLocation(),
                                     superClassName.getClassDefinition());
        compiler.addClassDefinition(ct.getDefinition());

        Type verifiedType = className.verifyType(compiler);
        assert ct == verifiedType;
    }

    /**
     * Second pass of contextual check
     *
     * @param compiler
     *
     * @throws ContextualError
     */
    @Override
    protected void verifyClassMembers(DecacCompiler compiler)
            throws ContextualError {

        // Positionning offsets for extension
        if (!this.superClassName.getDefinition().getType()
                .equals(compiler.objectType)) {
            this.className.getClassDefinition().setNumberOfFields(
                    this.superClassName.getClassDefinition()
                            .getNumberOfFields());
        }

        this.fieldSets
                .verifyFieldSet(compiler, this.className.getClassDefinition());
        this.methods.verifySignatures(compiler,
                                      this.className.getClassDefinition());
    }

    /**
     * Third pass of contextual check
     *
     * @param compiler
     *
     * @throws ContextualError
     */
    @Override
    protected void verifyClassBody(DecacCompiler compiler)
            throws ContextualError {
        this.methods
                .verifyBodies(compiler, this.className.getClassDefinition());
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        className.prettyPrint(s, prefix, false);
        if (superClassName != null) {
            superClassName.prettyPrint(s, prefix, false);
        }
        fieldSets.prettyPrint(s, prefix, false);
        methods.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        className.iter(f);
        if (superClassName != null) {
            superClassName.iter(f);
        }
        fieldSets.iter(f);
        methods.iter(f);
    }

    @Override
    public void setClassDAddr(DAddr daddr) {
        this.className.getDefinition().setDAddr(daddr);
    }

    /**
     * @return The last cell from GB which is empty
     */
    @Override
    public void codeGenMethodsTable(DecacCompiler compiler) {

        compiler.addComment(
                "Code of the methods table of " + this.className.getName() +
                " at address " +
                ((RegisterOffset) this.className.getDefinition().getDAddr())
                        .getOffset() + "(GB)");

        if (!this.className.getClassDefinition().getType()
                .equals(compiler.objectType)) {
            compiler.addInstruction(
                    new LEA(this.superClassName.getClassDefinition().getDAddr(),
                            Register.getR(0))); //LEA PARENT'S_METHOD_TABLE,
                            // R0
        } else {
            compiler.addInstruction(
                    new LOAD(new NullOperand(), Register.getR(0)));
        }

        compiler.addInstruction(new STORE(Register.getR(0),
                                          this.getClassName().getDefinition()
                                                  .getDAddr()));

        int indexFromGb = ((RegisterOffset) this.getClassName().getDefinition()
                .getDAddr()).getOffset();

        for (MethodDefinition m : this.className.getClassDefinition()
                .getMembers().getListOfMethods()) {
            indexFromGb++;
            m.codeGenMethodsTable(compiler, indexFromGb);
        }
    }

    @Override
    public void generateMethods(DecacCompiler compiler) {
        // Pick each method and generate it
        for (AbstractDeclMethod meth : this.methods.getList()) {
            meth.codeGenMethod(compiler, (ClassDefinition) this.className
                    .getDefinition());
        }
    }

    @Override
    public void generateInitMethod(DecacCompiler compiler) {
        LabelManager lm = compiler.getLabelManager();

        // Generate init method
        // If we've got a parent, call parent's init first
        // init.class:
        compiler.addLabel(lm.getInit(className));

        compiler.enableTSTOMonitoring();

        NonScratchManager mgr = NonScratchManager.newManagerR2(compiler);
        int regSavePoint = compiler.getNbLines();

        // Call superclass init (don't bother if superclass is Object)
        if (!this.superClassName.getClassDefinition().getType()
                .equals(compiler.objectType)) {
            // LOAD -2(LB), R0
            // PUSH R0
            // Get "this" and add it on the stack to call parent
            compiler.addInstruction(
                    new LOAD(new RegisterOffset(-2, Register.LB), Register.R0));
            compiler.addInstruction(new PUSH(Register.R0));

            // BSR init.parent
            // Call parent's init
            compiler.addInstruction(new BSR(lm.getInit(superClassName)),
                                    "call superclass init");

            // Remove pushed element to prevent stack size loose
            compiler.addInstruction(new SUBSP(1));
        }

        // Well, parent's fields are initialized, it's time to initialize ours
        // Load default value into R0
        for (AbstractDeclFieldSet fieldSet : this.fieldSets.getList()) {
            fieldSet.codeGen(compiler, mgr);
        }

        mgr.codeGenRestoreRegs(compiler, regSavePoint);

        compiler.addInstruction(new RTS());

        compiler.insertTSTO();
    }

    @Override
    public void allocateMethodsTable(int indexFromGb) {
        //set the address of the class in the methods table
        this.setClassDAddr(new RegisterOffset(indexFromGb, GPRegister.GB));
    }

    public AbstractIdentifier getSuperClassName() {
        return superClassName;
    }
}
