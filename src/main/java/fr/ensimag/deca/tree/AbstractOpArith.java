package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.BOV;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;
/**
 * Arithmetic binary operations (+, -, /, ...)
 *
 * @author @AUTHOR@
 */
public abstract class AbstractOpArith extends AbstractBinaryExpr {

    public AbstractOpArith(AbstractExpr leftOperand,
                           AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
                           ClassDefinition currentClass)
            throws ContextualError {
        Type tleft = this.getLeftOperand()
                .verifyExpr(compiler, localEnv, currentClass);
        Type tright = this.getRightOperand()
                .verifyExpr(compiler, localEnv, currentClass);

        if ((!tleft.isInt() && !tleft.isFloat()) ||
            (!tright.isInt() && !tright.isFloat())) {
            throw new ContextualError("Unexpected operand type, " +
                                      "operands cannot be something else than" +
                                      " Integer or Float",
                                      this.getLocation());
        }

        if (tleft.isFloat() && !tright.isFloat()) {
            this.setRightOperand(new ConvFloat(this.getRightOperand()));
            this.getRightOperand().setType(compiler.floatType);
            // this.getRightOperand().verifyExpr(compiler, localEnv,
            // currentClass);
            this.setType(compiler.floatType);
        } else if (!tleft.isFloat() && tright.isFloat()) {
            this.setLeftOperand(new ConvFloat(this.getLeftOperand()));
            this.getLeftOperand().setType(compiler.floatType);
            // this.getLeftOperand().verifyExpr(compiler, localEnv,
            // currentClass);
            this.setType(compiler.floatType);
        } else {
            assert (tleft.sameType(tright));
            this.setType(tleft);
        }

        return this.getType();
    }

    /**
     * This is the computation dispatcher, it chosses the right method to use to
     * realize the computation
     */
    @Override
    protected void codeGenInst(DecacCompiler compiler, NonScratchManager mgr) {
        GPRegister dest = mgr.getReg();

        if (mgr.hasReachedLimit()) {
            // <codeExp(e1, n)>
            this.getLeftOperand().codeGenInst(compiler, mgr);
            compiler.addInstruction(new PUSH(dest));

            // <codeExp(e2, n)>
            this.getRightOperand().codeGenInst(compiler, mgr);

            // LOAD Rn, R0
            compiler.addInstruction(new LOAD(dest, Register.R0));

            // POP Rn
            compiler.addInstruction(new POP(dest));

            this.calculateArithFrom(compiler, Register.R0, dest);
        } else {
            NonScratchManager nextMgr = mgr.next();

            // <codeExp(e1, n)>
            this.getLeftOperand().codeGenInst(compiler, mgr);
            // <codeExp(e2, n)>
            this.getRightOperand().codeGenInst(compiler, nextMgr);

            // <mnemo(op)> rn+1, rn
            this.calculateArithFrom(compiler, nextMgr.getReg(), dest);
        }
        
        if (!compiler.getCompilerOptions().getNoCheck() && !this.getLeftOperand().getType().isInt())
            compiler.addInstruction(
                    new BOV(compiler.getLabelManager().arithError));
    }

    /**
     * Each operator is pleased to get the two registers and operates its
     * operation. The result must be saved in op2
     *
     * @param compiler
     * @param op1
     * @param op2
     */
    protected abstract void calculateArithFrom(DecacCompiler compiler,
                                               GPRegister op1, GPRegister op2);
}
