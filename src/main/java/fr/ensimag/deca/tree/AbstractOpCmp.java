package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.BoolUtils;
import fr.ensimag.deca.tools.NonScratchManager;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.*;

/**
 * @author @AUTHOR@
 */
public abstract class AbstractOpCmp extends AbstractBinaryExpr {

    public AbstractOpCmp(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
                           ClassDefinition currentClass)
            throws ContextualError {
        Type leftType =
                getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
        Type rightType =
                getRightOperand().verifyExpr(compiler, localEnv, currentClass);

        if ((!leftType.isBoolean() || !rightType.isBoolean()) &&
            (!leftType.isArithOp() || !rightType.isArithOp()) &&
            (!leftType.isClassOrNull() || !rightType.isClassOrNull())) {
            throw new ContextualError(
                    "Cannot compare " + leftType + " with " + rightType,
                    this.getLocation());
        }

        this.verifyCmp(compiler, localEnv, currentClass);

        this.setType(compiler.booleanType);
        return compiler.booleanType;
    }

    protected abstract void verifyCmp(DecacCompiler compiler,
                                      EnvironmentExp localEnv,
                                      ClassDefinition currentClass)
            throws ContextualError;

    @Override
    public void codeGenCond(DecacCompiler compiler, NonScratchManager mgr,
                            boolean branchIfTrue, Label nextLabel) {
        Type leftType = getLeftOperand().getType();
        Type rightType = getRightOperand().getType();
        if (!mgr.hasReachedLimit()) {
            NonScratchManager leftMgr = mgr;
            NonScratchManager rightMgr = mgr.next();

            GPRegister leftGPR = leftMgr.getReg();
            GPRegister rightGPR = rightMgr.getReg();

            getLeftOperand().codeGenInst(compiler, leftMgr);
            getRightOperand().codeGenInst(compiler, rightMgr);

            if (leftType.isFloat() && rightType.isInt()) {
                compiler.addInstruction(new FLOAT(rightGPR, rightGPR));
            } else if (rightType.isFloat() && leftType.isInt()) {
                compiler.addInstruction(new FLOAT(leftGPR, leftGPR));
            }

            compiler.addInstruction(new CMP(rightGPR, leftGPR));
            compiler.addInstruction(
                    this.getBranchInstruction(branchIfTrue, nextLabel));
        } else {
            getRightOperand().codeGenInst(compiler, mgr);
            if (leftType.isFloat() && rightType.isInt()) {
                compiler.addInstruction(new FLOAT(mgr.getReg(), mgr.getReg()));
            }
            compiler.addInstruction(new PUSH(mgr.getReg()));

            getLeftOperand().codeGenInst(compiler, mgr);

            if (rightType.isFloat() && leftType.isInt()) {
                compiler.addInstruction(new FLOAT(mgr.getReg(), mgr.getReg()));
            }


            compiler.addInstruction(new CMP(new RegisterOffset(0, Register.SP), mgr.getReg()));
            compiler.addInstruction(new SUBSP(1));
            compiler.addInstruction(
                    this.getBranchInstruction(branchIfTrue, nextLabel));



        }
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler, NonScratchManager mgr) {
        BoolUtils.codeGenBool(compiler, mgr, this, false,
                              getClass().getSimpleName());
    }

    protected abstract Instruction getBranchInstruction(boolean branchIfTrue,
                                                        Label nextLabel);
}
