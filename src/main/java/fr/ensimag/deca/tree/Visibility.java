package fr.ensimag.deca.tree;

import fr.ensimag.deca.tools.IndentPrintStream;

/**
 * Visibility of a field.
 *
 * @author @AUTHOR@
 */

public enum Visibility {
    PUBLIC,
    PROTECTED;

    public void decompile(IndentPrintStream s) {
        if (this == Visibility.PROTECTED) {
            s.print(Visibility.PROTECTED.toString().toLowerCase() + " ");
        }
    }
}
