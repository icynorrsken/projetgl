package fr.ensimag.deca;

import fr.ensimag.deca.context.*;
import fr.ensimag.deca.syntax.DecaLexer;
import fr.ensimag.deca.syntax.DecaParser;
import fr.ensimag.deca.syntax.InvalidFormatInput;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.LabelManager;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.deca.tree.AbstractProgram;
import fr.ensimag.deca.tree.Location;
import fr.ensimag.deca.tree.LocationException;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BOV;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.TSTO;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Decac compiler instance.
 * <p/>
 * This class is to be instantiated once per source file to be compiled. It
 * contains the meta-data used for compiling (source file name, compilation
 * options) and the necessary utilities for compilation (symbol tables, abstract
 * representation of target file, ...).
 * <p/>
 * It contains several objects specialized for different tasks. Delegate methods
 * are used to simplify the code of the caller (e.g. call
 * compiler.addInstruction() instead of compiler.getProgram().addInstruction()).
 *
 * @author @AUTHOR@
 */
public class DecacCompiler {
    private static final Logger LOG = Logger.getLogger(DecacCompiler.class);

    /**
     * Portable newline character.
     */
    private static final String nl = System.getProperty("line.separator", "\n");

    public final Type      intType;
    public final Type      floatType;
    public final Type      voidType;
    public final Type      booleanType;
    public final Type      nullType;
    public       ClassType objectType;
            // Not final because cannot be managed here
    // since Object is a in-memory object !

    // must be initialized by disableTSTOMonitoring
    private int tstoInsertPoint;

    protected LabelManager labelManager;

    protected LinkedList<String> emittedwarning;

    public DecacCompiler(CompilerOptions compilerOptions, File source) {
        super();
        this.compilerOptions = compilerOptions;
        this.source = source;

        disableTSTOMonitoring();

        voidType = newBuiltinTypeDefinition(new VoidType(getSymbol("void")));
        intType = newBuiltinTypeDefinition(new IntType(getSymbol("int")));
        floatType = newBuiltinTypeDefinition(new FloatType(getSymbol("float")));
        booleanType =
                newBuiltinTypeDefinition(new BooleanType(getSymbol("boolean")));
        nullType = newBuiltinTypeDefinition(new NullType(getSymbol("null")));

        // TODO: fill out object with actual deca code (or just a
        // pre-generated tree) for equals
        /* objectType  = new ClassType(getSymbol("Object"), Location.BUILTIN,
         null);
        ClassDefinition objectDef = new ClassDefinition(objectType, Location
        .BUILTIN, null);
        envTypes.put(objectType.getName(), objectDef);
        */

        this.labelManager = new LabelManager();

        if (compilerOptions != null) {
            if (this.compilerOptions.displayWarnings()) {
                this.emittedwarning = new LinkedList<>();

                // Adds default warnings
                if (this.compilerOptions.getNoCheck()) {
                    this.emitWarning(
                            "Overflow checking on arithmetic computation" +
                            " is disabled by argument \"-n\"",
                            Location.BUILTIN);
                }
            }
        }
    }

    private Type newBuiltinTypeDefinition(Type t) {
        envTypes.put(t.getName(), new TypeDefinition(t, Location.BUILTIN));
        return t;
    }

    public void addClassDefinition(ClassDefinition cd) {
        envTypes.put(cd.getType().getName(), cd);
    }

    /**
     * Source file associated with this compiler instance.
     */
    public File getSource() {
        return source;
    }

    /**
     * Compilation options (e.g. when to stop compilation, number of registers
     * to use, ...).
     */
    public CompilerOptions getCompilerOptions() {
        return compilerOptions;
    }

    /**
     * Returns the label manager
     *
     * @return
     */
    public LabelManager getLabelManager() {
        return this.labelManager;
    }

    /**
     * @see fr.ensimag.ima.pseudocode.IMAProgram#add(fr.ensimag.ima
     * .pseudocode.AbstractLine)
     */
    public void add(AbstractLine line) {
        program.add(line);
    }

    /**
     * @see fr.ensimag.ima.pseudocode.IMAProgram#addComment(java.lang.String)
     */
    public void addComment(String comment) {
        if (comment.contains("\n")) {
            this.addMultipleLineComment(comment);
        } else {
            program.addComment(comment);
        }
    }

    public void addMultipleLineComment(String comment) {
        String[] strings = comment.split("\n");
        for (String s : strings) {
            if (!s.isEmpty()) {
                program.addComment(s);
            }
        }
    }

    /**
     * @see fr.ensimag.ima.pseudocode.IMAProgram#addLabel(fr.ensimag.ima
     * .pseudocode.Label)
     */
    public void addLabel(Label label) {
        program.addLabel(label);
    }

    /**
     * Starts monitoring stack usage of all new instructions.
     */
    public void enableTSTOMonitoring() {
        tstoInsertPoint = getNbLines();
    }

    /**
     * Sets the TSTO insertion point to an obviously wrong value, so that any
     * calls to insertTSTO() will fail unless enableTSTOMonitoring() has been
     * called.
     */
    private void disableTSTOMonitoring() {
        tstoInsertPoint = -1;
    }

    /**
     * Inserts TSTO for all instructions added since enableTSTOMonitoring(), and
     * disables TSTO monitoring.
     */
    public void insertTSTO() {
        // nothing to do here if -n options was given to decac
        if (getCompilerOptions().getNoCheck())
            return;

        assert tstoInsertPoint >= 0;

        int peak = 0;
        int current = 0;

        for (AbstractLine line : program
                .getLines(tstoInsertPoint, getNbLines())) {
            assert line instanceof Line;
            Instruction inst = ((Line) line).getInstruction();
            if (inst != null) {
                current += inst.getStackUsage();
                if (current > peak) {
                    peak = current;
                }
            }
        }

        insertInstruction(tstoInsertPoint, new TSTO(peak));
        insertInstruction(tstoInsertPoint + 1,
                          new BOV(labelManager.stackOverflow));

        disableTSTOMonitoring();
    }

    /**
     * @see fr.ensimag.ima.pseudocode.IMAProgram#addInstruction(fr.ensimag
     * .ima.pseudocode.Instruction)
     */
    public void addInstruction(Instruction instruction) {
        program.addInstruction(instruction);
    }

    /**
     * @see fr.ensimag.ima.pseudocode.IMAProgram#addInstruction(fr.ensimag
     * .ima.pseudocode.Instruction,
     * java.lang.String)
     */
    public void addInstruction(Instruction instruction, String comment) {
        program.addInstruction(instruction, comment);
    }

    public void insertInstruction(int index, Instruction instruction) {
        assert tstoInsertPoint < 0 || index >= tstoInsertPoint;
        program.insertInstruction(index, instruction);
    }

    public void insertInstruction(int index, Instruction instruction,
                                  String comment) {
        assert tstoInsertPoint < 0 || index >= tstoInsertPoint;
        program.insertInstruction(index, instruction, comment);
    }

    public int getNbLines() {
        return program.getNbLines();
    }

    public SymbolTable.Symbol getSymbol(String name) {
        return symbolTable.create(name);
    }

    /**
     * Adds a warning message to the queue
     *
     * @param message
     * @param location
     */
    public void emitWarning(String message, Location location) {
        if (this.emittedwarning != null) {
            this.emittedwarning.add(location.toString() + ": " + message);
        }
    }

    /**
     * Display warnings to user
     */
    public void displayWarnings(PrintStream out) {
        if (this.emittedwarning != null && this.emittedwarning.size() != 0) {
            out.println("Warning(s) for file " + this.getSource().getName());
            for (String w : this.emittedwarning) {
                out.println(w);
            }
        }
    }

    /**
     * Retrieves a type from envTypes.
     */
    public TypeDefinition getTypeDefinition(SymbolTable.Symbol symbol) {
        return envTypes.get(symbol);
    }

    /**
     * @see fr.ensimag.ima.pseudocode.IMAProgram#display()
     */
    public String displayIMAProgram() {
        return program.display();
    }

    private final CompilerOptions compilerOptions;
    private final File            source;
    /**
     * The main program. Every instruction generated will eventually end up
     * here.
     */
    private final IMAProgram program = new IMAProgram();

    /**
     * Symbol table (maps String objects to Symbols to speed up String/String
     * comparisons).
     */
    private final SymbolTable symbolTable = new SymbolTable();

    /**
     * Global types environment mapping type symbols to built-in type
     * definitions and user class type definitions.
     */
    private final HashMap<SymbolTable.Symbol, TypeDefinition> envTypes =
            new HashMap<SymbolTable.Symbol, TypeDefinition>();

    /**
     * Run the compiler (parse source file, generate code)
     *
     * @return true on error
     */
    public boolean compile() {
        String sourceFile = source.getAbsolutePath();

        int beforeExtension = sourceFile.lastIndexOf('.');
        if (beforeExtension < 0) {
            beforeExtension = sourceFile.length();
        }
        String destFile = sourceFile.substring(0, beforeExtension) + ".ass";

        PrintStream err = System.err;
        PrintStream out = System.out;
        LOG.debug("Compiling file " + sourceFile + " to assembly file " +
                  destFile);
        try {
            boolean ok = doCompile(sourceFile, destFile, out, err);

            // If compilation is a success, also display warnings
            this.displayWarnings(err);

            return ok;
        } catch (LocationException e) {
            e.display(err);
            return true;
        } catch (DecacFatalError e) {
            err.println(e.getMessage());
            return true;
        } catch (StackOverflowError e) {
            LOG.debug("stack overflow", e);
            e.printStackTrace();
            err.println(
                    "Stack overflow while compiling file " + sourceFile + ".");
            return true;
        } catch (InvalidFormatInput e) {
            LOG.fatal("Invalid format provided for " + e.getMessage());
            err.println("Invalid format provided for " + e.getMessage());

            return true;
        } catch (Exception e) {
            LOG.fatal(
                    "Exception raised while compiling file " + sourceFile + ":",
                    e);
            err.println("Internal compiler error while compiling file " +
                        sourceFile + ", sorry.");
            return true;
        } catch (AssertionError e) {
            LOG.fatal(
                    "Assertion failed while compiling file " + sourceFile + ":",
                    e);
            err.println("Internal compiler error while compiling file " +
                        sourceFile + ", sorry.");
            return true;
        }
    }

    /**
     * Internal function that does the job of compiling (i.e. calling lexer,
     * verification and code generation).
     *
     * @param sourceName name of the source (deca) file
     * @param destName name of the destination (assembly) file
     * @param out stream to use for standard output (output of decac -p)
     * @param err stream to use to display compilation errors
     *
     * @return true on error
     */
    private boolean doCompile(String sourceName, String destName,
                              PrintStream out, PrintStream err)
            throws DecacFatalError, LocationException {
        AbstractProgram prog = doLexingAndParsing(sourceName, err);

        if (prog == null) {
            LOG.info("Parsing failed");
            return true;
        }

        if (compilerOptions.getParseOnly()) {//if "decac -p"
            System.out.println(prog.decompile());
            return false;
        }

        assert (prog.checkAllLocations());
        prog.verifyProgram(this);
        assert (prog.checkAllDecorations());

        if (compilerOptions.getVerifOnly())//if "decac -v"
            return false;

        addComment("start main program");
        prog.codeGenProgram(this);
        addComment("end main program");
        LOG.debug("Generated assembly code:" + nl + program.display());
        LOG.info("Output file assembly file is: " + destName);

        FileOutputStream fstream = null;
        try {
            fstream = new FileOutputStream(destName);
        } catch (FileNotFoundException e) {
            throw new DecacFatalError(
                    "Failed to open output file: " + e.getLocalizedMessage());
        }

        LOG.info("Writing assembler file ...");

        program.display(new PrintStream(fstream));
        LOG.info("Compilation of " + sourceName + " successful.");

        return false;
    }

    /**
     * Build and call the lexer and parser to build the primitive abstract
     * syntax tree.
     *
     * @param sourceName Name of the file to parse
     * @param err Stream to send error messages to
     *
     * @return the abstract syntax tree
     *
     * @throws DecacFatalError When an error prevented opening the source file
     * @throws DecacInternalError When an inconsistency was detected in the
     * @throws LocationException When a compilation error (incorrect program)
     * occurs. compiler.
     */
    protected AbstractProgram doLexingAndParsing(String sourceName,
                                                 PrintStream err)
            throws DecacFatalError, DecacInternalError {
        DecaLexer lex;
        try {
            lex = new DecaLexer(new ANTLRFileStream(sourceName));
        } catch (IOException ex) {
            throw new DecacFatalError(
                    "Failed to open input file: " + ex.getLocalizedMessage());
        }
        lex.setDecacCompiler(this);
        CommonTokenStream tokens = new CommonTokenStream(lex);
        DecaParser parser = new DecaParser(tokens);
        parser.setDecacCompiler(this);
        return parser.parseProgramAndManageErrors(err);
    }

    /**
     * Check for null pointer deref (only if -n option wasn't given to decac)
     *
     * @param reg register containing the tested pointer
     */
    public void checkNullDeref(GPRegister reg) {
        if (getCompilerOptions().getNoCheck())
            return;

        // TODO : CMP is maybe not required here, because LOAD already sets
        // condition codes
        // ...but we have to de absolutely positive that a LOAD occured
        // before this method
        addInstruction(new CMP(new NullOperand(), reg),
                       "probably not required - LOAD already sets CC");
        addInstruction(new BEQ(getLabelManager().nullDeref),
                       "prevent null deref");
    }
}
