package fr.ensimag.deca.context;

import fr.ensimag.deca.DecacCompiler;

/**
 * @author Ensimag
 */
public class StringType extends Type {

    /**
     * Secret because the user cannot create string variables. Therefore, the
     * symbol name must contain forbidden characters.
     */
    public static final String SECRET_STRING_SYMBOL_NAME = "~~~ String ~~~";

    public StringType(DecacCompiler compiler) {
        super(compiler.getSymbol(SECRET_STRING_SYMBOL_NAME));
    }

    @Override
    public boolean isString() {
        return true;
    }

    @Override
    public boolean sameType(Type otherType) {
        return otherType instanceof StringType;
    }
}
