package fr.ensimag.deca.context;

import fr.ensimag.deca.codegen.AbstractCastGen;
import fr.ensimag.deca.tools.SymbolTable.Symbol;
import fr.ensimag.deca.tree.Location;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.ImmediateInteger;

/**
 * Deca Type (internal representation of the compiler)
 *
 * @author @AUTHOR@
 */

public abstract class Type {

    public AbstractCastGen getCastGen(Type dest) {
        return null;
    }

    /**
     * True if this and otherType represent the same type (in the case of
     * classes, this means they represent the same class).
     */
    public abstract boolean sameType(Type otherType);

    /**
     * True if assignments such as "lhsVar = rhsVar" are allowed, where this
     * type is that of lhsVar.
     */
    public boolean assignCompatible(Type rhs) {
        return sameType(rhs);
    }

    /**
     * True if casting this type to dest is allowed, i.e. if (dest)(x) is
     * allowed where (x) is of this type.
     *
     * @param dest destination type
     */
    public boolean staticCastCompatible(Type dest) {
        return this.assignCompatible(dest) || dest.assignCompatible(this);
    }

    private final Symbol name;

    public Type(Symbol name) {
        this.name = name;
    }

    public Symbol getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName().toString();
    }

    public boolean isClass() {
        return false;
    }

    public boolean isInt() {
        return false;
    }

    public boolean isFloat() {
        return false;
    }

    public boolean isBoolean() {
        return false;
    }

    public boolean isArithOp() {
        return this.isInt() || this.isFloat();
    }

    public boolean isVoid() {
        return false;
    }

    public boolean isString() {
        return false;
    }

    public boolean isNull() {
        return false;
    }

    public boolean isClassOrNull() {
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Type) {
            Type objType = (Type) obj;

            return this.name.equals(objType.name) &&
                   this.isArithOp() == objType.isArithOp() &&
                   this.isBoolean() == objType.isBoolean() &&
                   this.isClass() == objType.isClass() &&
                   this.isClassOrNull() == objType.isClassOrNull() &&
                   this.isFloat() == objType.isFloat() &&
                   this.isInt() == objType.isInt() &&
                   this.isString() == objType.isString() &&
                   this.isVoid() == objType.isVoid();
        } else {
            return false;
        }
    }

    /**
     * Returns the same object, as type ClassType, if possible. Throws
     * ContextualError(errorMessage, l) otherwise.
     * <p/>
     * Can be seen as a cast, but throws an explicit contextual error when the
     * cast fails.
     */
    public ClassType asClassType(String errorMessage, Location l)
            throws ContextualError {
        throw new ContextualError(errorMessage, l);
    }

    public DVal getDefaultValue() {
        return new ImmediateInteger(0);
    }
}
