package fr.ensimag.deca.context;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tree.DeclMethod;
import fr.ensimag.deca.tree.Location;
import fr.ensimag.ima.pseudocode.*;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.STORE;
import org.apache.commons.lang.Validate;

/**
 * Definition of a method
 *
 * @author @AUTHOR@
 */
public class MethodDefinition extends NonTypeDefinition {

    @Override
    public boolean isMethod() {
        return true;
    }

    public Label getLabel() {
        Validate.isTrue(label != null,
                        "setLabel() should have been called before");
        return label;
    }

    public void setLabel(Label label) {
        this.label = label;
    }

    public int getIndex() {
        return index;
    }

    private final ClassDefinition containingClass;
    private       int             index;

    @Override
    public MethodDefinition asMethodDefinition(String errorMessage, Location l)
            throws ContextualError {
        return this;
    }

    private final Signature  signature;
    private       Label      label;
    private final DeclMethod decl;

    public DeclMethod getDecl() {
        return decl;
    }

    /**
     * @param type Return type of the method
     * @param location Location of the declaration of the method
     * @param memberOf
     * @param signature List of arguments of the method
     * @param index Index of the method in the class. Starts from 0.
     */
    public MethodDefinition(Type type, Location location,
                            ClassDefinition memberOf, Signature signature,
                            int index, DeclMethod decl) {
        super(type, location);
        this.containingClass = memberOf;
        this.signature = signature;
        this.index = index;
        this.decl = decl;
    }

    public Signature getSignature() {
        return signature;
    }

    public ClassDefinition getContainingClass() {
        return containingClass;
    }

    @Override
    public String getKind() {
        return "method";
    }

    @Override
    public boolean isExpression() {
        return false;
    }

    /**
     * Generate the code which set the current method in the methods table
     *
     * @param compiler
     * @param indexFromGb : Where write the method's label from GB
     */
    public void codeGenMethodsTable(DecacCompiler compiler, int indexFromGb) {
        //Ajout de l'adresse de la méthode dans la table des méthodes
        this.setDAddr(new RegisterOffset(indexFromGb, GPRegister.GB));

        compiler.addInstruction(new LOAD(new LabelOperand(this.getLabel()),
                                         Register.getR(
                                                 0))); //LEA
                                                 // PARENT'S_METHOD_TABLE,R0
        compiler.addInstruction(new STORE(Register.getR(0), this.getDAddr()));
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof MethodDefinition) {
            MethodDefinition md = (MethodDefinition) other;
            return md.containingClass.equals(this.containingClass) &&
                   this.signature.equals(md.signature) &&
                   (this.getLabel() != null && md.getLabel() != null ? this
                           .getLabel().equals(md.getLabel()) : true);
        }

        return false;
    }
}
