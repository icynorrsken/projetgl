package fr.ensimag.deca.context;

import fr.ensimag.deca.codegen.AbstractCastGen;
import fr.ensimag.deca.codegen.StaticCast;
import fr.ensimag.deca.tools.SymbolTable;

/**
 * @author Ensimag
 */
public class NullType extends Type {

    public NullType(SymbolTable.Symbol name) {
        super(name);
    }

    @Override
    public boolean sameType(Type otherType) {
        return otherType instanceof NullType;
    }

    @Override
    public boolean staticCastCompatible(Type dest) {
        return dest.isClassOrNull();
    }

    @Override
    public AbstractCastGen getCastGen(Type dest) {
        if (dest.isClass()) {
            return new StaticCast();
        } else {
            return null;
        }
    }

    @Override
    public boolean isNull() {
        return true;
    }

    @Override
    public boolean isClassOrNull() {
        return true;
    }
}
