package fr.ensimag.deca.context;

import fr.ensimag.deca.tree.Location;
import fr.ensimag.ima.pseudocode.DAddr;

/**
 * Definition of an identifier.
 *
 * @author @AUTHOR@
 */
public abstract class Definition {
    // This is an indicator : a hidden definition
    // will provoque the compiler to ignore
    // double definitions for classes (principaly)
    protected boolean hidden;

    @Override
    public String toString() {
        String res;
        res = getKind();
        if (location == Location.BUILTIN) {
            res += " (builtin)";
        } else {
            res += " defined at " + location;
        }
        res += ", type=" + type;
        return res;
    }

    public abstract String getKind();

    public Definition(Type type, Location location) {
        super();
        this.location = location;
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    private Location location;
    private Type     type;
    private DAddr daddr = null;

    public boolean isField() {
        return false;
    }

    public boolean isMethod() {
        return false;
    }

    public boolean isClass() {
        return false;
    }

    public boolean isParam() {
        return false;
    }

    /**
     * Return the same object, as type MethodDefinition, if possible. Throws
     * ContextualError(errorMessage, l) otherwise.
     */
    public MethodDefinition asMethodDefinition(String errorMessage, Location l)
            throws ContextualError {
        throw new ContextualError(errorMessage, l);
    }

    /**
     * Return the same object, as type FieldDefinition, if possible. Throws
     * ContextualError(errorMessage, l) otherwise.
     */
    public FieldDefinition asFieldDefinition(String errorMessage, Location l)
            throws ContextualError {
        throw new ContextualError(errorMessage, l);
    }

    public abstract boolean isExpression();

    public DAddr getDAddr() {
        return daddr;
    }

    public void setDAddr(DAddr daddr) {
        this.daddr = daddr;
    }

    @Override
    public boolean equals(Object otherObject) {
        // TODO: c'est pas propre, e.g. si on a deux methodes de noms
        // differents ca renverra true
        if (otherObject instanceof Definition) {
            Definition d = (Definition) otherObject;
            return d.isClass() == this.isClass() &&
                   d.isExpression() == this.isExpression() &&
                   d.isField() == this.isField() &&
                   d.isMethod() == this.isMethod() &&
                   d.isParam() == this.isParam();
        }
        return false;
    }

    public boolean isHidden() {
        if (this.hidden) {
            this.hidden = false;
            return true;
        }
        return false;
    }

    public void hide() {
        this.hidden = true;
    }
}
