/**
 * Utilities used for Deca contextual verifications.
 * <p/>
 * The contextual verification itself is implement as methods of the {@link
 * fr.ensimag.deca.tree.Tree} class.
 */
package fr.ensimag.deca.context;
