package fr.ensimag.deca.context;

import fr.ensimag.deca.tree.Location;
import org.apache.commons.lang.Validate;

/**
 * Definition of a class.
 *
 * @author @AUTHOR@
 */
public class ClassDefinition extends TypeDefinition {

    public void setNumberOfFields(int numberOfFields) {
        this.numberOfFields = numberOfFields;
    }

    public int getNumberOfFields() {
        return numberOfFields;
    }

    public int incNumberOfFields() {
        this.numberOfFields++;
        return numberOfFields;
    }

    /**
     * @return Number of methods of the current class, excluding inherited
     * methods
     */
    public int getNumberOfMethodsExcludingInherited() {
        return numberOfMethods;
    }

    /**
     * @return Number of methods of the current class, including inherited
     * methods
     */
    public int getTotalNumberOfMethods() {
        return this.getMembers().getListOfMethods().size();
    }

    public void setNumberOfMethods(int n) {
        Validate.isTrue(n >= 0);
        numberOfMethods = n;
    }

    public int incNumberOfMethods() {
        numberOfMethods++;
        return numberOfMethods;
    }

    private int numberOfFields  = 0;
    private int numberOfMethods = 0;

    @Override
    public boolean isClass() {
        return true;
    }

    @Override
    public ClassType getType() {
        // Cast succeeds by construction because the type has been correctly set
        // in the constructor.
        return (ClassType) super.getType();
    }

    ;

    public ClassDefinition getSuperClass() {
        return superClass;
    }

    private EnvironmentExp  members;
    private ClassDefinition superClass;

    public EnvironmentExp getMembers() {
        return members;
    }

    public void setMembers(EnvironmentExp members) {
        this.members = members;
    }

    public ClassDefinition(ClassType type, Location location,
                           ClassDefinition superClass) {
        super(type, location);
        this.superClass = superClass;
        if (superClass != null) {
            this.members = new EnvironmentExp(superClass.getMembers());
        }
    }

    @Override
    public boolean equals(Object otherObject) {
        return otherObject instanceof ClassDefinition &&
               ((ClassDefinition) otherObject).getType().sameType(getType());
    }
}
