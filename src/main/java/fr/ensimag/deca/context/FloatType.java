package fr.ensimag.deca.context;

import fr.ensimag.deca.codegen.AbstractCastGen;
import fr.ensimag.deca.codegen.Float2IntCast;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.ImmediateFloat;

/**
 * @author Ensimag
 */
public class FloatType extends Type {

    public FloatType(SymbolTable.Symbol name) {
        super(name);
    }

    @Override
    public boolean isFloat() {
        return true;
    }

    @Override
    public boolean sameType(Type otherType) {
        return otherType instanceof FloatType;
    }

    @Override
    public boolean assignCompatible(Type rhs) {
        return sameType(rhs) || rhs.isInt();
    }

    @Override
    public AbstractCastGen getCastGen(Type dest) {
        if (dest.isInt()) {
            return new Float2IntCast();
        } else {
            return null;
        }
    }

    @Override
    public DVal getDefaultValue() {
        return new ImmediateFloat(0);
    }
}
