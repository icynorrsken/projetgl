package fr.ensimag.deca.context;

import fr.ensimag.deca.tools.SymbolTable.Symbol;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Dictionary associating identifier's NonTypeDefinition to their names.
 * <p/>
 * This is actually a linked list of dictionaries: each EnvironmentExp has a
 * pointer to a parentEnvironment (corresponding to superclass or superblock).
 * Searching a definition is done in the EnvironmentExp first and in the
 * parentEnvironment if it fails. Insertion is always done in the first
 * EnvironmentExp of the list.
 *
 * @author @AUTHOR@
 */
public class EnvironmentExp {
    // A FAIRE : implémenter la structure de donnée représentant un
    // environnement (association nom -> définition, avec possibilité
    // d'empilement).

    private final HashMap<Symbol, NonTypeDefinition> map =
            new HashMap<Symbol, NonTypeDefinition>();

    private LinkedList<MethodDefinition> listOfMethods;
            // List of methods contained in parents
    // and in local environment

    // Our parent's environment
    private final EnvironmentExp parent;

    // The environments that have this one as parent
    private final LinkedList<EnvironmentExp> children;

    public EnvironmentExp(EnvironmentExp parentEnvironment) {
        this.parent = parentEnvironment;

        this.listOfMethods = new LinkedList<>();

        this.children = new LinkedList<>();

        // Finally, when all is good, if our parent is not empty,
        // then say "Vader, I am your child"
        if (this.parent != null) {
            this.parent.iAmOneOfYourChildren(this);
        }
    }

    /**
     * To get the partial methods table, if available
     *
     * @return
     */
    public LinkedList<MethodDefinition> getListOfMethods() {
        return (LinkedList<MethodDefinition>) this.listOfMethods.clone();
    }

    /**
     * To register an EnvExp as children to an other
     *
     * @param child
     */
    public void iAmOneOfYourChildren(EnvironmentExp child) {
        this.children.add(child);
    }

    /**
     * Called by a parent to say to the child that its methods table have been
     * updated and that he must also update its own list
     */
    public void sayThatTheMethodsHaveBeenUpdated() {
        this.listOfMethods = this.parent.getListOfMethods();
        this.sayToChildrenThatListOfMethodsHaveBeenUpdated();
    }

    /**
     * The father declares to all of its childrenn that its methods have been
     * updated
     */
    public void sayToChildrenThatListOfMethodsHaveBeenUpdated() {
        for (EnvironmentExp e : this.children) {
            e.sayThatTheMethodsHaveBeenUpdated();
        }
    }

    public static class DoubleDefException extends Exception {
        private static final long serialVersionUID = -2733379901827316441L;
    }

    /**
     * Return the definition of the symbol in the environment, or null if the
     * symbol is undefined.
     */
    public NonTypeDefinition get(Symbol key) {
        if (map.containsKey(key)) {
            return map.get(key);
        } else if (parent != null) {
            return parent.get(key);
        } else {
            return null;
        }
    }

    protected void reserve(NonTypeDefinition def) {
        // no-op
    }

    /**
     * Add the definition def associated to the symbol name in the environment.
     * <p/>
     * Adding a symbol which is already defined in the environment throws
     * DoubleDefException if the symbol is already declared in the first
     * Environment, and shadows the old declaration if the declaration was done
     * in an ancestor Environment.
     *
     * @param name Name of the symbol to define
     * @param def Definition of the symbol
     *
     * @throws DoubleDefException if the symbol is already defined at the last
     * level of the environment.
     */
    public void declare(Symbol name, NonTypeDefinition def)
            throws DoubleDefException {
        assert (name != null);

        if (map.containsKey(name)) {
            throw new DoubleDefException();
        } else {
            map.put(name, def);
            reserve(def);
        }

        // TODO: move to declare() ?
        // The new definition is a method, so we have to add it
        // to the flat definition to generate Method table
        if (def.isMethod()) {
            MethodDefinition defmd = (MethodDefinition) def;

            // At this state, parent had said when its table was updated
            // so we also updated ours to follow its changes

            // Next, if we are Object, then we're the only class in the one
            // we puts directly methods regardless of eventual overrides
            // (there was any other classes checked before)
            if (parent == null) {
                this.listOfMethods.add(defmd);

                // Don't forget to tell the children we get new
                // methods
                this.sayToChildrenThatListOfMethodsHaveBeenUpdated();
            } else {
                // If we're not Object, then, maybe are we overriding
                // parent's methods, so check that

                // Get a method that have the same name that the one we're
                // declaring
                // in parent
                NonTypeDefinition parentDefinition = this.parent.get(name);

                // If parent already have such a method
                if (parentDefinition != null && parentDefinition.isMethod()) {

                    // Are we overriding this method ?
                    MethodDefinition parentMethodDefinition =
                            (MethodDefinition) parentDefinition;

                    int index =
                            this.listOfMethods.indexOf(parentMethodDefinition);
                    this.listOfMethods.remove(index);
                    this.listOfMethods.add(index, defmd);

                    this.sayToChildrenThatListOfMethodsHaveBeenUpdated();
                } else {
                    // The current method is not called like an other method
                    // in parent
                    this.listOfMethods.add(defmd);
                    this.sayToChildrenThatListOfMethodsHaveBeenUpdated();
                }
            }
        }
    }

    /**
     * @return The number of symbols in te current environment
     */
    public int size() {
        return this.map.size();
    }

    /**
     * Indicates if this environment is method environment or not
     *
     * @return
     */
    public boolean isMethodEnv() {
        return false;
    }
}
