package fr.ensimag.deca.context;

import fr.ensimag.deca.codegen.AbstractCastGen;
import fr.ensimag.deca.codegen.DynamicCast;
import fr.ensimag.deca.codegen.StaticCast;
import fr.ensimag.deca.tools.SymbolTable.Symbol;
import fr.ensimag.deca.tree.Location;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.NullOperand;

/**
 * Type defined by a class.
 *
 * @author @AUTHOR@
 */
public class ClassType extends Type {

    protected ClassDefinition definition;

    public ClassDefinition getDefinition() {
        return this.definition;
    }

    @Override
    public ClassType asClassType(String errorMessage, Location l) {
        return this;
    }

    @Override
    public boolean isClass() {
        return true;
    }

    public AbstractCastGen getCastGen(Type dest) {
        if (staticCastCompatible(dest)) {
            return new StaticCast();
        } else if (dynamicCastCompatible(dest)) {
            return new DynamicCast((ClassType) dest);
        } else {
            return null;
        }
    }

    @Override
    public boolean staticCastCompatible(Type dest) {
        return dest.isClass() &&
               (sameType(dest) || isSubClassOf((ClassType) dest));
    }

    protected boolean dynamicCastCompatible(Type dest) {
        return dest.isClass() &&
               (sameType(dest) || ((ClassType) dest).isSubClassOf(this));
    }

    @Override
    public boolean assignCompatible(Type rhs) {
        return rhs.isNull() ||
               (rhs.isClass() && ((ClassType) rhs).isSubClassOf(this));
    }

    @Override
    public boolean isClassOrNull() {
        return true;
    }

    /**
     * Standard creation of a type class.
     */
    public ClassType(Symbol className, Location location,
                     ClassDefinition superClass) {
        super(className);
        this.definition = new ClassDefinition(this, location, superClass);
    }

    /**
     * Creates a type representing a class className. (To be used by subclasses
     * only)
     */
    protected ClassType(Symbol className) {
        super(className);
    }

    @Override
    public boolean sameType(Type otherType) {
        return this.equals(otherType);
    }

    /**
     * Return true if this is equal to, or a subclass of, ct.
     */
    public boolean isSubClassOf(ClassType ct) {
        ClassType ancestry = this;

        while (ancestry != null) {
            if (ancestry.sameType(ct)) {
                return true;
            }
            ClassDefinition superDef = ancestry.getDefinition().getSuperClass();
            if (superDef == null) {
                return false;
            }
            ancestry = ancestry.getDefinition().getSuperClass().getType();
        }

        return false;
    }

    @Override
    public DVal getDefaultValue() {
        return new NullOperand();
    }
}
