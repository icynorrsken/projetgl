package fr.ensimag.deca.context;

import java.util.ArrayList;
import java.util.List;

/**
 * Signature of a method (i.e. list of arguments)
 *
 * @author @AUTHOR@
 */
public class Signature {
    List<Type> args = new ArrayList<Type>();

    public void add(Type t) {
        args.add(t);
    }

    public Type paramNumber(int n) {
        return args.get(n);
    }

    public int size() {
        return args.size();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Signature) {
            return args.equals(((Signature) obj).args);
        } else {
            return false;
        }
    }
}
