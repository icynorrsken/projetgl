package fr.ensimag.deca.context;

import fr.ensimag.deca.tools.SymbolTable;

/**
 * @author Ensimag
 */
public class VoidType extends Type {

    public VoidType(SymbolTable.Symbol name) {
        super(name);
    }

    @Override
    public boolean isVoid() {
        return true;
    }

    @Override
    public boolean sameType(Type otherType) {
        return otherType instanceof VoidType;
    }
}
