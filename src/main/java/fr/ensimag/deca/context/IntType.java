package fr.ensimag.deca.context;

import fr.ensimag.deca.codegen.AbstractCastGen;
import fr.ensimag.deca.codegen.Int2FloatCast;
import fr.ensimag.deca.tools.SymbolTable;

/**
 * @author Ensimag
 */
public class IntType extends Type {

    public IntType(SymbolTable.Symbol name) {
        super(name);
    }

    @Override
    public boolean isInt() {
        return true;
    }

    @Override
    public boolean sameType(Type otherType) {
        return otherType instanceof IntType;
    }

    @Override
    public AbstractCastGen getCastGen(Type dest) {
        if (dest.isFloat()) {
            return new Int2FloatCast();
        } else {
            return null;
        }
    }
}
